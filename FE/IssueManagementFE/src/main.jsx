import "@fortawesome/fontawesome-free/css/all.min.css";
import ReactDOM from "react-dom/client";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "react-toastify/dist/ReactToastify.css";
import { store } from "./app/store";
import { ImportFile } from "./components/Import/ImportFile";
import { Layout } from "./layout/Layout";
import { ClassDetailsGeneral } from "./pages/ClassDetailsPage/ClassDetailsGeneral";
import { ClassDetailsMilestone } from "./pages/ClassDetailsPage/ClassDetailsMilestone";
import { ClassDetailsSettings } from "./pages/ClassDetailsPage/ClassDetailsSettings";
import { ClassDetailsStudents } from "./pages/ClassDetailsPage/ClassDetailsStudents";
import { ClassListPage } from "./pages/ClassListPage/ClassListPage";
import { ForgotPassword } from "./pages/ForgotPassword/ForgotPassword";
import { LandingPage } from "./pages/LandingPage/LandingPage";
import { SubjectDetailsAssignment } from "./pages/SubjectDetailsPage/SubjectDetailsAssignment";
import { SubjectDetailsGeneral } from "./pages/SubjectDetailsPage/SubjectDetailsGeneral";
import { SubjectDetailsSetting } from "./pages/SubjectDetailsPage/SubjectDetailsSetting";
import { SubjectListPage } from "./pages/SubjectListPage/SubjectListPage";
import SystemSettingListPage from "./pages/SystemSettingListPage/SystemSettingListPage";
import { UserListPage } from "./pages/UserListPage/UserListPage";
import UserLoginPage from "./pages/UserLoginPage/UserLoginPage";
import { UserProfilePage } from "./pages/UserProfilePage/UserProfilePage";
import UserRegisterPage from "./pages/UserRegisterPage/UserRegisterPage";
import { IssueListPage } from "./pages/IssueListPage/IssueListPage";
import { ProjectListPage } from "./pages/ProjectListPage/ProjectListPage";

ReactDOM.createRoot(document.getElementById("root")).render(
  <Provider store={store}>
    <BrowserRouter>
      <Routes path="/" element={<Layout />}>
        <Route path="/sign-in" element={<UserLoginPage />} />
        <Route path="/register" element={<UserRegisterPage />} />
        <Route path="/forgot-password" element={<ForgotPassword />} />

        {/* <Route path="/" element={<PrivateRoute />}> */}
        <Route path="/user-list" element={<UserListPage />} />
        <Route path="/class-list" element={<ClassListPage />} />
        <Route
          path="/class-details-general/:classId"
          element={<ClassDetailsGeneral />}
        />
        <Route
          path="/class-details-students/:classId"
          element={<ClassDetailsStudents />}
        />
        <Route
          path="/class-details-milestones/:classId"
          element={<ClassDetailsMilestone />}
        />
        <Route
          path="/class-details-settings/:classId"
          element={<ClassDetailsSettings />}
        />
        <Route path="/project-list" element={<ProjectListPage />} />
        <Route path="/issue-list" element={<IssueListPage />} />
        <Route path="/user-profile" element={<UserProfilePage />} />
        <Route path="/landing" element={<LandingPage />} />
        <Route
          path="/system-setting-list"
          element={<SystemSettingListPage />}
        />
        <Route path="/subject-list" element={<SubjectListPage />} />
        <Route
          path="/subject-details-general/:subjectId"
          element={<SubjectDetailsGeneral />}
        />
        <Route
          path="/subject-details-assignment/:subjectId"
          element={<SubjectDetailsAssignment />}
        />
        <Route
          path="/subject-details-setting/:subjectId"
          element={<SubjectDetailsSetting />}
        />

        <Route path="/demo" element={<ImportFile />} />
        {/* </Route> */}
      </Routes>
    </BrowserRouter>
  </Provider>
);
