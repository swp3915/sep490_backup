import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  loggedIn: {
    user_id: '',
    email: "",
    fullname: "",
    role: "",
    exp: 0,
  },
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setLoggedInUser(state, action) {
      state.loggedIn = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setLoggedInUser } = userSlice.actions;

const userReducer = userSlice.reducer;
export { userReducer };
