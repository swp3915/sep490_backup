import { ConditionEnum } from "src/enum/Enum";

const searchUtils = (filter, searchParams, setSearchParams, fetchData) => {
  if (filter.field === "") {
    return;
  }
  // console.log(filter);
  // const objIndex = searchParams.filterConditions.findIndex(obj => obj.field === filter.field)
  const filterConditions = searchParams.filterConditions.filter(
    (obj) => obj.condition !== ConditionEnum.Like
  );
  filterConditions.push(filter);
  const newSearchParams = {
    ...searchParams,
    filterConditions: filterConditions,
  };
  // console.log(newSearchParams);
  setSearchParams(newSearchParams);
  fetchData(newSearchParams);
};

const filterUtils = (filter, searchParams, setSearchParams, fetchData) => {
  // console.log(searchParams);
  const filterConditions = searchParams.filterConditions.filter(
    (obj) => obj.field !== filter.field
  );
  if (filter.value !== "all") {
    filterConditions.push(filter);
  }
  // console.log(filter.field);
  const newSearchParams = {
    ...searchParams,
    filterConditions: filterConditions,
  };
  // console.log(newSearchParams);
  setSearchParams(newSearchParams);
  // fetchData(newSearchParams);
};
export { searchUtils, filterUtils };
