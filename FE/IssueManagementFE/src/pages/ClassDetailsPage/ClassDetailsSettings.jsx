import { Tooltip } from "antd";
import React from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { NavbarDashboard } from "src/components/NavbarDashboard/NavbarDashboard";
import { ClassSettings } from "./ClassSettings/ClassSettings";
import { ToastContainer } from "react-toastify";

export const ClassDetailsSettings = () => {
  const { classId } = useParams();
  const navigate = useNavigate();
  return (
    <>
      <ToastContainer autoClose="2000" theme="colored" />
      <NavbarDashboard
        position="class"
        dashboardBody={
          <div className="d-flex flex-column flexGrow_1">
            <div className="col-xl-12">
              <div className="card custom-card">
                <div className="card-body p-0">
                  <div className="d-flex p-3 align-items-center justify-content-between">
                    <div>
                      <h6 className="fw-bold mb-0 title-setting">
                        Class Details
                      </h6>
                    </div>
                    <div>
                      <ul
                        className="nav nav-tabs nav-tabs-header mb-0 d-sm-flex d-block"
                        role="tablist"
                      >
                        <li className="nav-item m-1">
                          <a
                            className="nav-link"
                            onClick={() => {
                              navigate(`/class-details-general/${classId}`);
                            }}
                          >
                            General
                          </a>
                        </li>
                        <li className="nav-item pointer m-1">
                          <a
                            className="nav-link"
                            onClick={() => {
                              navigate(`/class-details-students/${classId}`);
                            }}
                          >
                            Students
                          </a>
                        </li>
                        <li className="nav-item pointer m-1">
                          <a
                            className="nav-link"
                            onClick={() => {
                              navigate(`/class-details-milestones/${classId}`);
                            }}
                          >
                            Milestones
                          </a>
                        </li>
                        <li className="nav-item pointer m-1">
                          <a
                            className="nav-link pointer active"
                            onClick={() => {
                              navigate(`/class-details-settings/${classId}`);
                            }}
                          >
                            Settings
                          </a>
                        </li>
                      </ul>
                    </div>
                    <Tooltip
                      title="Add new"
                      placement="top"
                      color="
                        #26BF94"
                      size="large"
                    >
                      <div className="btn-list"></div>
                    </Tooltip>
                  </div>
                </div>
              </div>
            </div>
            <ClassSettings classId={classId} />
          </div>
        }
      />
    </>
  );
};
