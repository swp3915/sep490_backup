import { PlusOutlined } from "@ant-design/icons";
import { Tooltip } from "antd";
import { useFormik } from "formik";
import React, { useState } from "react";
import { toast } from "react-toastify";
import { Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { axiosClient } from "src/axios/AxiosClient";
import { BaseButton } from "src/components/Base/BaseButton/BaseButton";
import { BaseCheckbox } from "src/components/Base/BaseCheckbox/BaseCheckbox";
import { BaseColorPicker } from "src/components/Base/BaseColorPicker/BaseColorPicker";
import { BaseInputField } from "src/components/Base/BaseInputField/BaseInputField";
import { BaseTextArea } from "src/components/Base/BaseTextArea/BaseTextArea";
import { swalWithBootstrapButtons } from "src/enum/swal";
import * as Yup from "yup";

export const NewIssueSetting = ({
  dataGroup,
  classId,
  searchParams,
  fetchData,
}) => {
  const formik = useFormik({
    initialValues: {
      issue_value: "",
      issue_group: dataGroup,
      description: "",
      status: 1,
      class_id: classId,
      style: "",
    },
    validationSchema: Yup.object({
      issue_value: Yup.string().required("Issue Value is required"),
      //   style: Yup.string().required("Style is required"),
    }),
    onSubmit: async (values) => {
      //   window.alert("submit");
      //   console.log(values);
      swalWithBootstrapButtons
        .fire({
          title: "Are you sure?",
          text: "Are you sure to add new Issue Setting?",
          icon: "warning",
          showCancelButton: true,
          reverseButtons: true,
          confirmButtonText: "Yes,add it!",
          cancelButtonText: "No, cancel!",
        })
        .then(async (result) => {
          // console.log(result);
          if (result.isConfirmed) {
            const { err } = await axiosClient.post(`/IssueSetting`, values);

            if (err) {
              toast.error("Add fail!");
            } else {
              toast.success("Add success!");
              swalWithBootstrapButtons.fire(
                "Updated!",
                "New subject has been added!.",
                "success"
              );
              formik.resetForm();
              setModal(!modal);
              fetchData(searchParams);
            }
          } else {
            {
              swalWithBootstrapButtons.fire(
                "Cancelled",
                "Your imaginary file is safe :)",
                "error"
              );
            }

            // console.log(values);
          }
        });
    },
  });
  const [modal, setModal] = useState(false);

  const toggle = () => {
    setModal(!modal);
  };
  return (
    <>
      <Tooltip
        title="Add New"
        placement="top"
        color="rgb(38, 191, 148)"
        size="large"
      >
        <div>
          <BaseButton
            icon={<PlusOutlined />}
            variant="outline"
            nameTitle="px-2 py-1"
            color="success"
            onClick={toggle}
          />
        </div>
      </Tooltip>
      <Modal isOpen={modal} toggle={toggle} size="lg" centered>
        <form onSubmit={formik.handleSubmit} autoComplete="off">
          <ModalHeader toggle={toggle}>Add New Issue Setting</ModalHeader>

          <ModalBody className="row">
            <div className="col-md-12 col-sm-12">
              <BaseInputField
                type="text"
                id="issue_value"
                name="issue_value"
                label="Issue Value"
                placeholder="Enter Issue Value"
                value={formik.values.issue_value}
                onChange={formik.handleChange}
                classNameInput={
                  formik.errors.issue_value && formik.touched.issue_value
                    ? "is-invalid"
                    : ""
                }
                important="true"
                onBlur={formik.handleBlur}
              />
              {formik.errors.issue_value && formik.touched.issue_value ? (
                <p className="errorMsg"> {formik.errors.issue_value} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div>

            <div className="col-md-12 col-sm-12 mt-3">
              <BaseTextArea
                formik={formik}
                label="Description"
                placeholder="Description"
                important="false"
                row={4}
              />
            </div>
            <div className="col-md-12 col-sm-12 mt-3">
              <BaseColorPicker />
            </div>
            <div className="col-md-6 col-sm-12 mt-1">
              <BaseCheckbox formik={formik} type="status" />
            </div>
          </ModalBody>
          <ModalFooter>
            <BaseButton
              className="ms-3"
              type="submit"
              value="Add New"
              color="secondary"
            />
          </ModalFooter>
        </form>
      </Modal>
    </>
  );
};
