import React from "react";
import { Table } from "antd";

const dataSource = [
  {
    key: "1",
    name: "John Doe",
    age: 30,
    address: "123 Main St",
  },
  {
    key: "2",
    name: "Jane Smith",
    age: 25,
    address: "456 Elm St",
  },
  // Add more data as needed
];

const columns = [
  {
    title: "Name",
    dataIndex: "name",
    key: "name",
    width: 150,
  },
  {
    title: "Age",
    dataIndex: "age",
    key: "age",
    width: 100,
  },
  {
    title: "Address",
    dataIndex: "address",
    key: "address",
    width: 200,
  },
];

const expandedRowRender = (record) => {
  return (
    <p style={{ overflowY: "scroll", height: "10px" }}>
      Details for {record.name}:<br />
      Age: {record.age}
      <br />
      Address: {record.address}
    </p>
  );
};

const MyTable = () => {
  return (
    <Table
      dataSource={dataSource}
      columns={columns}
      expandable={{ expandedRowRender }}
    />
  );
};

export default MyTable;
