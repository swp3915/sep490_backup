import React, { useEffect, useState } from "react";
import { ClassSettingsTable } from "./ClassSettingsTable/ClassSettingsTable";
import "./ClassSettings.scss";
import { Box, Grid } from "@mui/material";
import { axiosClient } from "src/axios/AxiosClient";
import { ConditionEnum } from "src/enum/Enum";
import { searchUtils } from "src/utils/handleSearchFilter";
import MyTable from "./ClassSettingsTable/MyTable";
import { DemoTable } from "./ClassSettingsTable/DemoTable";

export const ClassSettings = ({ classId }) => {
  const [loading, setLoading] = useState(false);
  const [isSearch, setIsSearch] = useState(false);
  const [classSettings, setClassSettings] = useState({
    totalRecord: 0,
    data: [],
    summary: "",
  });
  const filterConditions = [
    {
      field: "class_id",
      value: classId,
      condition: ConditionEnum.Equal,
    },
  ];
  const [searchParams, setSearchParams] = useState({
    pageNumber: 1,
    pageSize: 4,
    sortString: "",
    filterConditions: filterConditions,
  });

  const fetchData = async (searchParams) => {
    const { data: issueSettingArr } = await axiosClient.post(
      "/IssueSetting/GetByPaging",
      searchParams
    );
    setClassSettings(issueSettingArr);
    setLoading(false);
  };

  const [userPage, setUserPage] = useState({
    totalRecord: 0,
    data: [],
  });


  const onSearch = (filter) => {
    setIsSearch(true)
    searchUtils(filter, searchParams, setSearchParams, fetchData);
  };

  const handleReset = () => {
    setLoading(true);
    setIsSearch(false)
    let reLoad = {
      ...searchParams,
      filterConditions: [
        {
          field: "class_id",
          value: classId,
          condition: ConditionEnum.Equal,
        },
      ],
    };
    setSearchParams(reLoad);
    fetchData(reLoad);
  };

  const filterIssueGroup = (value) => {
    const filter = {
      field: "issue_group",
      value: value,
      condition: ConditionEnum.Equal,
    };
    filterConditions.push(filter);
    const newSearchParams = {
      ...searchParams,
      filterConditions: filterConditions,
    };
    setSearchParams(newSearchParams);
    // fetchData(newSearchParams);
    return newSearchParams;
    // console.log(newSearchParams);
  };

  const onPageChange = (pageNumber) => {
    const newSearchParams = { ...searchParams, pageNumber: pageNumber };
    setSearchParams(newSearchParams);
    fetchData(newSearchParams);
    console.log(pageNumber);
  };

  useEffect(() => {
    fetchData(searchParams);
  }, []);
  return (
    <Box className="d-flex flex-column flexGrow_1">
      <div className="card custom-card mb-0 flexGrow_1">
        <div className="card-body d-flex flex-column flexGrow_1">
          <div className="row d-flex flex-column">
            <div className="col-lg-7 col-md-3 my-auto d-flex">
              {/* <BaseSearch
              className="col-lg-9 col-md-8 p-0 m-0"
              placeholderInput="Search here..."
              placeholderSelect="Search by"
              options={searchUser}
              onSearch={onSearch}
              checkedSearchSelect={checkedSearchSelect}
              onResetSearchSelect={onResetSearchSelect}
              checkedSearchInput={checkedSearchInput}
              onResetSearchInput={onResetSearchInput}
            /> */}
            </div>
            <div className="col-lg-5 col-md-8 mt-sm-0 mt-2 position-relative align-items-center float-end">
              {/* <BaseButton
                nameTitle="my-auto ms-3 px-3 py-2 col-lg-3 col-md-3 mb-1 float-end addNewBtn"
                onClick={toggle}
                color="warning"
                value="Add New"
              /> */}
            </div>
          </div>

          <Grid container className="m-0 flexGrow_1">
            <Grid item md={12} xs={12} sm={12} className="d-flex flex-column ">
              <ClassSettingsTable
                onSearch={onSearch}
                classSettings={classSettings}
                fetchData={fetchData}
                searchParams={searchParams}
                classId={classId}
                handleReset={handleReset}
                loading={loading}
                isSearch={isSearch}
              />
              {/* <MyTable/> */}
            </Grid>
          </Grid>
        </div>
      </div>
    </Box>
  );
};
