import { Tooltip } from "antd";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router";
import { ToastContainer } from "react-toastify";
import { axiosClient } from "src/axios/AxiosClient";
import { NavbarDashboard } from "src/components/NavbarDashboard/NavbarDashboard";
import { ConditionEnum } from "src/enum/Enum";
import { ClassGeneral } from "./ClassGeneral/ClassGeneral";

export const ClassDetailsGeneral = () => {
  const { classId } = useParams();
  const navigate = useNavigate();
  const [classObj, setClassObj] = useState({});
  const [loading, setLoading] = useState(false);
  const [semesters, setSemesters] = useState([]);
  const [subjects, setSubjects] = useState([]);
  const [teachers, setTeachers] = useState([]);

  const fetchData = async () => {
    const { data: classById } = await axiosClient.get(`/Class/${classId}`);
    setClassObj(classById);
    setLoading(true);
  };

  const fetchDataSelect = async () => {
    const { data: systemSettingArr } = await axiosClient.post(
      `/Setting/GetFilterData?sortString=display_order ASC`,
      [
        {
          field: "data_group",
          value: "2",
          condition: ConditionEnum.Equal,
        },
      ]
    );
    setSemesters(systemSettingArr);

    const { data: subjectArr } = await axiosClient.post(
      "/Subject/GetFilterData?sortString=created_date ASC",
      []
    );
    setSubjects(subjectArr);

    const { data: roleList } = await axiosClient.post(
      `/Setting/GetFilterData?sortString=created_date ASC`,
      [
        {
          field: "setting_value",
          value: "Teacher",
          condition: ConditionEnum.Equal,
        },
      ]
    );

    const { data: userArr } = await axiosClient.post(
      "/User/GetFilterData?sortString=created_date ASC",
      [
        {
          field: "setting_id",
          value: roleList[0].setting_id,
          condition: ConditionEnum.Equal,
        },
      ]
    );
    setTeachers(userArr);

    setLoading(true);
  };

  useEffect(() => {
    fetchData();
    fetchDataSelect();
  }, []);
  return (
    <>
      <ToastContainer autoClose="2000" theme="colored" />
      <NavbarDashboard
        position="class"
        dashboardBody={
          <div className="d-flex flex-column flexGrow_1">
            <div className="col-xl-12">
              <div className="card custom-card">
                <div className="card-body p-0">
                  <div className="d-flex p-3 align-items-center justify-content-between">
                    <div>
                      <h6 className="fw-bold mb-0 title-setting">
                        Class Details
                      </h6>
                    </div>
                    <div>
                      <ul
                        className="nav nav-tabs nav-tabs-header mb-0 d-sm-flex d-block"
                        role="tablist"
                      >
                        <li className="nav-item m-1">
                          <a
                            className="nav-link pointer active"
                            onClick={() => {
                              navigate(`/class-details-general/${classId}`);
                            }}
                          >
                            General
                          </a>
                        </li>
                        <li className="nav-item pointer m-1">
                          <a
                            className="nav-link"
                            onClick={() => {
                              navigate(`/class-details-students/${classId}`);
                            }}
                          >
                            Students
                          </a>
                        </li>
                        <li className="nav-item pointer m-1">
                          <a
                            className="nav-link"
                            onClick={() => {
                              navigate(`/class-details-milestones/${classId}`);
                            }}
                          >
                            Milestones
                          </a>
                        </li>
                        <li className="nav-item pointer m-1">
                          <a
                            className="nav-link"
                            onClick={() => {
                              navigate(`/class-details-settings/${classId}`);
                            }}
                          >
                            Settings
                          </a>
                        </li>
                      </ul>
                    </div>
                    <Tooltip
                      title="Add new"
                      placement="top"
                      color="
                        #26BF94"
                      size="large"
                    >
                      <div className="btn-list"></div>
                    </Tooltip>
                  </div>
                </div>
              </div>
            </div>
            <div className="card custom-card mb-0 flexGrow_1">
              <div className="card-body d-flex flex-column">
                {loading ? (
                  <ClassGeneral
                    classObj={classObj}
                    semesters={semesters}
                    teachers={teachers}
                    subjects={subjects}
                    fetchData={fetchData}
                  />
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        }
      />
    </>
  );
};
