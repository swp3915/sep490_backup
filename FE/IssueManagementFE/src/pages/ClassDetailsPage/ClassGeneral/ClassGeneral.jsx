import { useFormik } from "formik";
import React, { useState } from "react";
import { useNavigate } from "react-router";
import { toast } from "react-toastify";
import { axiosClient } from "src/axios/AxiosClient";
import { BaseButton } from "src/components/Base/BaseButton/BaseButton";
import { BaseCheckbox } from "src/components/Base/BaseCheckbox/BaseCheckbox";
import { BaseInputField } from "src/components/Base/BaseInputField/BaseInputField";
import { SelectInputSetting } from "src/components/Base/BaseSelectInput/SelectInputSetting";
import { SelectInputSubject } from "src/components/Base/BaseSelectInput/SelectInputSubject";
import { SelectInputUser } from "src/components/Base/BaseSelectInput/SelectInputUser";
import { BaseTextArea } from "src/components/Base/BaseTextArea/BaseTextArea";
import { swalWithBootstrapButtons } from "src/enum/swal";
import * as Yup from "yup";

export const ClassGeneral = ({
  classObj,
  semesters,
  teachers,
  subjects,
  fetchData,
}) => {
  const navigate = useNavigate();
  //   const [classById, setClassById] = useState({});
  //   const fetchClassData = async () => {
  //     const { data: classById } = await axiosClient.get(
  //       `/Class/${classObj.class_id}`
  //     );
  //     setClassById(classById);
  //   };
  //   fetchClassData()
  const formik = useFormik({
    initialValues: { ...classObj },
    validationSchema: Yup.object({
      class_code: Yup.string()
        .required("Class Code is required")
        .max(100, "Class Code must be lower than 100 characters"),
      class_name: Yup.string()
        .required("Class Name is required")
        .max(255, "Class Name must be lower than 255 characters"),
      subject_id: Yup.string().required("Subject Code is required"),
      semester_id: Yup.string().required("Setting Value is required"),
      teacher_id: Yup.string().required("Teacher is required"),
    }),
    onSubmit: async (values) => {
      // window.alert("Form submitted");
      swalWithBootstrapButtons
        .fire({
          title: "Are you sure?",
          text: `Are you sure to update Class?`,
          icon: "warning",
          showCancelButton: true,
          confirmButtonText: "Yes, update it!",
          cancelButtonText: "No, cancel!",
          reverseButtons: true,
        })
        .then(async (result) => {
          if (result.isConfirmed) {
            const { data, err } = await axiosClient.put(
              `/Class/${values.class_id}`,
              values
            );
            if (err) {
              toast.error(err.response.data.Message);
              return;
            } else {
              toast.success("Add Successful!");
              fetchData();
              swalWithBootstrapButtons.fire(
                "Updated!",
                "User has been updated!.",
                "success"
              );
            }
          } else {
            {
              swalWithBootstrapButtons.fire(
                "Cancelled",
                "Your imaginary file is safe :)",
                "error"
              );
            }
          }
        });
    },
  });
  return (
    <>
      <form
        onSubmit={formik.handleSubmit}
        autoComplete="off"
        className="d-flex flex-column flexGrow_1"
      >
        <div className="flexGrow_1 ">
          <div className="row">
            <div className="col-md-6 col-sm-12">
              <BaseInputField
                type="text"
                id="class_code"
                name="class_code"
                label="Class Code"
                placeholder="Enter Class Code"
                value={formik.values.class_code}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                classNameInput={
                  formik.errors.class_code && formik.touched.class_code
                    ? "is-invalid"
                    : ""
                }
                important="true"
              />
              {formik.errors.class_code && formik.touched.class_code ? (
                <p className="errorMsg"> {formik.errors.class_code} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div>
            <div className="col-md-6 col-sm-12">
              <BaseInputField
                type="text"
                id="class_name"
                name="class_name"
                label="Class Name"
                placeholder="Enter Class Name"
                value={formik.values.class_name}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                classNameInput={
                  formik.errors.class_name && formik.touched.class_name
                    ? "is-invalid"
                    : ""
                }
                important="true"
              />
              {formik.errors.class_name && formik.touched.class_name ? (
                <p className="errorMsg"> {formik.errors.class_name} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div>
            <div className="col-md-6 col-sm-12">
              <SelectInputSetting
                label="Semester"
                id="semester_id"
                defaultValue={formik.values.semester_name}
                onBlur={formik.handleBlur}
                placeholder="Semester"
                options={semesters}
                status={
                  formik.errors.semester_id && formik.touched.semester_id
                    ? "error"
                    : ""
                }
                onChange={formik.handleChange}
                important="true"
                formik={formik}
              />
              {formik.errors.semester_id && formik.touched.semester_id ? (
                <p className="errorMsg"> {formik.errors.semester_id} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div>
            <div className="col-md-6 col-sm-12">
              <SelectInputSubject
                label="Subject Code"
                id="subject_id"
                defaultValue={formik.values.subject_code}
                onBlur={formik.handleBlur}
                status={
                  formik.errors.subject_id && formik.touched.subject_id
                    ? "error"
                    : ""
                }
                placeholder="Subject Code"
                options={subjects}
                onChange={formik.handleChange}
                important="true"
                formik={formik}
              />
              {formik.errors.subject_id && formik.touched.subject_id ? (
                <p className="errorMsg"> {formik.errors.subject_id} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div>
            <div className="col-md-12 col-sm-12">
              <SelectInputUser
                label="Teacher"
                id="teacher_id"
                placeholder="Teacher"
                defaultValue={formik.values.teacher_name}
                options={teachers}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                status={
                  formik.errors.teacher_id && formik.touched.teacher_id
                    ? "error"
                    : ""
                }
                important="true"
                isFilter={false}
                formik={formik}
              />
              {formik.errors.teacher_id && formik.touched.teacher_id ? (
                <p className="errorMsg"> {formik.errors.teacher_id} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div>
            <div className="col-md-12 col-sm-12 mt-1">
              <BaseTextArea
                formik={formik}
                label="Description"
                placeholder="Description"
                important="false"
                row="5"
              />
            </div>

            <div className="col-md-6 col-sm-12 mt-1">
              {/* <BaseRadio label="Status" important="true" formik={formik} type="status"/> */}
              <BaseCheckbox formik={formik} type="status" />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <BaseButton
              nameTitle="float-end mt-4"
              type="submit"
              value="Update"
              color="secondary"
            />
            <BaseButton
              nameTitle="float-end mt-4 me-3"
              type="button"
              value="Back"
              color="dark"
              onClick={() => {
                navigate(-1);
              }}
            />
          </div>
        </div>
      </form>
    </>
  );
};
