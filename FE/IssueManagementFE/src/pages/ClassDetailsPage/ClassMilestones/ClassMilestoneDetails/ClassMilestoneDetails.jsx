import { useFormik } from "formik";
import { useState } from "react";
import { toast } from "react-toastify";
import { Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { axiosClient } from "src/axios/AxiosClient";
import { BaseButton } from "src/components/Base/BaseButton/BaseButton";
import { BaseCheckbox } from "src/components/Base/BaseCheckbox/BaseCheckbox";
import { BaseInputField } from "src/components/Base/BaseInputField/BaseInputField";
import { BaseRangePicker } from "src/components/Base/BaseRangePicker/BaseRangePicker";
import { SelectInputClass } from "src/components/Base/BaseSelectInput/SelectInputClass";
import { BaseTextArea } from "src/components/Base/BaseTextArea/BaseTextArea";
import { swalWithBootstrapButtons } from "src/enum/swal";
import * as Yup from "yup";

export const ClassMilestoneDetails = ({
  searchParams,
  classId,
  milestone,
  fetchData,
  classes,
}) => {
  const formik = useFormik({
    initialValues: {
      ...milestone,
    },
    validationSchema: Yup.object({
      milestone_name: Yup.string()
        .required("Milestone Name is required")
        .max(255, "Must be less than 255 characters"),
    }),
    onSubmit: async (values) => {
      console.log(values);
      swalWithBootstrapButtons
        .fire({
          title: "Are you sure?",
          text: "Are you sure to update class milestone?",
          icon: "warning",
          showCancelButton: true,
          reverseButtons: true,
          confirmButtonText: "Yes,update it!",
          cancelButtonText: "No, cancel!",
        })
        .then(async (result) => {
          console.log(result);
          if (result.isConfirmed) {
            const { err } = await axiosClient.put(
              `/Milestone/${milestone.milestone_id}`,
              values
            );

            if (err) {
              toast.error("Update fail!");
            } else {
              toast.success("Update successfully!");
            }

            setModal(!modal);
            fetchData(searchParams);

            console.log("values", values);
          }
        });
    },
  });
  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);
  return (
    <>
      {console.log(milestone)}
      {/* {console.log(classes)} */}
      <BaseButton
        color="secondary"
        variant="outline"
        value="View Details"
        nameTitle="float-end py-1 px-2"
        onClick={toggle}
      />

      <Modal isOpen={modal} toggle={toggle} size="lg" centered>
        <form onSubmit={formik.handleSubmit} autoComplete="off">
          <ModalHeader toggle={toggle}>Subject Assignment Details</ModalHeader>

          <ModalBody className="row">
            <div className="col-md-12 col-sm-12">
              <BaseInputField
                type="text"
                label="Milestone Name"
                value={formik.values.milestone_name}
                placeholder="Enter Milestone Name"
                classNameInput={
                  formik.errors.milestone_name && formik.touched.milestone_name
                    ? "is-invalid"
                    : ""
                }
                important="true"
                formik={formik}
                isRandom={false}
                onChange={formik.handleChange}
                id="milestone_name"
                readOnly={false}
                onBlur={formik.handleBlur}
              />
              {formik.errors.milestone_name && formik.touched.milestone_name ? (
                <p className="errorMsg"> {formik.errors.milestone_name} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div>
            <div className="col-md-6 col-sm-12">
              <SelectInputClass
                label="Class Code"
                id="class_id"
                defaultValue={formik.values.class_code}
                onBlur={formik.handleBlur}
                status={
                  formik.errors.class_id && formik.touched.class_id
                    ? "error"
                    : ""
                }
                placeholder="Class Code"
                options={classes}
                onChange={formik.handleChange}
                important="true"
                formik={formik}
                disabled={true}
              />
              {formik.errors.class_id && formik.touched.class_id ? (
                <p className="errorMsg"> {formik.errors.class_id} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div>
            <div className="col-md-6 col-sm-12">
              <SelectInputClass
                label="Class Name"
                id="class_id"
                defaultValue={formik.values.class_name}
                onBlur={formik.handleBlur}
                status={
                  formik.errors.class_id && formik.touched.class_id
                    ? "error"
                    : ""
                }
                placeholder="Class Name"
                options={classes}
                onChange={formik.handleChange}
                important="true"
                formik={formik}
                disabled={true}
              />
              {formik.errors.class_id && formik.touched.class_id ? (
                <p className="errorMsg"> {formik.errors.class_id} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div>
            <div className="col-md-12 col-sm-12 px-3">
              <BaseRangePicker
                id="fromTo_date"
                name="fromTo_date"
                className="w-100 px-2 datePicker"
                label="From Date - To Date"
                valueFromDate={formik.values.from_date}
                valueToDate={formik.values.to_date}
                important="true"
                value={formik.values.from_date}
                formik={formik}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                status={
                  formik.errors.from_date && formik.touched.from_date
                    ? "error"
                    : ""
                }
              />
              {formik.errors.from_date && formik.touched.from_date ? (
                <p className="errorMsg"> {formik.errors.from_date} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div>
            <div className="col-md-12 col-sm-12 ">
              <BaseTextArea
                formik={formik}
                label="Description"
                placeholder="Description"
                important="false"
                row={4}
              />
            </div>
            <div className="col-md-6 col-sm-12 mt-4">
              <BaseCheckbox formik={formik} type="status" />
            </div>
          </ModalBody>
          <ModalFooter>
            <BaseButton
              className="ms-3"
              type="submit"
              value="Update"
              color="secondary"
            />
          </ModalFooter>
        </form>
      </Modal>
    </>
  );
};
