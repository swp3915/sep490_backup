import { Progress } from "antd";
import React, { useEffect, useState } from "react";
import { axiosClient } from "src/axios/AxiosClient";
import { ConditionEnum } from "src/enum/Enum";
import { ClassMilestoneItem } from "./ClassMilestoneItem/ClassMilestoneItem";
import { NewClassMilestone } from "./NewClassMilestone/NewClassMilestone";
import "./ClassMilestones.scss";
import { BasePagination } from "src/components/Base/BasePagination/BasePagination";
import { BaseSearch } from "src/components/Base/BaseSearch/BaseSearch";
import { filterUtils, searchUtils } from "src/utils/handleSearchFilter";
import {
  FilterOutlined,
  LoadingOutlined,
  ReloadOutlined,
} from "@ant-design/icons";
import { FilterClassMilestone } from "./FilterClassMilestone/FilterClassMilestone";
const searchClassMilestones = [
  {
    id: "milestone_name",
    value: "Milestone Name",
  },
];
export const ClassMilestones = ({ classId }) => {
  const [dropdown, setDropdown] = useState(false);
  const [classes, setClasses] = useState([]);
  const [checkedSearchSelect, setCheckedSearchSelect] = useState(null);
  const [checkedSearchInput, setCheckedSearchInput] = useState(null);
  const [loading, setLoading] = useState(false);
  const [milestones, setMilestones] = useState({
    totalRecord: 0,
    data: [],
    summary: "",
  });
  const classFilter = {
    field: "class_id",
    value: classId,
    condition: ConditionEnum.Equal,
  };
  const [searchParams, setSearchParams] = useState({
    pageNumber: 1,
    pageSize: 3,
    sortString: "from_date ASC",
    filterConditions: [classFilter],
  });
  const onPageChange = (pageNumber) => {
    const newSearchParams = { ...searchParams, pageNumber: pageNumber };
    setSearchParams(newSearchParams);
    fetchData(newSearchParams);
    // console.log(pageNumber);
  };

  const onReset = () => {
    const newSearchParams = {
      ...searchParams,
      filterConditions: [
        {
          field: "class_id",
          value: classId,
          condition: ConditionEnum.Equal,
        },
      ],
    };
    setSearchParams(newSearchParams);
    setCheckedSearchSelect(null);
    setCheckedSearchInput(null);
    fetchData(newSearchParams);
  };

  const onSearch = (filter) => {
    searchUtils(filter, searchParams, setSearchParams, fetchData);
  };

  const onFilter = (filter) => {
    filterUtils(filter, searchParams, setSearchParams, fetchData);
  };

  const onResetSearchSelect = (value) => {
    setCheckedSearchSelect(value);
  };

  const onResetSearchInput = (value) => {
    setCheckedSearchInput(value);
  };

  const fetchData = async (searchParams) => {
    const { data: milestoneArr } = await axiosClient.post(
      "/Milestone/GetByPaging",
      searchParams
    );
    setMilestones(milestoneArr);
    setLoading(false);
  };
  // const fetchDataSelect = async (values) => {
  //   const { data: classArr } = await axiosClient.get(
  //     `/Class/${values.class_id}`,
  //     values
  //   );
  //   setClasses(classArr);
  //   console.log(classArr);
  // };

  useEffect(() => {
    fetchData(searchParams);
    // fetchDataSelect();
  }, []);
  return (
    <div className="d-flex flex-column flexGrow_1">
      <div className="card custom-card mb-0 flexGrow_1">
        <div className="card-body class__timeline d-flex flex-column">
          <div className="row">
            <div className="col-lg-7 col-md-3 my-auto">
              <BaseSearch
                className="col-lg-9 col-md-8 p-0 m-0"
                placeholderInput="Search here..."
                placeholderSelect="Search by"
                options={searchClassMilestones}
                onSearch={onSearch}
                checkedSearchSelect={checkedSearchSelect}
                onResetSearchSelect={onResetSearchSelect}
                checkedSearchInput={checkedSearchInput}
                onResetSearchInput={onResetSearchInput}
              />
            </div>
            <div className="col-lg-5 col-md-8 mt-sm-0 mt-2 position-relative align-items-center float-end">
              <NewClassMilestone
                classId={classId}
                fetchData={fetchData}
                searchParams={searchParams}
              />
              <div className="col-lg-7 float-end me-5 d-flex h-100 justify-content-end">
                <FilterOutlined
                  className={
                    dropdown === true
                      ? "filterIcon iconActive position-relative my-auto float-end me-3"
                      : "filterIcon position-relative my-auto float-end me-3"
                  }
                  onClick={() => setDropdown(!dropdown)}
                />
                {loading ? (
                  <LoadingOutlined
                    className="filterIcon ms-4 float-end"
                    disabled
                  />
                ) : (
                  <ReloadOutlined
                    className="filterIcon ms-4 float-end"
                    onClick={() => {
                      setLoading(true);
                      let reLoad = {
                        ...searchParams,
                        filterConditions: [classFilter],
                      };
                      setSearchParams(reLoad);
                      fetchData(reLoad);
                    }}
                  />
                )}
              </div>

              <div
                // onBlur={() => setDropdown(false)}
                className={
                  dropdown === false
                    ? "position-absolute cardDropdown bottom-0 end-0 shadow d-none"
                    : "position-absolute cardDropdown bottom-0 end-0 shadow"
                }
              >
                <div className="card custom-card mb-0">
                  <div className="card-body filterCard">
                    <FilterClassMilestone
                      classId={classId}
                      onFilter={onFilter}
                      fetchData={fetchData}
                      searchParams={searchParams}
                      onReset={onReset}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <ul
            className="timeline list-unstyled flexGrow_1"
            style={{ right: "0% !important" }}
          >
            {milestones.data.map((milestone) => (
              <ClassMilestoneItem
                key={milestone.milestone_id}
                milestone={milestone}
                classes={classes}
                classId={classId}
                fetchData={fetchData}
                searchParams={searchParams}
              />
            ))}
          </ul>
          <BasePagination
            pageNumber={searchParams.pageNumber}
            onPageChange={(pageNumber) => {
              // setSearchParams({ ...searchParams, pageNumber: pageIndex });
              // console.log(searchParams, pageIndex);
              onPageChange(pageNumber);
            }}
            pageSize={searchParams.pageSize}
            totalRecord={milestones.totalRecord}
          />
        </div>
      </div>
    </div>
  );
};
