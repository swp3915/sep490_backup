import { DatePicker } from "antd";
import { useFormik } from "formik";
import { useState } from "react";
import { toast } from "react-toastify";
import { Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { axiosClient } from "src/axios/AxiosClient";
import { BaseButton } from "src/components/Base/BaseButton/BaseButton";
import { BaseCheckbox } from "src/components/Base/BaseCheckbox/BaseCheckbox";
import { BaseRangePicker } from "src/components/Base/BaseRangePicker/BaseRangePicker";
// import { BaseDatePicker } from "src/components/Base/BaseDatePicker/BaseDatePicker";
import { BaseInputField } from "src/components/Base/BaseInputField/BaseInputField";
import { BaseTextArea } from "src/components/Base/BaseTextArea/BaseTextArea";
import { swalWithBootstrapButtons } from "src/enum/swal";
import * as Yup from "yup";
// const { RangePicker } = DatePicker;

export const NewClassMilestone = ({ classId, fetchData, searchParams }) => {
  const formik = useFormik({
    initialValues: {
      milestone_name: "",
      from_date: "",
      to_date: "",
      status: 0,
      class_id: classId,
      description: "",
    },
    validationSchema: Yup.object({
      milestone_name: Yup.string()
        .required("Milestone Name is required")
        .max(255, "Must be less than 255 characters"),
      // from_date: Yup.string().required("From Date is required"),
      // to_date: Yup.string().required("To Date is required"),
      // fromTo_date: Yup.string().required("To Date is required"),
      // project_id: Yup.string().required("Project Code is required"),
    }),
    onSubmit: async (values) => {
      // window.alert("Form submitted");
      swalWithBootstrapButtons
        .fire({
          title: "Are you sure?",
          text: "Are you sure to add new class milestone?",
          icon: "warning",
          showCancelButton: true,
          confirmButtonText: "Yes, add it!",
          cancelButtonText: "No, cancel!",
          reverseButtons: true,
        })
        .then(async (result) => {
          console.log(result);
          if (result.isConfirmed) {
            const { data, err } = await axiosClient.post(`/Milestone`, values);
            if (err) {
              toast.error("Add fail!");
              return;
            } else {
              toast.success("Add Successful!");
              swalWithBootstrapButtons.fire(
                "Updated!",
                "User has been updated!.",
                "success"
              );
              formik.resetForm();
              setModal(!modal);
              fetchData(searchParams);
            }
          } else {
            {
              swalWithBootstrapButtons.fire(
                "Cancelled",
                "Your imaginary file is safe :)",
                "error"
              );
            }
          }
        });

      console.log(searchParams, values);
    },
  });
  const [swalProps, setSwalProps] = useState({});
  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);
  return (
    <>
      <BaseButton
        nameTitle="my-auto ms-3 px-3 py-2 col-lg-3 col-md-3 mb-1 float-end"
        onClick={toggle}
        color="warning"
        value="Add New"
      />
      <Modal isOpen={modal} toggle={toggle} size="lg" centered>
        <form onSubmit={formik.handleSubmit} autoComplete="off">
          <ModalHeader toggle={toggle}>Add New Milestone</ModalHeader>
          <ModalBody className="row">
            <div className="col-md-12 col-sm-12 px-3">
              <BaseInputField
                type="text"
                label="Milestone Name"
                value={formik.values.milestone_name}
                placeholder="Enter Milestone Name"
                classNameInput={
                  formik.errors.milestone_name && formik.touched.milestone_name
                    ? "is-invalid"
                    : ""
                }
                important="true"
                formik={formik}
                isRandom={false}
                onChange={formik.handleChange}
                id="milestone_name"
                readOnly={false}
                onBlur={formik.handleBlur}
              />
              {formik.errors.milestone_name && formik.touched.milestone_name ? (
                <p className="errorMsg"> {formik.errors.milestone_name} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div>
            <div className="col-md-12 col-sm-12 px-3">
              <BaseRangePicker
                id="fromTo_date"
                name="fromTo_date"
                className="w-100 px-2 datePicker"
                label="From Date - To Date"
                valueFromDate={formik.values.from_date}
                valueToDate={formik.values.to_date}
                important="true"
                value={formik.values.from_date}
                formik={formik}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                status={
                  formik.errors.from_date && formik.touched.from_date
                    ? "error"
                    : ""
                }
              />
              {formik.errors.from_date && formik.touched.from_date ? (
                <p className="errorMsg"> {formik.errors.from_date} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div>
            {/* <div className="col-md-6 col-sm-12 px-3">
              <BaseDatePicker
                id="from_date"
                name="from_date"
                className="w-100 px-2 datePicker"
                label="From Date - To Date"
                important="true"
                value={formik.values.from_date}
                formik={formik}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                status={
                  formik.errors.from_date && formik.touched.from_date
                    ? "error"
                    : ""
                }
              />
              {formik.errors.from_date && formik.touched.from_date ? (
                <p className="errorMsg"> {formik.errors.from_date} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div>
            <div className="col-md-6 col-sm-12 px-3">
              <BaseDatePicker
                id="to_date"
                name="to_date"
                className="w-100 px-2 datePicker"
                label="From Date - To Date"
                important="true"
                value={formik.values.to_date}
                formik={formik}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                status={
                  formik.errors.to_date && formik.touched.to_date ? "error" : ""
                }
              />
              {formik.errors.to_date && formik.touched.to_date ? (
                <p className="errorMsg"> {formik.errors.to_date} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div> */}
            <div className="col-md-12 col-sm-12 px-3">
              <BaseTextArea
                formik={formik}
                label="Description"
                placeholder="Description"
                important="false"
                row="4"
              />
            </div>
            <div className="col-md-6 col-sm-12 mt-1 px-3">
              {/* <BaseRadio label="Status" important="true" formik={formik} type="status"/> */}
              <BaseCheckbox formik={formik} type="status" />
            </div>
          </ModalBody>
          <ModalFooter>
            <BaseButton
              type="reset"
              value="Reset"
              color="dark"
              onClick={() => {
                formik.resetForm();
                setModal(!modal);
              }}
            />
            <BaseButton
              nameTitle="ms-3 me-0"
              type="submit"
              value="Add New"
              color="secondary"
            />
          </ModalFooter>
        </form>
      </Modal>
    </>
  );
};
