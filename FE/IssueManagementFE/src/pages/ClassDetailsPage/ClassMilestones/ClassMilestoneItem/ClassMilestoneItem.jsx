import { Progress } from "antd";
import React from "react";
import { toast } from "react-toastify";
import { axiosClient } from "src/axios/AxiosClient";
import { BaseBadge } from "src/components/Base/BaseBadge/BaseBadge";
import { BaseButton } from "src/components/Base/BaseButton/BaseButton";
import { StatusEnum } from "src/enum/Enum";
import { swalWithBootstrapButtons } from "src/enum/swal";
import { ClassMilestoneDetails } from "../ClassMilestoneDetails/ClassMilestoneDetails";

export const ClassMilestoneItem = ({
  milestone,
  fetchData,
  searchParams,
  classId,
  classes,
}) => {
  const handleDate = (inputDate) => {
    const date = new Date(inputDate);
    const options = { year: "numeric", month: "short", day: "numeric" };
    const formattedDate = date.toLocaleDateString("en-US", options);
    return formattedDate;
  };

  const handleDay = (inputDate) => {
    const date = new Date(inputDate);
    const options = { weekday: "long" };
    const formattedDay = date
      .toLocaleDateString("en-US", options)
      .toUpperCase();
    return formattedDay;
  };

  const handleHour = (inputDate) => {
    const date = new Date(inputDate);
    const formattedTime = date.toLocaleTimeString("en-US", {
      hour: "2-digit",
      minute: "2-digit",
      hour12: false,
    });
    return formattedTime;
  };

  function handleToDDMM(inputDate) {
    // Create a Date object from the ISO date
    const date = new Date(inputDate);

    // Get the day, month, and year from the Date object
    const day = date.getDate();
    const month = date.getMonth() + 1; // Note: Months are zero-based

    // Create formatted strings for day and month with leading zeros
    const formattedDay = day < 10 ? `0${day}` : day;
    const formattedMonth = month < 10 ? `0${month}` : month;

    // Create the "DD/MM" format string
    const formattedDate = `${formattedDay}/${formattedMonth}`;

    return formattedDate;
  }

  const handleChangeStatus = async (optionId, status) => {
    const userIdArr = [];
    userIdArr.push(optionId);
    swalWithBootstrapButtons
      .fire({
        title: "Are you sure?",
        text: `Are you sure to update Class Milestone Status?`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, update it!",
        cancelButtonText: "No, cancel!",
        reverseButtons: true,
      })
      .then(async (result) => {
        console.log(result);
        if (result.isConfirmed) {
          const { data, err } = await axiosClient.post(
            `/Milestone/UpdateStatus?status=${status}`,
            userIdArr
          );

          swalWithBootstrapButtons.fire(
            "Updated!",
            "User has been updated!.",
            "success"
          );
          if (err) {
            toast.error("Change fail!");
            return;
          } else {
            toast.success("Change Successful!");
            fetchData(searchParams);
          }
        } else {
          {
            swalWithBootstrapButtons.fire(
              "Cancelled",
              "Your imaginary file is safe :)",
              "error"
            );
          }
        }
      });
  };
  return (
    <>
      <li className="p-0 d-flex flex-colum">
        <div className="timeline-time text-end">
          <span className="date">{handleDay(milestone.from_date)}</span>
          <span className="time d-inline-block">
            {handleToDDMM(milestone.from_date)}
          </span>
        </div>
        <div className="timeline-icon">
          <a href="#"></a>
        </div>

        <div
          className="timeline-body pe-0 shadow mt-2"
          style={{ width: "92%", top: "0%" }}
        >
          <div className="d-flex align-items-top timeline-main-content flex-wrap mt-0">
            <div className="flex-fill">
              <div className="row align-items-center">
                <div className="col-5 mt-sm-0 mt-2">
                  <p className="mb-0 fs-14 fw-semibold">
                    Milestone Name: <strong>{milestone.milestone_name}</strong>
                  </p>
                  <p className="mb-0 fs-13 text-muted ">
                    {handleDate(milestone.from_date)} -{" "}
                    {handleDate(milestone.to_date)}
                  </p>
                  {milestone.status === StatusEnum.Inactive ? (
                    <BaseBadge
                      bageName="Inactive"
                      color="danger"
                      roundedPill="fw-semibold float-start mt-1"
                    />
                  ) : (
                    ""
                  )}
                  {milestone.status === StatusEnum.Active ? (
                    <BaseBadge
                      bageName="Active"
                      color="success"
                      roundedPill="fw-semibold float-start mt-1"
                    />
                  ) : (
                    ""
                  )}
                </div>
                <Progress
                  percent={30}
                  size="small"
                  strokeColor="#ec8550f0"
                  className="float-end mt-1 fs-10 fw-semibold col-5 mx-0"
                />
                <div className="col-2 row">
                  <div className="col-12 px-0">
                    {/* <span className="float-end badge bg-light text-muted timeline-badge mb-4">
                    30,Sep 2022
                  </span> */}
                    {milestone.status === StatusEnum.Inactive ? (
                      <BaseButton
                        color="dark"
                        variant="outline"
                        value="Open Milestone"
                        nameTitle="float-end mb-3 py-1 px-2"
                        onClick={() => {
                          handleChangeStatus(
                            milestone.milestone_id,
                            StatusEnum.Active
                          );
                        }}
                      />
                    ) : (
                      ""
                    )}
                    {milestone.status === StatusEnum.Active ? (
                      <BaseButton
                        color="dark"
                        variant="outline"
                        value="Close Milestone"
                        nameTitle="float-end mb-3 py-1 px-2"
                        onClick={() => {
                          handleChangeStatus(
                            milestone.milestone_id,
                            StatusEnum.Inactive
                          );
                        }}
                      />
                    ) : (
                      ""
                    )}
                  </div>
                  <div className="col-12 px-0">
                    <ClassMilestoneDetails
                      milestone={milestone}
                      classes={classes}
                      classId={classId}
                      fetchData={fetchData}
                      searchParams={searchParams}
                    />
                  </div>
                </div>
                <div className="position-absolute bottom-0 end-0 me-3 mb-2 col-3"></div>
              </div>
            </div>
          </div>
        </div>
      </li>
    </>
  );
};
