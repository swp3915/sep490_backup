import { Tabs } from "antd";
import { useFormik } from "formik";
import React, { useEffect, useState } from "react";
import { Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { BaseButton } from "src/components/Base/BaseButton/BaseButton";
import { BaseCheckbox } from "src/components/Base/BaseCheckbox/BaseCheckbox";
import { BaseInputField } from "src/components/Base/BaseInputField/BaseInputField";
import { SelectInputSetting } from "src/components/Base/BaseSelectInput/SelectInputSetting";
import { BaseTextArea } from "src/components/Base/BaseTextArea/BaseTextArea";
import { GeneratePassword } from "src/pages/UserListPage/components/GeneratePassword/GeneratePassword";
import * as Yup from "yup";
import { NewClassStudentsTable } from "./NewClassStudentsTable/NewClassStudentsTable";
import { axiosClient } from "src/axios/AxiosClient";
import { ConditionEnum } from "src/enum/Enum";

export const NewClassStudentExist = ({ modal, toggle, classId, fetchData, searchParams }) => {
  const [students, setStudents] = useState({
    totalRecord: 0,
    data: [],
    summary: "",
  });

  const [subjectsParams, setSubjectsParams] = useState({
    pageNumber: 1,
    pageSize: 8,
    sortString: "",
    filterConditions: [
      {
        field: "class_id",
        value: classId,
        condition: ConditionEnum.NotIn,
      },
    ],
  });
  const fetchDataSubject = async (subjectsParams) => {
    const { data: studentArr } = await axiosClient.post(
      "/ClassStudent/GetStudents",
      subjectsParams
    );
    setStudents(studentArr);

    // for (let student of studentArr.data) {
    //   const { data: studentArray } = await axiosClient.get(
    //     `/User/${student.student_id}`
    //   );
    //   setUsers(users.push(studentArray));
    // }
    // console.log(students, studentArr);
  };
  const onPageChange = (pageNumber) => {
    const newSearchParams = { ...subjectsParams, pageNumber: pageNumber };
    setSubjectsParams(newSearchParams);
    fetchDataSubject(newSearchParams);
    console.log(pageNumber);
  };
  useEffect(() => {
    fetchDataSubject(subjectsParams);
  }, []);

  return (
    <>
      <div>
        <Modal isOpen={modal} toggle={toggle} size="xl" centered>
          {/* <Tabs defaultActiveKey="1" items={items} onChange={onChange} className="px-3"/> */}
          <form autoComplete="off">
            <ModalHeader toggle={toggle}>Add New Student Exist</ModalHeader>

            <ModalBody>
              <div className="row">
                <NewClassStudentsTable
                  classId={classId}
                  students={students}
                  onPageChange={onPageChange}
                  searchParams={searchParams}
                  fetchData={fetchData}
                  subjectsParams={subjectsParams}
                  fetchDataSubject={fetchDataSubject}
                  toggle={toggle}
                />
              </div>
            </ModalBody>
            {/* <ModalFooter>
              <BaseButton type="reset" value="Reset" color="dark" />
              <BaseButton
                nameTitle="ms-3"
                type="submit"
                value="Add New"
                color="secondary"
              />
            </ModalFooter> */}
          </form>
        </Modal>
      </div>
    </>
  );
};
