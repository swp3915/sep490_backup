import { Tabs } from "antd";
import { useFormik } from "formik";
import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { axiosClient } from "src/axios/AxiosClient";
import { BaseButton } from "src/components/Base/BaseButton/BaseButton";
import { BaseCheckbox } from "src/components/Base/BaseCheckbox/BaseCheckbox";
import { BaseInputField } from "src/components/Base/BaseInputField/BaseInputField";
import { SelectInputSetting } from "src/components/Base/BaseSelectInput/SelectInputSetting";
import { BaseTextArea } from "src/components/Base/BaseTextArea/BaseTextArea";
import { ConditionEnum, UserEnum } from "src/enum/Enum";
import { swalWithBootstrapButtons } from "src/enum/swal";
import { GeneratePassword } from "src/pages/UserListPage/components/GeneratePassword/GeneratePassword";
import * as Yup from "yup";

const items = [
  {
    key: "1",
    label: "Tab 1",
    children: "Content of Tab Pane 1",
  },
  {
    key: "2",
    label: "Tab 2",
    children: "Content of Tab Pane 2",
  },
];
export const NewClassStudent = ({
  modal,
  toggle,
  classId,
  fetchData,
  searchParams,
}) => {
  // const [modal, setModal] = useState(false);

  // const toggle = () => setModal(!modal);
  const [role, setRole] = useState(null);
  const [roleParam, setRoleParam] = useState([
    {
      field: "setting_value",
      value: "Student",
      condition: ConditionEnum.Equal,
    },
  ]);
  const onChange = (key) => {
    console.log(key);
  };
  let settingId = null;
  useEffect(() => {
    const fetchData = async () => {
      const { data: roleArr } = await axiosClient.post(
        "/Setting/GetFilterData?sortString=order by display_order ASC",
        [
          {
            field: "setting_value",
            value: "Student",
            condition: ConditionEnum.Equal,
          },
        ]
      );

      setRole(roleArr);
      // console.log("role", role);
      // console.log(role[0].setting_id);
      // formik.setFieldValue("setting_id", role[0].setting_id);
    };
    fetchData();
  }, []);
  const formik = useFormik({
    initialValues: {
      email: "",
      fullname: "",
      password: "Dung1881@",
      phone_number: "",
      note: "",
      setting_id: "",
      status: 1,
      action: UserEnum.Add,
    },
    validationSchema: Yup.object({
      fullname: Yup.string()
        .required("Fullname is required")
        .min(4, "Must be 4 characters or more"),
      email: Yup.string()
        .required("Email is required")
        .matches(
          /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/,
          "Please enter a valid email address"
        ),
      phone_number: Yup.string()
        .nullable()
        .test(
          "is-valid-postal-code",
          "Phone must be a valid phone number and have 10 digits",
          function (value) {
            if (value && value.trim() !== null) {
              const postalCodePattern =
                /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/;
              if (!postalCodePattern.test(value)) {
                return false; // Validation failed
              }
            }
            return true;
          }
        ),
    }),
    onSubmit: async (values) => {
      swalWithBootstrapButtons
        .fire({
          title: "Are you sure?",
          text: "Are you sure to add new user?",
          icon: "warning",
          showCancelButton: true,
          confirmButtonText: "Yes, add it!",
          cancelButtonText: "No, cancel!",
          reverseButtons: true,
        })
        .then(async (result) => {
          if (result.isConfirmed) {
            const newValue = {
              ...values,
              setting_id: role[0].setting_id,
            };
            const { data: user, err } = await axiosClient.post(
              `/User`,
              newValue
            );
            if (err) {
              window.alert(err.response.data.Message);
            } else {
              // window.alert("add");
              const { data: userByEmail } = await axiosClient.post(
                "/User/GetFilterData?sortString=order by created_date ASC",
                [
                  {
                    field: "email",
                    value: newValue.email,
                    condition: ConditionEnum.Equal,
                  },
                ]
              );
              const { data, err } = await axiosClient.post("/ClassStudent", {
                student_id: userByEmail[0].user_id,
                class_id: classId,
              });
              if (!err) {
                toast.success("Add Successful!");
                swalWithBootstrapButtons.fire(
                  "Updated!",
                  "User has been added!.",
                  "success"
                );
                formik.resetForm();
                toggle();
                fetchData(searchParams);
              }
            }
          }
        });
    },
  });

  return (
    <>
      <div>
        <Modal isOpen={modal} toggle={toggle} size="lg" centered>
          {/* <Tabs defaultActiveKey="1" items={items} onChange={onChange} className="px-3"/> */}
          <form autoComplete="off" onSubmit={formik.handleSubmit}>
            <ModalHeader toggle={toggle}>Add New Student</ModalHeader>

            <ModalBody>
              <div className="row">
                <div className="col-md-6 col-sm-12">
                  <BaseInputField
                    type="text"
                    id="fullname"
                    name="fullname"
                    label="Full name"
                    onBlur={formik.handleBlur}
                    placeholder="Enter Full name"
                    value={formik.values.fullname}
                    onChange={formik.handleChange}
                    classNameInput={
                      formik.errors.fullname && formik.touched.fullname
                        ? "is-invalid"
                        : ""
                    }
                    important="true"
                  />
                  {formik.errors.fullname && formik.touched.fullname ? (
                    <p className="errorMsg"> {formik.errors.fullname} </p>
                  ) : (
                    <p className="hiddenMsg">acb</p>
                  )}
                </div>
                <div className="col-md-6 col-sm-12">
                  <BaseInputField
                    type="text"
                    id="email"
                    name="email"
                    label="Email"
                    placeholder="Enter Email"
                    onBlur={formik.handleBlur}
                    value={formik.values.email}
                    onChange={formik.handleChange}
                    classNameInput={
                      formik.errors.email && formik.touched.email
                        ? "is-invalid"
                        : ""
                    }
                    important="true"
                  />
                  {formik.errors.email && formik.touched.email ? (
                    <p className="errorMsg"> {formik.errors.email} </p>
                  ) : (
                    <p className="hiddenMsg">acb</p>
                  )}
                </div>
                <div className="col-md-12 col-sm-12">
                  <BaseInputField
                    type="text"
                    id="phone_number"
                    label="Phone"
                    placeholder="Enter Phone"
                    onBlur={formik.handleBlur}
                    value={
                      formik.values.phone_number === null
                        ? ""
                        : formik.values.phone_number
                    }
                    onChange={formik.handleChange}
                    classNameInput={
                      formik.errors.phone_number && formik.touched.phone_number
                        ? "is-invalid"
                        : ""
                    }
                  />
                  {formik.errors.phone_number && formik.touched.phone_number ? (
                    <p className="errorMsg"> {formik.errors.phone_number} </p>
                  ) : (
                    <p className="hiddenMsg">acb</p>
                  )}
                </div>
                {/* <div className="col-md-6 col-sm-12">
                  <GeneratePassword
                    length="10"
                    uppercase={true}
                    lowercase={true}
                    numbers={true}
                    symbols={true}
                    placeholder="Enter Password"
                    classNameInput={
                      formik.errors.password && formik.touched.password
                        ? "is-invalid"
                        : ""
                    }
                    label="Password"
                    important="true"
                    isRandom={true}
                    formik={formik}
                  />
                  {formik.errors.password && formik.touched.password ? (
                    <p className="errorMsg"> {formik.errors.password} </p>
                  ) : (
                    <p className="hiddenMsg">acb</p>
                  )}
                </div> */}
                <div className="col-md-12 col-sm-12">
                  <BaseTextArea
                    formik={formik}
                    label="Note"
                    placeholder="Note"
                    important="false"
                    type="user"
                    row="4"
                  />
                </div>
                <div className="col-md-6 col-sm-12 mt-1">
                  {/* <BaseRadio label="Status" important="true" formik={formik} type="status"/> */}
                  <BaseCheckbox formik={formik} type="status" />
                </div>
              </div>
            </ModalBody>
            <ModalFooter>
              <BaseButton type="reset" value="Reset" color="dark" />
              <BaseButton
                nameTitle="ms-3"
                type="submit"
                value="Add New"
                color="secondary"
              />
            </ModalFooter>
          </form>
        </Modal>
      </div>
    </>
  );
};
