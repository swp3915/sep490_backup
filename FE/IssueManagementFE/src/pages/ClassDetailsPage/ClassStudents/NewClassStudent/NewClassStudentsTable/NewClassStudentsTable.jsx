import { Button, ConfigProvider, Table, Tooltip } from "antd";
import { useRef, useState } from "react";
import { toast } from "react-toastify";
import { axiosClient } from "src/axios/AxiosClient";
import { BasePagination } from "src/components/Base/BasePagination/BasePagination";
import { swalWithBootstrapButtons } from "src/enum/swal";
import { Status } from "src/pages/UserListPage/components/UserTable/Status/Status";
const sortOptions = [
  { key: "asc", value: "Price ASC" },
  { key: "desc", value: "Price DESC" },
];
export const NewClassStudentsTable = ({
  classId,
  students,
  searchParams,
  onPageChange,
  fetchData,
  subjectsParams,
  fetchDataSubject,
  toggle,
}) => {
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [loading, setLoading] = useState(false);
  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const searchInput = useRef(null);
  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };
  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText("");
  };
  const [userPage, setUserPage] = useState({
    totalRecord: 0,
    data: [],
  });

  const handleChangeStatus = async (userId, status) => {
    const { data, err } = await axiosClient.post(
      `/Class/UpdateStatus?status=${status}`,
      userId
    );
    if (err) {
      window.alert("Change fail!");
      return;
    } else {
      window.alert("Change Successful!");
      fetchData(searchParams);
    }
  };

  const handleAdd = (value, status) => {
    setLoading(true);
    // ajax request after empty completing
    setTimeout(() => {
      setSelectedRowKeys([]);
      setLoading(false);
      handleChangeStatus(value, status);
    }, 1000);
  };

  const onSelectChange = (newSelectedRowKeys) => {
    console.log("selectedRowKeys changed: ", newSelectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  const hasSelected = selectedRowKeys.length > 0;
  const handleAddMultiple = async () => {
    // window.alert("submit");
    swalWithBootstrapButtons
      .fire({
        title: "Are you sure?",
        text: "Are you sure to add new user?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, add it!",
        cancelButtonText: "No, cancel!",
        reverseButtons: true,
      })
      .then(async (result) => {
        if (result.isConfirmed) {
          const value = [];
          for (let select of selectedRowKeys) {
            value.push({ student_id: select, class_id: classId });
            console.log("value", value);
          }
          const { data, err } = await axiosClient.post(
            "/ClassStudent/InsertMultiple",
            value
          );
          if (err) {
            toast.error("Add fail!");
            return;
          } else {
            toast.success("Add Successful!");
            swalWithBootstrapButtons.fire(
              "Updated!",
              "User has been added!.",
              "success"
            );
            fetchData(searchParams);
            fetchDataSubject(subjectsParams);
            toggle();
          }
        } else {
          {
            swalWithBootstrapButtons.fire(
              "Cancelled",
              "Your imaginary file is safe :)",
              "error"
            );
          }
        }
      });
  };

  const data = [];
  for (let index = 0; index < students.data.length; index++) {
    const student = students.data[index];
    let i = index + 1;
    data.push({
      key: student.user_id,
      order: (searchParams.pageNumber - 1) * searchParams.pageSize + i,
      fullname: student.fullname,
      email: student.email,
      phone_number: student.phone_number,
      status: <Status status={student.status} />,
    });
  }
  const columns = [
    {
      title: "#",
      dataIndex: "order",
      key: "order",
      width: "5%",
      fixed: "left",
      align: "center",
      sorter: (a, b) => a.order - b.order,
    },
    {
      title: "Student Name",
      dataIndex: "fullname",
      key: "fullname",
      width: "25%",
      sorter: (a, b) => a.fullname.length - b.fullname.length,
      render: (fullname) => (
        <Tooltip placement="topLeft" title={fullname}>
          {fullname}
        </Tooltip>
      ),
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
      width: "30%",
      sorter: (a, b) => a.email.length - b.email.length,
      render: (email) => (
        <Tooltip placement="topLeft" title={email}>
          {email}
        </Tooltip>
      ),
    },
    {
      title: "Phone",
      dataIndex: "phone_number",
      key: "phone_number",
      width: "20%",
      sorter: (a, b) => a.phone_number.length - b.phone_number.length,
      render: (phone_number) => (
        <Tooltip placement="topLeft" title={phone_number}>
          {phone_number}
        </Tooltip>
      ),
    },
    {
      title: "Status",
      dataIndex: "status",
      key: "status",
      align: "center",
      width: "15%",
      sorter: (a, b) => a.status.length - b.status.length,
      render: (status) => (
        <Tooltip placement="topLeft" title={status}>
          {status}
        </Tooltip>
      ),
    },

    // {
    //   title: "Status",
    //   dataIndex: "status",
    //   key: "status",
    //   width: "10%",
    //   align: "center",
    //   // sorter: (a, b) => a.status - b.status,
    // },
  ];

  return (
    <div>
      <div>
        <Button
          type="primary"
          onClick={handleAddMultiple}
          disabled={!hasSelected}
          loading={loading}
          style={{ borderRadius: "4px" }}
        >
          Add
        </Button>
        <span
          style={{
            marginLeft: 8,
          }}
        >
          {hasSelected ? `Selected ${selectedRowKeys.length} items` : ""}
        </span>
      </div>
      <ConfigProvider
        theme={{
          components: {
            Table: {
              borderColor: "rgba(0, 0, 0, 0.1)",
              headerBorderRadius: "4px",
              controlItemBgActiveHover: "rgba(0, 0, 0, 0.05)",
              controlItemBgActive: "rgba(0, 0, 0, 0.05)",
            },
          },
        }}
      >
        <Table
          style={{
            minHeight: "390px",
          }}
          rowSelection={rowSelection}
          columns={columns}
          dataSource={data}
          pagination={false}
          size="small"
          bordered
          scroll={{
            x: 766,
          }}
        />
        <BasePagination
          pageNumber={searchParams.pageNumber}
          onPageChange={(pageNumber) => {
            // setSearchParams({ ...searchParams, pageNumber: pageIndex });
            // console.log(searchParams, pageIndex);
            onPageChange(pageNumber);
          }}
          pageSize={searchParams.pageSize}
          totalRecord={students.totalRecord}
        />
      </ConfigProvider>
    </div>
  );
};
