import {
  DownOutlined,
  DownloadOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Box, Grid } from "@mui/material";
import { Dropdown, Space, Tooltip, message } from "antd";
import { useEffect, useState } from "react";
import { axiosClient } from "src/axios/AxiosClient";
import { BaseButton } from "src/components/Base/BaseButton/BaseButton";
import { ConditionEnum } from "src/enum/Enum";
import { exportToExcel } from "src/utils/handleExcel";
import { filterUtils, searchUtils } from "src/utils/handleSearchFilter";
import { ClassStudentsTable } from "./ClassStudentsTable/ClassStudentsTable";
import { ImportClassStudent } from "./ImportClassStudent/ImportClassStudent";
import { NewClassStudent } from "./NewClassStudent/NewClassStudent";
import { NewClassStudentExist } from "./NewClassStudent/NewClassStudentExist";
import "./ClassStudent.scss";
const items = [
  {
    key: "1",
    label: "New Student",
    children: <NewClassStudent />,
  },
  {
    key: "2",
    label: "Exist Student",
    children: "Content of Tab Pane 2",
  },
];

const searchClassStudent = [
  {
    id: "student_name",
    value: "Student Name",
  },
  {
    id: "student_email",
    value: "Email",
  },
  {
    id: "student_phone",
    value: "Phone",
  },
  {
    id: "project_code",
    value: "Project Code",
  },
  {
    id: "project_name",
    value: "Project Name",
  },
];
// const headers = [
//   "Mã số",
//   "Họ tên",
//   "Email",
//   "Số điện thoại",
//   "Mã lớp",
//   "Tên lớp",
//   "Mã dự án",
//   "Tên dự án",
//   "Ghi chú",
// ];

export const ClassStudents = ({ classId }) => {
  const [modalStudent, setModalStudent] = useState(false);
  const [modalStudentExist, setModalStudentExist] = useState(false);
  const [checkedSearchSelect, setCheckedSearchSelect] = useState(null);
  const [checkedSearchInput, setCheckedSearchInput] = useState(null);

  const toggleStudent = () => setModalStudent(!modalStudent);
  const toggleStudentExist = () => setModalStudentExist(!modalStudentExist);

  const onChange = (key) => {
    console.log(key);
  };
  const [students, setStudents] = useState({
    totalRecord: 0,
    data: [],
    summary: "",
  });

  const [searchParams, setSearchParams] = useState({
    pageNumber: 1,
    pageSize: 10,
    sortString: "",
    filterConditions: [
      {
        field: "class_id",
        value: classId,
        condition: ConditionEnum.Equal,
      },
    ],
  });

  const onSearch = (filter) => {
    searchUtils(filter, searchParams, setSearchParams, fetchData);
  };

  const onFilter = (filter) => {
    filterUtils(filter, searchParams, setSearchParams, fetchData);
  };

  const onReset = () => {
    const newSearchParams = {
      ...searchParams,
      filterConditions: [
        {
          field: "class_id",
          value: classId,
          condition: ConditionEnum.Equal,
        },
      ],
    };
    setSearchParams(newSearchParams);
    setCheckedSearchSelect(null);
    setCheckedSearchInput(null);
    fetchData(newSearchParams);
  };
  const onResetSearchSelect = (value) => {
    setCheckedSearchSelect(value);
  };

  const onResetSearchInput = (value) => {
    setCheckedSearchInput(value);
  };
  const [exportParam, setExportParam] = useState([]);

  const onPageChange = (pageNumber) => {
    const newSearchParams = { ...searchParams, pageNumber: pageNumber };
    setSearchParams(newSearchParams);
    fetchData(newSearchParams);
    console.log(pageNumber);
  };

  const fetchData = async (searchParams) => {
    const { data: studentArr } = await axiosClient.post(
      "/ClassStudent/GetByPaging",
      searchParams
    );
    setStudents(studentArr);
  };

  const handleExportToExcel = async () => {
    try {
      // Gọi API để lấy dữ liệu Excel
      const { data: exportExcel } = await axiosClient.post(
        "/ClassStudent/Export",
        exportParam,
        {
          responseType: "arraybuffer", // Đảm bảo dữ liệu trả về dưới dạng binary
        }
      );
      exportToExcel(exportExcel);
      // const blob = new Blob([exportExcel], {
      //   type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      // });

      // // Tạo liên kết tải về
      // const url = window.URL.createObjectURL(blob);
      // const link = document.createElement("a");
      // link.href = url;
      // link.setAttribute("download", "Danhsachsinhvien.xlsx");
      // document.body.appendChild(link);
      // link.click();

      // // Giải phóng URL khi không cần thiết nữa
      // window.URL.revokeObjectURL(url);
    } catch (error) {
      console.error("Lỗi khi tải file Excel: ", error);
    }
  };

  useEffect(() => {
    fetchData(searchParams);
  }, []);

  const handleButtonClick = (e) => {
    message.info("Click on left button.");
    console.log("click left button", e);
  };
  const handleMenuClick = (e) => {
    message.info("Click on menu item.");
    console.log("click", e);
  };
  const handleAddnewStudent = () => {
    toggleStudent();
  };
  const handleAddnewStudentExist = () => {
    toggleStudentExist();
  };
  const items = [
    {
      label: (
        <a
          target="_blank"
          rel="noopener noreferrer"
          onClick={handleAddnewStudent}
        >
          Add New Student User
        </a>
      ),
      key: "1",
      icon: <UserOutlined />,
    },
    {
      label: (
        <a
          target="_blank"
          rel="noopener noreferrer"
          onClick={handleAddnewStudentExist}
        >
          Add New Student Exist
        </a>
      ),
      key: "2",
      icon: <UserOutlined />,
    },
  ];
  const menuProps = {
    items,
  };
  return (
    <>
      <Box className="box w-100 d-flex flex-column flexGrow_1">
        <div className="card custom-card mb-0 flexGrow_1">
          <div className="card-body d-flex flex-column">
            <div className="row d-flex flex-row">
              <div className="col-7 my-auto d-flex">
                {/* <BaseSearch
                className="col-lg-9 col-md-8 p-0 m-0"
                placeholderInput="Search here..."
                placeholderSelect="Search by"
                options={searchClassStudent}
                onSearch={onSearch}
                checkedSearchSelect={checkedSearchSelect}
                onResetSearchSelect={onResetSearchSelect}
                checkedSearchInput={checkedSearchInput}
                onResetSearchInput={onResetSearchInput}
              /> */}
                <Tooltip
                  title="Export"
                  placement="top"
                  color="#8E69DF"
                  size="large"
                >
                  <div>
                    <BaseButton
                      color="primary"
                      variant="outline"
                      nameTitle="me-2 px-3 py-1"
                      icon={<DownloadOutlined />}
                      onClick={() => handleExportToExcel()}
                    />
                  </div>
                </Tooltip>
                <ImportClassStudent classId={classId}/>
              </div>
              <div className="col-5 mt-sm-0 mt-2 align-items-center float-end d-flex flex-row">
                <Space
                  wrap
                  className="my-auto py-2 col-lg-3 mb-1 float-end flexGrow_1 dropdownStudent"
                >
                  <Dropdown.Button
                    type="primary"
                    className="flexGrow_1 d-flex flex-row justify-content-end"
                    menu={menuProps}
                    icon={<DownOutlined />}
                    onChange={handleButtonClick}
                  >
                    Add New
                  </Dropdown.Button>
                </Space>
                <NewClassStudent
                  modal={modalStudent}
                  toggle={toggleStudent}
                  fetchData={fetchData}
                  searchParams={searchParams}
                  classId={classId}
                />
                <NewClassStudentExist
                  modal={modalStudentExist}
                  toggle={toggleStudentExist}
                  classId={classId}
                  fetchData={fetchData}
                  searchParams={searchParams}
                />
              </div>
            </div>

            <Grid container className="m-0 flexGrow_1 d-flex flex-column">
              <Grid
                item
                md={12}
                xs={12}
                sm={12}
                className="flexGrow_1 d-flex flex-column"
              >
                <ClassStudentsTable
                  // users={users}
                  students={students}
                  searchParams={searchParams}
                  fetchData={fetchData}
                  onPageChange={onPageChange}
                />
              </Grid>
            </Grid>
          </div>
        </div>
      </Box>
    </>
  );
};
