import { Button, ConfigProvider, Input, Space, Table, Tooltip } from "antd";
import { useRef, useState } from "react";
import { axiosClient } from "src/axios/AxiosClient";
import { DeleteFilled, SearchOutlined } from "@ant-design/icons";
import { BasePagination } from "src/components/Base/BasePagination/BasePagination";
import { Status } from "src/pages/UserListPage/components/UserTable/Status/Status";
import { BaseButton } from "src/components/Base/BaseButton/BaseButton";
const sortOptions = [
  { key: "asc", value: "Price ASC" },
  { key: "desc", value: "Price DESC" },
];
export const ClassStudentsTable = ({
  students,
  searchParams,
  fetchData,
  onPageChange,
}) => {
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [loadingActive, setLoadingActive] = useState(false);
  const [loadingInactive, setLoadingInactive] = useState(false);
  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const searchInput = useRef(null);
  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };
  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText("");
  };
  const [userPage, setUserPage] = useState({
    totalRecord: 0,
    data: [],
  });

  // const handleChangeStatus = async (userId, status) => {
  //   const { data, err } = await axiosClient.post(
  //     `/Class/UpdateStatus?status=${status}`,
  //     userId
  //   );
  //   if (err) {
  //     window.alert("Change fail!");
  //     return;
  //   } else {
  //     window.alert("Change Successful!");
  //     fetchData(searchParams);
  //   }
  // };

  // const handleChange = (value, status) => {
  //   {
  //     status === 1 ? setLoadingActive(true) : setLoadingInactive(true);
  //   }
  //   // ajax request after empty completing
  //   setTimeout(() => {
  //     setSelectedRowKeys([]);
  //     {
  //       status === 1 ? setLoadingActive(false) : setLoadingInactive(false);
  //     }
  //     handleChangeStatus(value, status);
  //   }, 1000);
  // };

  const onSelectChange = (newSelectedRowKeys) => {
    console.log("selectedRowKeys changed: ", newSelectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  const hasSelected = selectedRowKeys.length > 0;

  const data = [];
  for (let index = 0; index < students.data.length; index++) {
    const student = students.data[index];
    let i = index + 1;
    data.push({
      key: student.class_student_id,
      order: (searchParams.pageNumber - 1) * searchParams.pageSize + i,
      student_name: student.student_name,
      student_email: student.student_email,
      student_phone: student.student_phone,
      // status: <Status status={student.status} />,
      // status: user.status === 1 ? "Active" : "Inactive",
      action: (
        <BaseButton
          color="danger"
          variant="outline"
          nameTitle="px-3 py-1"
          icon={<DeleteFilled />}
        />
      ),
    });
  }
  const columns = [
    {
      title: "#",
      dataIndex: "order",
      key: "order",
      width: "5%",
      fixed: "left",
      align: "center",
      sorter: (a, b) => a.order - b.order,
    },
    {
      title: "Student Name",
      dataIndex: "student_name",
      key: "student_name",
      width: "30%",
      sorter: (a, b) => a.student_name.length - b.student_name.length,
      render: (student_name) => (
        <Tooltip placement="topLeft" title={student_name}>
          {student_name}
        </Tooltip>
      ),
    },
    {
      title: "Student Email",
      dataIndex: "student_email",
      key: "student_email",
      width: "30%",
      sorter: (a, b) => a.student_email.length - b.student_email.length,
      render: (student_email) => (
        <Tooltip placement="topLeft" title={student_email}>
          {student_email}
        </Tooltip>
      ),
    },
    {
      title: "Student Phone",
      dataIndex: "student_phone",
      key: "student_phone",
      width: "20%",
      sorter: (a, b) => a.student_phone.length - b.student_phone.length,
      render: (student_phone) => (
        <Tooltip placement="topLeft" title={student_phone}>
          {student_phone}
        </Tooltip>
      ),
    },
    // {
    //   title: "Status",
    //   dataIndex: "status",
    //   key: "status",
    //   width: "10%",
    //   align: "center",
    //   // sorter: (a, b) => a.status - b.status,
    // },
    {
      title: "Action",
      dataIndex: "action",
      key: "action",
      width: "10%",
      align: "center",
      fixed: "right",
      borderColor: "black",
    },
  ];

  return (
    <div className="d-flex flex-column flexGrow_1">
      {/* <div
        style={{
          marginBottom: 16,
        }}
      >
        <Button
          type="primary"
          // onClick={() => {
          //   handleChange(selectedRowKeys, 1);
          // }}
          disabled={!hasSelected}
          loading={loadingActive}
        >
          Active
        </Button>
        <Button
          type="primary"
          // onClick={() => {
          //   handleChange(selectedRowKeys, 0);
          // }}
          disabled={!hasSelected}
          loading={loadingInactive}
          danger
        >
          Inactive
        </Button>
        <span
          style={{
            marginLeft: 8,
          }}
        >
          {hasSelected ? `Selected ${selectedRowKeys.length} items` : ""}
        </span>
      </div> */}
      <ConfigProvider
        theme={{
          components: {
            Table: {
              borderColor: "rgba(0, 0, 0, 0.1)",
              headerBorderRadius: "4px",
              controlItemBgActiveHover: "rgba(0, 0, 0, 0.05)",
              controlItemBgActive: "rgba(0, 0, 0, 0.05)",
            },
          },
        }}
      >
        <Table
          className="flexGrow_1"
          rowSelection={rowSelection}
          columns={columns}
          dataSource={data}
          pagination={false}
          size="small"
          bordered
          scroll={{
            x: 1183,
            y: 330,
          }}
        />
        <BasePagination
          pageNumber={searchParams.pageNumber}
          onPageChange={(pageNumber) => {
            // setSearchParams({ ...searchParams, pageNumber: pageIndex });
            // console.log(searchParams, pageIndex);
            onPageChange(pageNumber);
          }}
          pageSize={searchParams.pageSize}
          totalRecord={students.totalRecord}
        />
      </ConfigProvider>
    </div>
  );
};
