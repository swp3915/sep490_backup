import { DownloadOutlined, UploadOutlined } from "@ant-design/icons";
import { Tooltip } from "antd";
import React, { useState } from "react";
import { Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { BaseButton } from "src/components/Base/BaseButton/BaseButton";
import { BaseUploadFile } from "src/components/Base/BaseUploadFile/BaseUploadFile";

export const ImportClassStudent = ({classId}) => {
  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);
  return (
    <>
      <Tooltip title="Import" placement="top" color="#E6533C" size="large">
        <div>
          <BaseButton
            variant="outline" nameTitle="px-3 py-1"
            color="danger"
            icon={<UploadOutlined />}
            onClick={toggle}
          />
        </div>
      </Tooltip>
      <Modal isOpen={modal} toggle={toggle} size="lg" centered>
        <form autoComplete="off">
          <ModalHeader toggle={toggle}>Import Student</ModalHeader>
          <ModalBody className="row">
            <BaseUploadFile classId={classId}/>
          </ModalBody>
        </form>
      </Modal>
    </>
  );
};
