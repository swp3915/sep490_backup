import {
  FilterOutlined,
  LoadingOutlined,
  ReloadOutlined,
} from "@ant-design/icons";
import { Box } from "@mui/material";
import { useEffect, useState } from "react";
import { ToastContainer } from "react-toastify";
import { axiosClient } from "src/axios/AxiosClient";
import { BasePagination } from "src/components/Base/BasePagination/BasePagination";
import { BaseSearch } from "src/components/Base/BaseSearch/BaseSearch";
import { NavbarDashboard } from "src/components/NavbarDashboard/NavbarDashboard";
import { ConditionEnum } from "src/enum/Enum";
import "./SubjectListPage.scss";
import { FilterSubject } from "./components/FilterSubject/FilterSubject";
import { NewSubject } from "./components/NewSubject/NewSubject";
import { SubjectItem } from "./components/SubjectItem/SubjectItem";
import { Empty } from "antd";

const searchSubject = [
  {
    id: "subject_code",
    value: "Subject Code",
  },
  {
    id: "subject_name",
    value: "Subject Name",
  },
];
const filter = {
  field: "",
  value: "",
  condition: ConditionEnum.Equal,
};
export const SubjectListPage = () => {
  const [subjects, setSubjects] = useState({
    totalRecord: 0,
    data: [],
    summary: "",
  });

  const [users, setUsers] = useState([]);
  const [dropdown, setDropdown] = useState(false);
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);
  const [checkedSearchSelect, setCheckedSearchSelect] = useState(null);
  const [checkedSearchInput, setCheckedSearchInput] = useState(null);
  const [searchParams, setSearchParams] = useState({
    pageNumber: 1,
    pageSize: 9,
    sortString: "",
    filterConditions: [],
  });

  const fetchData = async (searchParams) => {
    const { data: subjectList } = await axiosClient.post(
      "/Subject/GetByPaging",
      searchParams
    );
    setSubjects(subjectList);

    setLoading(false);
  };
  const fetchDataSelect = async () => {
    const { data: roleArr } = await axiosClient.post(
      "/Setting/GetFilterData?sortString=order by display_order ASC",
      [
        {
          field: "setting_value",
          value: "Manager",
          condition: ConditionEnum.Equal,
        },
      ]
    );

    const { data: userArr } = await axiosClient.post(
      "/User/GetFilterData?sortString=order by created_date ASC",
      [
        {
          field: "setting_id",
          value: roleArr[0].setting_id,
          condition: ConditionEnum.Equal,
        },
      ]
    );
    setUsers(userArr);
  };
  // console.log(users);
  const onPageChange = (pageNumber) => {
    const newSearchParams = { ...searchParams, pageNumber: pageNumber };
    setSearchParams(newSearchParams);
    fetchData(newSearchParams);
  };

  const onSearch = (filter) => {
    if (filter.field === "") {
      return;
    }
    // console.log(filter);
    const filterConditions = searchParams.filterConditions.filter(
      (obj) => obj.condition !== ConditionEnum.Like
    );
    filterConditions.push(filter);
    const newSearchParams = {
      ...searchParams,
      filterConditions: filterConditions,
    };
    console.log(newSearchParams);
    setSearchParams(newSearchParams);
    fetchData(newSearchParams);
  };

  const onFilter = (filter) => {
    // console.log(searchParams);
    const filterConditions = searchParams.filterConditions.filter(
      (obj) => obj.field !== filter.field
    );
    if (filter.value !== "all") {
      filterConditions.push(filter);
    }
    // console.log(filter.field);
    const newSearchParams = {
      ...searchParams,
      filterConditions: filterConditions,
    };
    // console.log(newSearchParams);
    setSearchParams(newSearchParams);
    // fetchData(newSearchParams);
  };

  const onReset = () => {
    // const filterConditions = searchParams.filterConditions.filter(
    //   (obj) => obj.field !== 'status' && obj.field !== 'setting_id'
    // );
    const newSearchParams = {
      ...searchParams,
      filterConditions: [],
    };
    setSearchParams(newSearchParams);
    setCheckedSearchSelect(null);
    setCheckedSearchInput(null);
    fetchData(newSearchParams);
  };

  const onResetSearchSelect = (value) => {
    setCheckedSearchSelect(value);
  };

  const onResetSearchInput = (value) => {
    setCheckedSearchInput(value);
  };

  useEffect(() => {
    fetchData(searchParams);
    fetchDataSelect();
  }, []);

  return (
    <>
      <ToastContainer autoClose="2000" theme="colored" />
      <NavbarDashboard
        position="subject"
        dashboardBody={
          // <div className="col-xl-12">
          <Box className="box w-100 d-flex flex-column flexGrow_1">
            <div className="col-xl-12">
              <div className="card custom-card">
                <div className="row card-body ">
                  <div className="contact-header">
                    <div className="row align-items-center justify-content-between d-flex flex-row">
                      <div className="col-xl-1 col-l h5 fw-bold mb-0 pe-0 d-flex flex-row flexGrow_1">
                        Subject List
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="card custom-card mb-0 flexGrow_1">
              <div className="  card-body d-flex flex-column">
                <div className="row">
                  <div className="row p-0 m-0  mt-2 mb-3 align-items-center justify-content-between ">
                    <div className="col-lg-7 col-md-3 my-auto">
                      <BaseSearch
                        className="col-lg-9 col-md-8 p-0 m-0"
                        placeholderInput="Search here..."
                        placeholderSelect="Search by"
                        options={searchSubject}
                        onSearch={onSearch}
                        checkedSearchSelect={checkedSearchSelect}
                        onResetSearchSelect={onResetSearchSelect}
                        checkedSearchInput={checkedSearchInput}
                        onResetSearchInput={onResetSearchInput}
                      />
                    </div>
                    <div className="col-lg-5 col-md-8 mt-sm-0 mt-2 px-2 position-relative align-items-center float-end ">
                      {/* <div
                          className=" align-items-center float-end "
                          style={{ marginRight: "10px" }}
                        > */}
                      <NewSubject
                        users={users}
                        fetchData={fetchData}
                        searchParams={searchParams}
                      />
                      {/* </div> */}
                      <div className="col-lg-7 float-end me-5 mt-2 d-flex h-100 justify-content-end">
                        <FilterOutlined
                          className={
                            dropdown === true
                              ? "filterIcon iconActive position-relative my-auto float-end me-3"
                              : "filterIcon position-relative my-auto float-end me-3"
                          }
                          onClick={() => setDropdown(!dropdown)}
                          // onBlur={() => setDropdown(false)}
                        />
                        {loading ? (
                          <LoadingOutlined
                            className="filterIcon ms-4 float-end"
                            disabled
                          />
                        ) : (
                          <ReloadOutlined
                            className="filterIcon ms-4 float-end"
                            onClick={() => {
                              setLoading(true);
                              fetchData(searchParams);
                            }}
                          />
                        )}
                        <div
                          className={
                            dropdown === false
                              ? "position-absolute cardDropdown bottom-0 end-0 shadow d-none"
                              : "position-absolute cardDropdown bottom-0 end-0 shadow"
                          }
                        >
                          <div className="card custom-card mb-0">
                            <div className="card-body">
                              <FilterSubject
                                // subjects={subjects.data}
                                users={users}
                                onFilter={onFilter}
                                fetchData={fetchData}
                                searchParams={searchParams}
                                onReset={onReset}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className=" flexGrow_1">
                  <div className="row ">
                    {subjects.data.map((subject) => (
                      <SubjectItem
                        key={subject.subject_id}
                        subjectId={subject.subject_id}
                        subject={subject}
                        searchParams={searchParams}
                        onPageChange={onPageChange}
                        fetchData={fetchData}
                      />
                    ))}
                  </div>
                </div>

                <BasePagination
                  pageNumber={searchParams.pageNumber}
                  onPageChange={(pageNumber) => {
                    // setSearchParams({ ...searchParams, pageNumber: pageIndex });
                    // console.log(searchParams, pageIndex);
                    onPageChange(pageNumber);
                  }}
                  pageSize={searchParams.pageSize}
                  totalRecord={subjects.totalRecord}
                />
              </div>
            </div>
          </Box>
          // </div>
        }
      />
    </>
  );
};
