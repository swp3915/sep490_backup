import { useNavigate } from "react-router";
import { ToastContainer, toast } from "react-toastify";
import { axiosClient } from "src/axios/AxiosClient";
import { BaseButton } from "src/components/Base/BaseButton/BaseButton";
import Swal from "sweetalert2";

import "./SubjectItem.scss";

export const SubjectItem = ({
  subjectId,
  subject,
  fetchData,
  searchParams,
}) => {
  const navigate = useNavigate();
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: "btn btn-success mx-2",
      cancelButton: "btn btn-danger",
    },
    buttonsStyling: false,
  });
  const handleChangeStatus = async (subjectId, status) => {
    const subjectIdArr = [];
    subjectIdArr.push(subjectId);
    swalWithBootstrapButtons
      .fire({
        title: "Are you sure?",
        text: "Are you sure to update subject status?",
        icon: "warning",
        showCancelButton: true,
        reverseButtons: true,
        confirmButtonText: "Yes,update it!",
        cancelButtonText: "No, cancel!",
      })
      .then(async (result) => {
        console.log(result);
        if (result.isConfirmed) {
          const { err } = await axiosClient.post(
            `/Subject/UpdateStatus?status=${status}`,
            subjectIdArr
          );
          if (err) {
            toast.error("Change fail!");
            return;
          } else {
            toast.success("Change Successful!");
            fetchData(searchParams);
          }
        }
      });
  };
  // console.log(subject);
  return (
    <>
      <div className="col-xl-4 col-lg-6">
        <div className="card border border-primary custom-card">
          <div className="card-body contact-action position-relative">
            <div className="d-flex align-items-top ">
              <div className="d-flex flex-fill ">
                <div
                  className="avatar avatar-xl  me-3"
                  style={{ backgroundColor: "orange" }}
                >
                  <img src="/src/images/7.png" alt="" />
                </div>
                <div>
                  <h6 className="mb-1 fw-semibold">{subject.subject_name}</h6>
                  <p className="mb-1 text-muted contact-mail">
                    Code: {subject.subject_code}
                  </p>
                  <p className="fw-semibold  mb-0 text-muted  text-truncate">
                    Assignee: {subject.assignee_name}
                  </p>
                </div>
              </div>
              <div>
                <div className="position-relative">
                  {subject.status === 1 ? (
                    <div className="position-relative">
                      <span className="position-absolute top-50 start-0 ms-0 translate-middle-y p-1 bg-success border border-light rounded-circle"></span>
                      <span className="ms-3 text-success">Active</span>
                    </div>
                  ) : (
                    <div className="position-relative">
                      <span className="position-absolute top-50 start-0 ms-0 translate-middle-y p-1 bg-danger border border-light rounded-circle"></span>
                      <span className="ms-3 text-danger">Inactive</span>
                    </div>
                  )}
                </div>
              </div>
            </div>
            <ToastContainer autoClose="2000" theme="colored" />
            <div className="d-flex align-items-center justify-content-center gap-2 contact-hover-buttons">
              <BaseButton
                value="Details"
                nameTitle="btn btn-sm btn-light contact-hover-btn"
                onClick={() =>
                  navigate(`/subject-details-general/${subjectId}`)
                }
              />
              {/* {console.log(subjectId)} */}
              <button
                aria-label="button"
                className="btn btn-sm btn-light contact-hover-btn position-relative"
                type="button"
                onClick={() => {
                  const status = subject.status === 1 ? 0 : 1;
                  handleChangeStatus(subject.subject_id, status);
                  fetchData(searchParams);
                }}
              >
                {subject.status === 0 ? (
                  <>
                    <span className="position-absolute top-50 start-0 ms-1 translate-middle-y p-1 bg-success border border-light rounded-circle"></span>
                    <span className="ms-2 text-success">Active</span>
                  </>
                ) : (
                  <>
                    <span className="position-absolute top-50 start-0 ms-1 translate-middle-y p-1 bg-danger border border-light rounded-circle"></span>
                    <span className="ms-2 text-danger">Inactive</span>
                  </>
                )}
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
