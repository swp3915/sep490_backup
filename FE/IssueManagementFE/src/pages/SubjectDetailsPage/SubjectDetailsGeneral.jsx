import { Tooltip } from "antd";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router";
import { ToastContainer } from "react-toastify";
import { axiosClient } from "src/axios/AxiosClient";
import { NavbarDashboard } from "src/components/NavbarDashboard/NavbarDashboard";
import { ConditionEnum } from "src/enum/Enum";
import { SubjectGeneral } from "./SubjectGeneral/SubjectGeneral";
import { Box } from "@mui/material";

export const SubjectDetailsGeneral = () => {
  const { subjectId } = useParams();
  const navigate = useNavigate();
  const [users, setUsers] = useState([]);
  const [subjectObj, setSubjectObj] = useState({});
  const [loading, setLoading] = useState(false);
  const fetchData = async () => {
    const { data: subjectById } = await axiosClient.get(
      `/Subject/${subjectId}`
    );
    setSubjectObj(subjectById);
    setLoading(true);
  };
  const fetchDataSelect = async () => {
    const { data: roleArr } = await axiosClient.post(
      "/Setting/GetFilterData?sortString=order by display_order ASC",
      [
        {
          field: "setting_value",
          value: "Manager",
          condition: ConditionEnum.Equal,
        },
      ]
    );

    const { data: userArr } = await axiosClient.post(
      "/User/GetFilterData?sortString=order by created_date ASC",
      [
        {
          field: "setting_id",
          value: roleArr[0].setting_id,
          condition: ConditionEnum.Equal,
        },
      ]
    );
    setUsers(userArr);
  };

  useEffect(() => {
    fetchData();
    fetchDataSelect();
  }, []);
  return (
    <>
      <ToastContainer autoClose="2000" theme="colored" />
      <NavbarDashboard
        position="subject"
        dashboardBody={
          <>
            <div className="d-flex flex-column flexGrow_1">
              <div className="col-xl-12">
                <div className="card custom-card">
                  <div className="card-body p-0">
                    <div className="d-flex p-3 align-items-center justify-content-between">
                      <div>
                        <h6
                          className="fw-bold mb-0 title-setting"
                          style={{ fontSize: "x-large" }}
                        >
                          Subject Details
                        </h6>
                      </div>
                      <div>
                        <ul
                          className="nav nav-tabs nav-tabs-header mb-0 d-sm-flex d-block"
                          role="tablist"
                        >
                          <li className="nav-item m-1">
                            <a
                              className="nav-link pointer active"
                              onClick={() => {
                                navigate(
                                  `/subject-details-general/${subjectId}`
                                );
                              }}
                            >
                              General
                            </a>
                          </li>
                          <li className="nav-item pointer m-1">
                            <a
                              className="nav-link"
                              onClick={() => {
                                navigate(
                                  `/subject-details-assignment/${subjectId}`
                                );
                              }}
                            >
                              Assignment
                            </a>
                          </li>
                          <li className="nav-item pointer m-1">
                            <a
                              className="nav-link"
                              onClick={() => {
                                navigate(
                                  `/subject-details-setting/${subjectId}`
                                );
                              }}
                            >
                              Setting
                            </a>
                          </li>
                        </ul>
                      </div>
                      <Tooltip
                        title="Add new"
                        placement="top"
                        color="
                        #26BF94"
                        size="large"
                      >
                        <div className="btn-list"></div>
                      </Tooltip>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card custom-card mb-0 flexGrow_1">
                <div className="card-body d-flex flex-column">
                  {loading ? (
                    <SubjectGeneral
                      users={users}
                      subjectObj={subjectObj}
                      fetchData={fetchData}
                    />
                  ) : (
                    ""
                  )}
                </div>
              </div>
            </div>
          </>
        }
      />
    </>
  );
};
