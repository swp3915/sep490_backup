import { Tooltip } from "antd";
import { useNavigate, useParams } from "react-router-dom";
import { NavbarDashboard } from "src/components/NavbarDashboard/NavbarDashboard";
import { SubjectSetting } from "./SubjectSetting/SubjectSetting";

export const SubjectDetailsSetting = () => {
  const { subjectId } = useParams();
  const navigate = useNavigate();
  return (
    <NavbarDashboard
      position="subject"
      dashboardBody={
        <>
          <div className="col-xl-12">
            <div className="card custom-card">
              <div className="card-body p-0">
                <div className="d-flex p-3 align-items-center justify-content-between">
                  <div>
                    <h6 className="fw-bold mb-0 title-setting">
                      Subject Details
                    </h6>
                  </div>
                  <div>
                    <ul
                      className="nav nav-tabs nav-tabs-header mb-0 d-sm-flex d-block"
                      role="tablist"
                    >
                      <li className="nav-item m-1">
                        <a
                          className="nav-link"
                          onClick={() => {
                            navigate(`/subject-details-general/${subjectId}`);
                          }}
                        >
                          General
                        </a>
                      </li>
                      <li className="nav-item pointer m-1">
                        <a
                          className="nav-link"
                          onClick={() => {
                            navigate(
                              `/subject-details-assignment/${subjectId}`
                            );
                          }}
                        >
                          Assignment
                        </a>
                      </li>
                      <li className="nav-item pointer m-1">
                        <a
                          className="nav-link pointer active"
                          onClick={() => {
                            navigate(`/subject-details-setting/${subjectId}`);
                          }}
                        >
                          Setting
                        </a>
                      </li>
                    </ul>
                  </div>
                  <Tooltip
                    title="Add new"
                    placement="top"
                    color="
                        #26BF94"
                    size="large"
                  >
                    <div className="btn-list"></div>
                  </Tooltip>
                </div>
              </div>
            </div>
          </div>
          <div className="card custom-card mb-0" style={{ minHeight: "500px" }}>
            <div className="card-body">
              <SubjectSetting subjectId={subjectId} />
            </div>
          </div>
        </>
      }
    />
  );
};
