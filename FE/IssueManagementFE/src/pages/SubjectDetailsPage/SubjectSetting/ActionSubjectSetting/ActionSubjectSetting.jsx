import { EditOutlined, SettingOutlined } from "@ant-design/icons";
import { Tooltip } from "antd";
import { BaseButton } from "src/components/Base/BaseButton/BaseButton";

export const ActionSubjectSetting = () => {
  return (
    <>
      <div className="d-flex justify-content-around">
        <Tooltip title="Details" placement="top" color="#ffc107" size="large">
          <div>
            <BaseButton
              icon={<EditOutlined />}
              variant="outline"
              nameTitle="px-2 py-1"
              color="warning"
            />
          </div>
        </Tooltip>
        <Tooltip title="Inactive" placement="top" color="#dc3545" size="large">
          <div>
            <BaseButton
              icon={<SettingOutlined />}
              variant="outline"
              color="danger"
              nameTitle="px-2 py-1"
            />
          </div>
        </Tooltip>
        {/* <Tooltip title="Active" placement="top" color="#198754" size="large">
          <div>
            <BaseButton
              icon={<SettingOutlined />}
              variant="outline"
              color="success"
              nameTitle="px-2 py-1"
            />
          </div>
        </Tooltip> */}
      </div>
    </>
  );
};
