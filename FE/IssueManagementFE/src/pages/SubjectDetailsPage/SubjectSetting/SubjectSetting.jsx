import { Box, Grid } from "@mui/material";
import { useEffect, useState } from "react";
import { axiosClient } from "src/axios/AxiosClient";
import { ConditionEnum } from "src/enum/Enum";
import { searchUtils } from "src/utils/handleSearchFilter";
import "./SubjectSetting.scss";
import { SubjectSettingsTable } from "./SubjectSettingsTable/SubjectSettingsTable";

export const SubjectSetting = ({ subjectId }) => {
  const [loading, setLoading] = useState(false);
  const [subjectSettings, setSubjectSettings] = useState({
    totalRecord: 0,
    data: [],
    summary: "",
  });
  const filterConditions = [
    {
      field: "subject_id",
      value: subjectId,
      condition: ConditionEnum.Equal,
    },
  ];
  const [searchParams, setSearchParams] = useState({
    pageNumber: 1,
    pageSize: 4,
    sortString: "",
    filterConditions: filterConditions,
  });

  const fetchData = async (searchParams) => {
    const { data: issueSettingArr } = await axiosClient.post(
      "/IssueSetting/GetByPaging",
      searchParams
    );
    setSubjectSettings(issueSettingArr);
    setLoading(false);
  };

  const [userPage, setUserPage] = useState({
    totalRecord: 0,
    data: [],
  });

  const onSearch = (filter) => {
    searchUtils(filter, searchParams, setSearchParams, fetchData);
  };

  const handleReset = () => {
    setLoading(true);
    let reLoad = {
      ...searchParams,
      filterConditions: [],
    };
    setSearchParams(reLoad);
    fetchData(reLoad);
  };

  const filterIssueGroup = (value) => {
    const filter = {
      field: "issue_group",
      value: value,
      condition: ConditionEnum.Equal,
    };
    filterConditions.push(filter);
    const newSearchParams = {
      ...searchParams,
      filterConditions: filterConditions,
    };
    setSearchParams(newSearchParams);
    // fetchData(newSearchParams);
    return newSearchParams;
    // console.log(newSearchParams);
  };

  const onPageChange = (pageNumber) => {
    const newSearchParams = { ...searchParams, pageNumber: pageNumber };
    setSearchParams(newSearchParams);
    fetchData(newSearchParams);
    console.log(pageNumber);
  };

  useEffect(() => {
    fetchData(searchParams);
  }, []);
  return (
    <Box sx={{ flexGrow: 1 }} className="box w-100">
      <div className="card custom-card mb-0">
        <div className="card-body row">
          <div className="col-lg-7 col-md-3 my-auto">
            {/* <BaseSearch
              className="col-lg-9 col-md-8 p-0 m-0"
              placeholderInput="Search here..."
              placeholderSelect="Search by"
              options={searchUser}
              onSearch={onSearch}
              checkedSearchSelect={checkedSearchSelect}
              onResetSearchSelect={onResetSearchSelect}
              checkedSearchInput={checkedSearchInput}
              onResetSearchInput={onResetSearchInput}
            /> */}
          </div>
          <div className="col-lg-5 col-md-8 mt-sm-0 mt-2 position-relative align-items-center float-end">
            {/* <BaseButton
                nameTitle="my-auto ms-3 px-3 py-2 col-lg-3 col-md-3 mb-1 float-end addNewBtn"
                onClick={toggle}
                color="warning"
                value="Add New"
              /> */}
          </div>
          <Grid container className="m-0">
            <Grid item md={12}>
              <SubjectSettingsTable
                onSearch={onSearch}
                subjectSettings={subjectSettings}
                fetchData={fetchData}
                searchParams={searchParams}
                subjectId={subjectId}
                handleReset={handleReset}
                loading={loading}
              />
            </Grid>
          </Grid>
        </div>
      </div>
    </Box>
  );
};
