import { Progress } from "antd";
import { BaseBadge } from "src/components/Base/BaseBadge/BaseBadge";
import { BaseButton } from "src/components/Base/BaseButton/BaseButton";
import "./SubjectAssignmentItem.scss";
import { SubjectAssignmentDetails } from "../SubjectAssignment/SubjectAssignmentDetails/SubjectAssignmentDetails";

export const SubjectAssignmentItem = ({
  assignment,
  subjectId,
  fetchData,
  subjects,
  searchParams,
}) => {
  return (
    <>
      <li className="p-0 d-flex flex-column ">
        <div className="timeline-time text-end"></div>
        <div className="timeline-icon">
          <a href="#"></a>
        </div>

        <div
          className="timeline-body shadow mt-2"
          style={{ width: "94%", top: "0%" }}
        >
          <div className="d-flex align-items-top timeline-main-content flex-wrap mt-0">
            <div className="flex-fill">
              <div className="row align-items-center">
                <div className="col-3 mt-sm-0 mt-2 left-card">
                  <p className="mb-0 fs-15 fw-semibold title-card">
                    Name: {assignment.assignment_name}
                  </p>
                  <p className="mb-2 text-muted fs-13  text-truncate">
                    Description:{assignment.description}
                  </p>
                  {assignment.status === 0 ? (
                    <BaseBadge
                      bageName="Inactive"
                      color="danger"
                      roundedPill="fw-semibold float-start mt-1"
                    />
                  ) : (
                    ""
                  )}
                  {assignment.status === 1 ? (
                    <BaseBadge
                      bageName="Active"
                      color="success"
                      roundedPill="fw-semibold float-start"
                    />
                  ) : (
                    ""
                  )}
                </div>
                <Progress
                  percent={30}
                  size="small"
                  strokeColor="#ff6f21f0"
                  className="float-end mx-5 fs-10 fw-semibold col-6 progress-Ass"
                />

                <div className="ms-auto col-1 d-block">
                  <BaseButton
                    value="Close Assignment"
                    nameTitle="btnClose fw-semibold float-end mb-3 py-1 px-2"
                    variant="outline"
                    color="dark"
                    type="button"
                  />

                  <SubjectAssignmentDetails
                    assignment={assignment}
                    subjectId={subjectId}
                    fetchData={fetchData}
                    subjects={subjects}
                    searchParams={searchParams}
                  />
                </div>
                <div className="position-absolute bottom-0 end-0 me-3 mb-2 col-3"></div>
              </div>
            </div>
          </div>
        </div>
      </li>
    </>
  );
};
