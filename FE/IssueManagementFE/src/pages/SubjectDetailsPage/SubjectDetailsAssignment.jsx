import { useNavigate, useParams } from "react-router";
import { NavbarDashboard } from "src/components/NavbarDashboard/NavbarDashboard";
import "./SubjectDetailsAssignment.scss";
import { SubjectAssignment } from "./SubjectAssignment/SubjectAssignment";
import { ToastContainer } from "react-toastify";

export const SubjectDetailsAssignment = () => {
  const navigate = useNavigate();
  const { subjectId } = useParams();
  return (
    <>
      <ToastContainer autoClose="2000" theme="colored" />
      <NavbarDashboard
        position="subject"
        dashboardBody={
          <>
            <div className="d-flex flex-column flexGrow_1">
              <div className="col-xl-12">
                <div className="card custom-card">
                  <div className="card-body p-0 d-flex flex-column">
                    <div className="d-flex p-3 align-items-center justify-content-between">
                      <div>
                        <h6 className="fw-semibold mb-0 title-assignment">
                          Assignment
                        </h6>
                      </div>

                      <div>
                        <ul
                          className="nav nav-tabs nav-tabs-header mb-0 d-sm-flex d-block"
                          role="tablist"
                        >
                          <li className="nav-item m-1">
                            <a
                              className="nav-link"
                              onClick={() => {
                                navigate(
                                  `/subject-details-general/${subjectId}`
                                );
                              }}
                            >
                              General
                            </a>
                          </li>
                          <li className="nav-item pointer m-1">
                            <a
                              className="nav-link pointer active"
                              onClick={() => {
                                navigate(
                                  `/subject-details-assignment/${subjectId}`
                                );
                              }}
                            >
                              Assignment
                            </a>
                          </li>
                          <li className="nav-item pointer m-1">
                            <a
                              className="nav-link"
                              onClick={() => {
                                navigate(
                                  `/subject-details-setting/${subjectId}`
                                );
                              }}
                            >
                              Settings
                            </a>
                          </li>
                        </ul>
                      </div>
                      <div className="d-flex mt-sm-0 mt-2 align-items-center float-right">
                        <div className="btn-list"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <SubjectAssignment subjectId={subjectId} />
            </div>
          </>
        }
      />
    </>
  );
};
