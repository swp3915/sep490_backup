import { useFormik } from "formik";
import { useState } from "react";
import { toast } from "react-toastify";
import { Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { axiosClient } from "src/axios/AxiosClient";
import { BaseButton } from "src/components/Base/BaseButton/BaseButton";
import { BaseCheckbox } from "src/components/Base/BaseCheckbox/BaseCheckbox";
import { BaseInputField } from "src/components/Base/BaseInputField/BaseInputField";
import { SelectInputSubject } from "src/components/Base/BaseSelectInput/SelectInputSubject";
import { BaseTextArea } from "src/components/Base/BaseTextArea/BaseTextArea";
import { swalWithBootstrapButtons } from "src/enum/swal";
import * as Yup from "yup";
import "./SubjectAssignmentDetails.scss";

export const SubjectAssignmentDetails = ({
  searchParams,
  subjectId,
  assignment,
  fetchData,
  subjects,
}) => {
  const formik = useFormik({
    initialValues: {
      ...assignment,
    },
    validationSchema: Yup.object({
      assignment_name: Yup.string()
        .required("Assignment Name is required")
        .max(255, "Assignment Name must be lower than 255 characters"),
    }),
    onSubmit: async (values) => {
      console.log(values);
      swalWithBootstrapButtons
        .fire({
          title: "Are you sure?",
          text: "Are you sure to update subject assignment?",
          icon: "warning",
          showCancelButton: true,
          reverseButtons: true,
          confirmButtonText: "Yes,update it!",
          cancelButtonText: "No, cancel!",
        })
        .then(async (result) => {
          console.log(result);
          if (result.isConfirmed) {
            const { err } = await axiosClient.put(
              `/Assignment/${assignment.assignment_id}`,
              values
            );

            if (err) {
              toast.error("Update fail!");
            } else {
              toast.success("Update successfully!");
            }

            setModal(!modal);
            fetchData(searchParams);

            // console.log("values", values);
          }
        });
    },
  });
  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);
  return (
    <>
      <BaseButton
        color="secondary"
        variant="outline"
        value="View Details"
        nameTitle="btnView float-end py-1 px-2"
        onClick={toggle}
      />

      <Modal isOpen={modal} toggle={toggle} size="lg" centered>
        <form onSubmit={formik.handleSubmit} autoComplete="off">
          <ModalHeader toggle={toggle}>Subject Assignment Details</ModalHeader>

          <ModalBody className="row">
            <div className="col-md-12 col-sm-12 px-3">
              <BaseInputField
                type="text"
                id="assignment_name"
                name="assignment_name"
                label="Assignment Name"
                placeholder="Enter Assignment Name"
                value={formik.values.assignment_name}
                onChange={formik.handleChange}
                classNameInput={
                  formik.errors.assignment_name &&
                  formik.touched.assignment_name
                    ? "is-invalid"
                    : ""
                }
                important="true"
                onBlur={formik.handleBlur}
              />
              {formik.errors.assignment_name &&
              formik.touched.assignment_name ? (
                <p className="errorMsg"> {formik.errors.assignment_name} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div>
            <div className="col-md-6 col-sm-12 px-3">
              <SelectInputSubject
                label="Subject Code"
                id="subject_id"
                defaultValue={formik.values.subject_code}
                onBlur={formik.handleBlur}
                status={
                  formik.errors.subject_id && formik.touched.subject_id
                    ? "error"
                    : ""
                }
                placeholder="Subject Code"
                options={subjects}
                onChange={formik.handleChange}
                important="true"
                formik={formik}
                disabled={true}
              />
              {formik.errors.subject_id && formik.touched.subject_id ? (
                <p className="errorMsg"> {formik.errors.subject_id} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div>
            <div className="col-md-6 col-sm-12 px-3">
              <SelectInputSubject
                label="Subject Name"
                id="subject_id"
                defaultValue={formik.values.subject_name}
                onBlur={formik.handleBlur}
                status={
                  formik.errors.subject_id && formik.touched.subject_id
                    ? "error"
                    : ""
                }
                placeholder="Subject Name"
                options={subjects}
                onChange={formik.handleChange}
                important="true"
                formik={formik}
                disabled={true}
              />
              {formik.errors.subject_id && formik.touched.subject_id ? (
                <p className="errorMsg"> {formik.errors.subject_id} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div>

            <div className="col-md-12 col-sm-12 mt-0 px-3 ">
              <BaseTextArea
                formik={formik}
                label="Description"
                placeholder="Description"
                important="false"
                row={4}
              />
            </div>
            <div className="col-md-6 col-sm-12 mt-4 px-3">
              <BaseCheckbox formik={formik} type="status" />
            </div>
          </ModalBody>
          <ModalFooter>
            <BaseButton
              className="ms-3 px-3"
              type="submit"
              value="Update"
              color="secondary"
            />
          </ModalFooter>
        </form>
      </Modal>
    </>
  );
};
