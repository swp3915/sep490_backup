import { PlusOutlined } from "@ant-design/icons";
import { Tooltip } from "antd";
import { useFormik } from "formik";
import { useState } from "react";
import { toast } from "react-toastify";
import { Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { axiosClient } from "src/axios/AxiosClient";
import { BaseButton } from "src/components/Base/BaseButton/BaseButton";
import { BaseCheckbox } from "src/components/Base/BaseCheckbox/BaseCheckbox";
import { BaseInputField } from "src/components/Base/BaseInputField/BaseInputField";
import { BaseTextArea } from "src/components/Base/BaseTextArea/BaseTextArea";
import Swal from "sweetalert2";
import * as Yup from "yup";
import "./NewAssignment.scss";
import { swalWithBootstrapButtons } from "src/enum/swal";

export const NewAssignment = ({
  subjectId,
  assignments,
  fetchData,
  searchParams,
}) => {
  const formik = useFormik({
    initialValues: {
      description: null,
      assignment_name: null,
      status: 1,
      subject_id: subjectId,
    },
    validationSchema: Yup.object({
      assignment_name: Yup.string()
        .required("Assignment Name is required")
        .max(255, "Assignment Name must be lower than 255 characters"),
    }),
    onSubmit: async (values) => {
      swalWithBootstrapButtons
        .fire({
          title: "Are you sure?",
          text: "Are you sure to add new assignment?",
          icon: "warning",
          showCancelButton: true,
          reverseButtons: true,
          confirmButtonText: "Yes,add it!",
          cancelButtonText: "No, cancel!",
        })
        .then(async (result) => {
          // console.log(result);
          if (result.isConfirmed) {
            const { err } = await axiosClient.post(`/Assignment`, values);

            if (err) {
              toast.error("Add fail!");
            } else {
              toast.success("Add successfully!");
              formik.resetForm();
            }
            setModal(!modal);
            fetchData(searchParams);
            // console.log(values);
          }
        });
    },
  });
  const [modal, setModal] = useState(false);

  const toggle = () => {
    setModal(!modal);
  };
  return (
    <>
      {/* {console.log(assignments)} */}
      <BaseButton
        nameTitle="my-auto ms-3 px-3 py-2 col-lg-3 col-md-3 mb-1 float-end addNewBtn"
        onClick={toggle}
        color="warning"
        value="Add New"
      />
      <Modal isOpen={modal} toggle={toggle} size="lg" centered>
        <form onSubmit={formik.handleSubmit} autoComplete="off">
          <ModalHeader toggle={toggle}>Add New Assignment</ModalHeader>
          <ModalBody className="row">
            <div className="col-md-12 col-sm-12 px-3">
              <BaseInputField
                type="text"
                id="assignment_name"
                name="assignment_name"
                label="Assignment Name"
                placeholder="Enter Assignment Name"
                value={formik.values.assignment_name}
                onChange={formik.handleChange}
                classNameInput={
                  formik.errors.assignment_name &&
                  formik.touched.assignment_name
                    ? "is-invalid"
                    : ""
                }
                important="true"
                onBlur={formik.handleBlur}
              />
              {formik.errors.assignment_name &&
              formik.touched.assignment_name ? (
                <p className="errorMsg"> {formik.errors.assignment_name} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div>

            <div className="col-md-12 col-sm-12 mt-0 px-3">
              <BaseTextArea
                formik={formik}
                label="Description"
                placeholder="Description"
                important="false"
                row={4}
              />
            </div>
            <div className="col-md-6 col-sm-12 mt-4 px-3">
              <BaseCheckbox formik={formik} type="status" />
            </div>
          </ModalBody>
          <ModalFooter>
            <BaseButton
              className="ms-3 px-3"
              type="submit"
              value="Add New"
              color="secondary"
            />
          </ModalFooter>
        </form>
      </Modal>
    </>
  );
};
