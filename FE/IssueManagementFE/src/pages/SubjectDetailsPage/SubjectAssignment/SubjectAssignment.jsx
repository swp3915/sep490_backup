import { useEffect, useState } from "react";
import { axiosClient } from "src/axios/AxiosClient";
import { BasePagination } from "src/components/Base/BasePagination/BasePagination";
import { ConditionEnum } from "src/enum/Enum";
import { SubjectAssignmentItem } from "../SubjectAssignmentItem/SubjectAssignmentItem";
import { NewAssignment } from "./NewAssignment/NewAssignment";
import {
  FilterOutlined,
  LoadingOutlined,
  ReloadOutlined,
} from "@ant-design/icons";
import { BaseSearch } from "src/components/Base/BaseSearch/BaseSearch";
import { FilterAssignment } from "./FilterAssignment/FilterAssignment";
import "./SubjectAssignment.scss";
import { Empty } from "antd";

const searchAssignment = [
  {
    id: "assignment_name",
    value: "Assignment Name",
  },
];
const filter = {
  field: "",
  value: "",
  condition: ConditionEnum.Equal,
};
export const SubjectAssignment = ({ subjectId }) => {
  const [assignments, setAssignments] = useState({
    totalRecord: 0,
    data: [],
    summary: "",
  });
  const [dropdown, setDropdown] = useState(false);
  const [loading, setLoading] = useState(false);
  const [subjects, setSubjects] = useState([]);
  const [data, setData] = useState([]);
  const [checkedSearchSelect, setCheckedSearchSelect] = useState(null);
  const [checkedSearchInput, setCheckedSearchInput] = useState(null);
  const [searchParams, setSearchParams] = useState({
    pageNumber: 1,
    pageSize: 4,
    sortString: "",
    filterConditions: [
      {
        field: "subject_id",
        value: subjectId,
        condition: ConditionEnum.Equal,
      },
    ],
  });
  // console.log(subjectId);
  const fetchData = async (searchParams) => {
    const { data: assignmentArr } = await axiosClient.post(
      "Assignment/GetByPaging",
      searchParams
    );
    setAssignments(assignmentArr);
    // console.log(assignmentArr);
    setLoading(false);
  };
  // const fetchDataSelect = async () => {
  //   const { data: subjectArr } = await axiosClient.post(
  //     "/Subject/GetFilterData?sortString=order by created_date ASC",
  //     []
  //   );
  //   setSubjects(subjectArr);
  // };

  const onPageChange = (pageNumber) => {
    const newSearchParams = { ...searchParams, pageNumber: pageNumber };
    setSearchParams(newSearchParams);
    fetchData(newSearchParams);
    // console.log(pageNumber);
  };

  const onSearch = (filter) => {
    if (filter.field === "") {
      return;
    }
    // console.log(filter);
    const filterConditions = searchParams.filterConditions.filter(
      (obj) => obj.condition !== ConditionEnum.Like
    );
    filterConditions.push(filter);
    const newSearchParams = {
      ...searchParams,
      filterConditions: filterConditions,
    };
    // console.log(newSearchParams);
    setSearchParams(newSearchParams);
    fetchData(newSearchParams);
  };

  const onFilter = (filter) => {
    // console.log(searchParams);
    const filterConditions = searchParams.filterConditions.filter(
      (obj) => obj.field !== filter.field
    );
    if (filter.value !== "all") {
      filterConditions.push(filter);
    }
    // console.log(filter.field);
    const newSearchParams = {
      ...searchParams,
      filterConditions: filterConditions,
    };
    // console.log(newSearchParams);
    setSearchParams(newSearchParams);
    // fetchData(newSearchParams);
  };

  const onReset = () => {
    // const filterConditions = searchParams.filterConditions.filter(
    //   (obj) => obj.field !== 'status' && obj.field !== 'setting_id'
    // );
    const newSearchParams = {
      ...searchParams,
      filterConditions: [],
    };
    setSearchParams(newSearchParams);
    setCheckedSearchSelect(null);
    setCheckedSearchInput(null);
    fetchData(newSearchParams);
  };

  const onResetSearchSelect = (value) => {
    setCheckedSearchSelect(value);
  };

  const onResetSearchInput = (value) => {
    setCheckedSearchInput(value);
  };

  useEffect(() => {
    fetchData(searchParams);
    // fetchDataSelect();
  }, []);
  return (
    <>
      {/* {console.log(assignment)} */}
      {/* <div className="subject__timeline"> */}

      <div className="col-xl-12 d-flex flex-column flexGrow_1">
        <div className="d-flex flex-column flexGrow_1">
          <div className="card custom-card flexGrow_1 mb-0 ">
            <div className="card-body subject__timeline d-flex flex-column">
              <div className="row mb-3">
                <div className="col-lg-7 col-md-3 my-auto ">
                  <BaseSearch
                    className="col-lg-9 col-md-8 p-0 m-0"
                    placeholderInput="Search here..."
                    placeholderSelect="Search by"
                    options={searchAssignment}
                    onSearch={onSearch}
                    checkedSearchSelect={checkedSearchSelect}
                    onResetSearchSelect={onResetSearchSelect}
                    checkedSearchInput={checkedSearchInput}
                    onResetSearchInput={onResetSearchInput}
                  />
                </div>
                <div className="col-lg-5 col-md-8 mt-sm-0 mt-2 position-relative align-items-center float-end">
                  {/* <div
                className="d-flex mt-sm-0 mt-2 align-items-center float-right"
                style={{ marginRight: "10px" }}
              > */}
                  <NewAssignment
                    assignments={assignments}
                    subjectId={subjectId}
                    fetchData={fetchData}
                    searchParams={searchParams}
                  />
                  {/* </div> */}
                  <div className="col-lg-7 float-end me-5 mt-2 d-flex h-100 justify-content-end">
                    <FilterOutlined
                      className={
                        dropdown === true
                          ? "filterIcon iconActive position-relative my-auto float-end me-3"
                          : "filterIcon position-relative my-auto float-end me-3"
                      }
                      onClick={() => setDropdown(!dropdown)}
                      // onBlur={() => setDropdown(false)}
                    />
                    {loading ? (
                      <LoadingOutlined
                        className="filterIcon ms-4 float-end"
                        disabled
                      />
                    ) : (
                      <ReloadOutlined
                        className="filterIcon ms-4 float-end"
                        onClick={() => {
                          setLoading(true);
                          fetchData(searchParams);
                        }}
                      />
                    )}
                    <div
                      className={
                        dropdown === false
                          ? "position-absolute cardDropdown bottom-0 end-0 shadow d-none"
                          : "position-absolute cardDropdown bottom-0 end-0 shadow"
                      }
                    >
                      <div className="card custom-card mb-0">
                        <div className="card-body">
                          <FilterAssignment
                            assignments={assignments}
                            onFilter={onFilter}
                            fetchData={fetchData}
                            searchParams={searchParams}
                            onReset={onReset}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <ul className="timeline list-unstyled flexGrow_1">
                {assignments.data.map((assignment) => (
                  <SubjectAssignmentItem
                    key={assignment.assignment_id}
                    assignment={assignment}
                    subjectId={subjectId}
                    subjects={subjects}
                    fetchData={fetchData}
                    searchParams={searchParams}
                  />
                ))}
              </ul>

              <BasePagination
                pageNumber={searchParams.pageNumber}
                onPageChange={(pageNumber) => {
                  onPageChange(pageNumber);
                }}
                pageSize={searchParams.pageSize}
                totalRecord={assignments.totalRecord}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
{
  /* {data.length > 0 ? (
                ) : (
                  <Empty
                    image={Empty.PRESENTED_IMAGE_SIMPLE}
                    className="col-xl-12 d-flex flex-column flexGrow_1"
                  />
                )} */
}
