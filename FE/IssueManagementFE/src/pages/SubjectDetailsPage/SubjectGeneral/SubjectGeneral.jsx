import { useFormik } from "formik";
import { useNavigate } from "react-router";
import { toast } from "react-toastify";
import { axiosClient } from "src/axios/AxiosClient";
import { BaseButton } from "src/components/Base/BaseButton/BaseButton";
import { BaseCheckbox } from "src/components/Base/BaseCheckbox/BaseCheckbox";
import { BaseInputField } from "src/components/Base/BaseInputField/BaseInputField";
import { SelectInputUser } from "src/components/Base/BaseSelectInput/SelectInputUser";
import { BaseTextArea } from "src/components/Base/BaseTextArea/BaseTextArea";
import Swal from "sweetalert2";
import * as Yup from "yup";
import "./SubjectGeneral.scss";
import { swalWithBootstrapButtons } from "src/enum/swal";

export const SubjectGeneral = ({ users, subjectObj, fetchData }) => {
  const navigate = useNavigate();

  const formik = useFormik({
    initialValues: {
      ...subjectObj,
    },
    validationSchema: Yup.object({
      subject_name: Yup.string()
        .required("Subject Name is required")
        .max(255, "Subject Name must be lower than 255 characters"),
      subject_code: Yup.string()
        .required("Subject Code is required")
        .max(100, "Subject Code must be lower than 100 characters"),
      assignee_id: Yup.string().required("Subject Manager is required"),
    }),
    onSubmit: async (values) => {
      swalWithBootstrapButtons
        .fire({
          title: "Are you sure?",
          text: "Are you sure to update subject?",
          icon: "warning",
          showCancelButton: true,
          reverseButtons: true,
          confirmButtonText: "Yes,update it!",
          cancelButtonText: "No, cancel!",
        })
        .then(async (result) => {
          // console.log(result);
          if (result.isConfirmed) {
            const { data, err } = await axiosClient.put(
              `/Subject/${values.subject_id}`,
              values
            );
            if (err) {
              toast.error(err.response.data.Message);
            } else {
              toast.success(data);

              fetchData();
            }
          }
        });
      // console.log(values);
    },
  });
  return (
    <>
      <form
        onSubmit={formik.handleSubmit}
        autoComplete="off"
        className="d-flex flex-column flexGrow_1"
      >
        <div className="flexGrow_1 ">
          <div className="row">
            <div className="col-md-12 col-sm-12 px-3">
              <BaseInputField
                type="text"
                id="subject_name"
                name="subject_name"
                label="Subject Name"
                placeholder="Enter Subject Name"
                value={formik.values.subject_name}
                onChange={formik.handleChange}
                classNameInput={
                  formik.errors.subject_name && formik.touched.subject_name
                    ? "is-invalid"
                    : ""
                }
                important="true"
                onBlur={formik.handleBlur}
              />
              {formik.errors.subject_name && formik.touched.subject_name ? (
                <p className="errorMsg"> {formik.errors.subject_name} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div>
            <div className="col-md-6 col-sm-12 px-3">
              <BaseInputField
                type="text"
                id="subject_code"
                name="subject_code"
                label="Subject Code"
                placeholder="Enter Subject Code"
                value={formik.values.subject_code}
                onChange={formik.handleChange}
                classNameInput={
                  formik.errors.subject_code && formik.touched.subject_code
                    ? "is-invalid"
                    : ""
                }
                important="true"
                onBlur={formik.handleBlur}
              />
              {formik.errors.subject_code && formik.touched.subject_code ? (
                <p className="errorMsg"> {formik.errors.subject_code} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div>

            <div className="col-md-6 col-sm-12 px-3">
              <SelectInputUser
                label="Subject Manager"
                id="assignee_id"
                defaultValue={formik.values.assignee_name}
                options={users}
                onChange={formik.handleChange}
                important="true"
                formik={formik}
                isFilter={false}
                placeholder="Subject Manager"
                status={
                  formik.errors.assignee_id && formik.touched.assignee_id
                    ? "error"
                    : ""
                }
                onBlur={formik.handleBlur}
              />
              {formik.errors.assignee_id && formik.touched.assignee_id ? (
                <p className="errorMsg"> {formik.errors.assignee_id} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div>

            <div className="col-md-12 col-sm-12 mt-0 px-3">
              <BaseTextArea
                formik={formik}
                label="Description"
                placeholder="Description"
                important="false"
                row={5}
              />
            </div>
            <div className="col-md-6 col-sm-12 mt-4 px-3">
              <BaseCheckbox formik={formik} type="status" />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12 px-3">
            <BaseButton
              nameTitle="float-end mt-4"
              type="submit"
              value="Update"
              color="secondary"
            />
            <BaseButton
              nameTitle="float-end mt-4 me-3 btn-cancel"
              type="button"
              value="Back"
              color="dark"
              variant="outline"
              onClick={() => {
                navigate("/subject-list");
              }}
            />
          </div>
        </div>
      </form>
    </>
  );
};
