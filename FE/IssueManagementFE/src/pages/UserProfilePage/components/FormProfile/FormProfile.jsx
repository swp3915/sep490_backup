import { Avatar } from "antd";
import { useFormik } from "formik";
import { useRef, useState } from "react";
import { toast } from "react-toastify";
import { Input } from "reactstrap";
import { axiosClient } from "src/axios/AxiosClient";
import { BaseButton } from "src/components/Base/BaseButton/BaseButton";
import { BaseInputField } from "src/components/Base/BaseInputField/BaseInputField";
import { BaseSelectInput } from "src/components/Base/BaseSelectInput/BaseSelectInput";
import { ModalCmpt } from "src/components/Modal/ModalCmpt";
import Swal from "sweetalert2";
import * as Yup from "yup";
import ChangePassword from "../ChangePassword/ChangePassword";

export const FormProfile = ({ user, roles, userId }) => {
  const [imageSrc, setImageSrc] = useState("/src/images/user_none_img.jpg");
  const [file, setFile] = useState(null);
  const handleFileChange = async (event) => {
    const file = event.target.files[0]; // Get the selected file
    const formData = new FormData();
    formData.append("file", file);
    const { data } = await axiosClient.post("/your-upload-endpoint", formData);
    if (file) {
      const reader = new FileReader();
      reader.onload = (e) => {
        const blob = new Blob([e.target.result], { type: file.type });
        setImageSrc(URL.createObjectURL(blob)); // Set the blob as the source for an <img> element
        setFile(file);
      };
      reader.readAsArrayBuffer(file);
    }
  };

  const inputRef = useRef(null);
  const formik = useFormik({
    initialValues: {
      ...user,
    },
    validationSchema: Yup.object({
      fullname: Yup.string()
        .required("Fullname is required")
        .min(4, "Must be 4 characters or more"),
      email: Yup.string()
        .required("Email is required")
        .matches(
          /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/,
          "Please enter a valid email address"
        ),
      phone_number: Yup.string()
        .nullable()
        .test(
          "is-valid-postal-code",
          "Phone must be a valid phone number and have 10 digits",
          function (value) {
            if (value && value.trim() !== null) {
              const postalCodePattern =
                /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/;
              if (!postalCodePattern.test(value)) {
                return false; // Validation failed
              }
            }
            return true;
          }
        ),
    }),
    onSubmit: async (values) => {
      Swal.fire({
        title: "Are you sure?",
        text: "Are you sure to add new user?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes",
      }).then(async (result) => {
        console.log(result);
        if (result.isConfirmed) {
          const { data, err } = await axiosClient.put(
            `/User/${userId}`,
            values
          );
          if (err) {
            toast.error("Update fail!");
            return;
          } else {
            toast.success("Update Successful!");
          }
        }
      });
      // const { data, err } = await axiosClient.put(`/User/${userId}`, values);
      // window.alert("Form submitted");
      // if (err) {
      //   window.alert("Update fail!");
      // } else {
      //   window.alert("Update success!");
      // }
      // console.log(values);
    },
  });
  return (
    <form className="d-flex flex-column flexGrow_1" onSubmit={formik.handleSubmit}>
      <div className="row">
        <div
          className="col-md-5 col-sm-12 px-3"
          style={{ borderRight: "1px solid #f3f3f3" }}
        >
          <h3 className="text-center mb-4">{formik.values.fullname}</h3>
          <Avatar
            className="mx-auto d-block mb-5"
            size={132}
            icon={<img src={imageSrc} alt="Selected Image" />}
          />
          {/* <UploadFile /> */}
          <Input
            className="w-75 mx-auto d-block"
            id="exampleFile"
            ref={inputRef}
            name="file"
            type="file"
            onChange={handleFileChange}
          />
          {/* {imageSrc && (
          <div>
            <img src={imageSrc} alt="Selected Image" />
          </div>
        )}
         */}
          {console.log(imageSrc)}
        </div>
        <div className="col-md-7 col-sm-12 px-3 d-flex flex-column">
          <h3>General Information</h3>
          <div className="row d-flex flex-column">
            <div className="col-md-12 col-sm-12">
              <BaseInputField
                type="text"
                id="fullname"
                name="fullname"
                label="Full name"
                placeholder="Enter Full name"
                onBlur={formik.handleBlur}
                value={formik.values.fullname}
                onChange={formik.handleChange}
                classNameInput={
                  formik.errors.fullname && formik.touched.fullname
                    ? "is-invalid"
                    : ""
                }
                important="true"
              />
              {formik.errors.fullname && formik.touched.fullname ? (
                <p className="errorMsg"> {formik.errors.fullname} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div>
            <div className="col-md-12 col-sm-12">
              <BaseInputField
                type="text"
                id="email"
                name="email"
                label="Email"
                placeholder="Enter Email"
                value={formik.values.email}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                classNameInput={
                  formik.errors.email && formik.touched.email
                    ? "is-invalid"
                    : ""
                }
                important="true"
              />
              {formik.errors.email && formik.touched.email ? (
                <p className="errorMsg"> {formik.errors.email} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div>
            <div className="col-md-12 col-sm-12">
              <BaseInputField
                type="text"
                id="phone_number"
                label="Phone"
                placeholder="Enter Phone"
                onBlur={formik.handleBlur}
                value={formik.values.phone_number}
                onChange={formik.handleChange}
                classNameInput={
                  formik.errors.phone_number && formik.touched.phone_number
                    ? "is-invalid"
                    : ""
                }
                important="true"
              />
              {formik.errors.phone_number && formik.touched.phone_number ? (
                <p className="errorMsg"> {formik.errors.phone_number} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div>
            <div className="col-md-12 col-sm-12">
              <BaseSelectInput
                label="Role"
                id="setting_value"
                classNameDiv="col-md-6 col-sm-12"
                defaultValue={formik.values.setting_value}
                options={roles}
                onChange={formik.handleChange}
                user={user}
                important="true"
                formik={formik}
                disabled={true}
              />
            </div>
          </div>
          <div>
            <ModalCmpt
              classNameBtn="btn btn-light btn-wave waves-effect waves-light px-5 mt-5 my-auto"
              btnToggle="Change Password"
              isAnchor={false}
              variant="outline"
              isFooter={false}
              isHeader={false}
              modalBody={<ChangePassword />}
              isImage={true}
              imgSrc="/src/images/logo2.png"
              size="md"
              type="button"
            />
            <BaseButton
              type="submit"
              nameTitle="float-right mt-5"
              value="Update"
              color="secondary"
            />
          </div>
          {/* <BaseButton
          nameTitle="float-right me-3 mt-5"
          value="Cancel"
          color="dark"
        /> */}
        </div>
      </div>
    </form>
  );
};
