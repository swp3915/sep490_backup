import { Button, ConfigProvider, Input, Space, Table, Tooltip } from "antd";
import { useRef, useState } from "react";
import { axiosClient } from "src/axios/AxiosClient";
import { SearchOutlined } from "@ant-design/icons";
import { BasePagination } from "src/components/Base/BasePagination/BasePagination";
import { Status } from "src/pages/UserListPage/components/UserTable/Status/Status";
import { ActionClass } from "./ActionClass/ActionClass";
import { BaseAction } from "src/components/Base/BaseAction/BaseAction";
import { useNavigate } from "react-router";
const sortOptions = [
  { key: "asc", value: "Price ASC" },
  { key: "desc", value: "Price DESC" },
];
export const ClassTable = ({
  classes,
  searchParams,
  onPageChange,
  fetchData,
}) => {
  const navigate = useNavigate();
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [loadingActive, setLoadingActive] = useState(false);
  const [loadingInactive, setLoadingInactive] = useState(false);
  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const searchInput = useRef(null);
  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };
  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText("");
  };
  const [userPage, setUserPage] = useState({
    totalRecord: 0,
    data: [],
  });

  const handleChangeStatus = async (userId, status) => {
    const { data, err } = await axiosClient.post(
      `/Class/UpdateStatus?status=${status}`,
      userId
    );
    if (err) {
      window.alert("Change fail!");
      return;
    } else {
      window.alert("Change Successful!");
      fetchData(searchParams);
    }
  };

  const handleChange = (value, status) => {
    {
      status === 1 ? setLoadingActive(true) : setLoadingInactive(true);
    }
    // ajax request after empty completing
    setTimeout(() => {
      setSelectedRowKeys([]);
      {
        status === 1 ? setLoadingActive(false) : setLoadingInactive(false);
      }
      handleChangeStatus(value, status);
    }, 1000);
  };

  // const start = () => {
  //   setLoading(true);
  //   // ajax request after empty completing
  //   setTimeout(() => {
  //     setSelectedRowKeys([]);
  //     setLoading(false);
  //   }, 1000);
  // };
  const onSelectChange = (newSelectedRowKeys) => {
    console.log("selectedRowKeys changed: ", newSelectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  const hasSelected = selectedRowKeys.length > 0;

  const data = [];
  if (classes.data.length !== 0) {
    for (let index = 0; index < classes.data.length; index++) {
      const classObj = classes.data[index];
      let i = index + 1;
      data.push({
        key: classObj.class_id,
        order: (searchParams.pageNumber - 1) * searchParams.pageSize + i,
        class_code: classObj.class_code,
        class_name: classObj.class_name,
        subject_code: classObj.subject_code,
        subject_name: classObj.subject_name,
        setting_value: classObj.semester_name,
        status: <Status status={classObj.status} />,
        // status: user.status === 1 ? "Active" : "Inactive",
        action: (
          // <ActionClass
          //   classId={classObj.class_id}
          //   fetchData={fetchData}
          //   classObj={classObj}
          //   searchParams={searchParams}
          // />
          <BaseAction
            optionId={classObj.class_id}
            option={classObj}
            type="Class"
            fetchData={fetchData}
            searchParams={searchParams}
            onClick={() =>
              navigate(`/class-details-general/${classObj.class_id}`)
            }
          />
        ),
      });
    }
  }

  const columns = [
    {
      title: "#",
      dataIndex: "order",
      key: "order",
      width: "5%",
      fixed: "left",
      align: "center",
      sorter: (a, b) => a.order - b.order,
    },
    {
      title: "Class Code",
      dataIndex: "class_code",
      key: "class_code",
      width: "10%",
      sorter: (a, b) => a.class_code.length - b.class_code.length,
      render: (class_code) => (
        <Tooltip placement="topLeft" title={class_code}>
          {class_code}
        </Tooltip>
      ),
    },
    {
      title: "Class Name",
      dataIndex: "class_name",
      key: "class_name",
      width: "20%",
      sorter: (a, b) => a.class_name.length - b.class_name.length,
      render: (class_name) => (
        <Tooltip placement="topLeft" title={class_name}>
          {class_name}
        </Tooltip>
      ),
    },
    {
      title: "Subject Code",
      dataIndex: "subject_code",
      key: "subject_code",
      width: "10%",
      sorter: (a, b) => a.subject_code.length - b.subject_code.length,
      render: (subject_code) => (
        <Tooltip placement="topLeft" title={subject_code}>
          {subject_code}
        </Tooltip>
      ),
    },
    {
      title: "Subject Name",
      dataIndex: "subject_name",
      key: "subject_name",
      width: "20%",
      sorter: (a, b) => a.subject_name.length - b.subject_name.length,
      render: (subject_name) => (
        <Tooltip placement="topLeft" title={subject_name}>
          {subject_name}
        </Tooltip>
      ),
    },
    {
      title: "Semester",
      dataIndex: "setting_value",
      key: "setting_value",
      width: "11%",
      sorter: (a, b) => a.setting_value - b.setting_value,
      render: (setting_value) => (
        <Tooltip placement="topLeft" title={setting_value}>
          {setting_value}
        </Tooltip>
      ),
    },
    {
      title: "Status",
      dataIndex: "status",
      key: "status",
      width: "10%",
      align: "center",
      // sorter: (a, b) => a.status - b.status,
    },
    {
      title: "Action",
      dataIndex: "action",
      key: "action",
      width: "10%",
      align: "center",
      fixed: "right",
      borderColor: "black",
    },
  ];

  return (
    <div
      style={{
        flexGrow: 1,
      }}
      className="d-flex flex-column"
    >
      {/* <div
        style={{
          marginBottom: 16,
        }}
      >
        <Button
          type="primary"
          onClick={() => {
            handleChange(selectedRowKeys, 1);
          }}
          disabled={!hasSelected}
          loading={loadingActive}
        >
          Active
        </Button>
        <Button
          type="primary"
          onClick={() => {
            handleChange(selectedRowKeys, 0);
          }}
          disabled={!hasSelected}
          loading={loadingInactive}
          danger
        >
          Inactive
        </Button>
        <span
          style={{
            marginLeft: 8,
          }}
        >
          {hasSelected ? `Selected ${selectedRowKeys.length} items` : ""}
        </span>
      </div> */}
      <ConfigProvider
        theme={{
          components: {
            Table: {
              borderColor: "rgba(0, 0, 0, 0.1)",
              headerBorderRadius: "4px",
              controlItemBgActiveHover: "rgba(0, 0, 0, 0.05)",
              controlItemBgActive: "rgba(0, 0, 0, 0.05)",
            },
          },
        }}
      >
        <Table
          style={{
            flexGrow: 1,
          }}
          rowSelection={rowSelection}
          columns={columns}
          dataSource={data}
          pagination={false}
          size="small"
          bordered
          scroll={{
            x: 1183,
            y: 330,
          }}
        />
        <BasePagination
          pageNumber={searchParams.pageNumber}
          onPageChange={(pageNumber) => {
            // setSearchParams({ ...searchParams, pageNumber: pageIndex });
            // console.log(searchParams, pageIndex);
            onPageChange(pageNumber);
          }}
          pageSize={searchParams.pageSize}
          totalRecord={classes.totalRecord}
        />
      </ConfigProvider>
    </div>
  );
};
