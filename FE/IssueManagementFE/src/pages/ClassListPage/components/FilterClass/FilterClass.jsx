import React, { useRef, useState } from "react";
import { DatePicker } from "antd";
import dayjs from "dayjs";
import { SelectInputStatus } from "src/components/Base/BaseSelectInput/SelectInputStatus";
import { SelectInputSetting } from "src/components/Base/BaseSelectInput/SelectInputSetting";
import { SelectInputSubject } from "src/components/Base/BaseSelectInput/SelectInputSubject";
import { BaseButton } from "src/components/Base/BaseButton/BaseButton";

const { RangePicker } = DatePicker;
const status = [
  {
    value: 1,
    label: "Active",
  },
  {
    value: 2,
    label: "Pending",
  },
  {
    value: 0,
    label: "Inactive",
  },
];
export const FilterClass = ({
  subjects,
  semesters,
  onFilter,
  fetchData,
  searchParams,
  onReset,
}) => {
  const [checkedSubject, setCheckedSubject] = useState();
  const [checkedSetting, setCheckedSetting] = useState();
  const [checkedStatus, setCheckedStatus] = useState();
  const onChangeSubject = (value) => {
    setCheckedSubject(value);
  };
  const onChangeSetting = (value) => {
    setCheckedSetting(value)
  }
  const onChangeStatus = (value) => {
    setCheckedStatus(value);
  };
  return (
    <>
      <div className="d-flex row">
        <SelectInputStatus
          label="Status"
          id="status"
          classNameDiv="col-5 mx-auto p-0"
          placeholder="Status"
          options={status}
          important="true"
          isFilter={true}
          onFilter={onFilter}
          checked={checkedStatus}
          onChange={onChangeStatus}
        />
        <SelectInputSetting
          label="Semester"
          id="semester_id"
          classNameDiv="col-5 mx-auto p-0"
          placeholder="Semester"
          options={semesters}
          important="true"
          isFilter={true}
          onFilter={onFilter}
          checked={checkedSetting}
          onChange={onChangeSetting}
        />
        <SelectInputSubject
          label="Subject Code"
          id="subject_id"
          classNameDiv="col-11 mx-auto p-0 mt-3"
          placeholder="Subject Code"
          options={subjects}
          important="true"
          isFilter={true}
          onFilter={onFilter}
          checked={checkedSubject}
          onChange={onChangeSubject}
        />

        <div className="col-11 mx-auto p-0 mt-3">
          <BaseButton
            nameTitle="float-end my-0 ms-3 cardBtn"
            value="Save"
            color="success"
            onClick={() => fetchData(searchParams)}
          />
          <BaseButton
            nameTitle="float-end my-0 cardBtn"
            type="button"
            value="Reset"
            color="dark"
            onClick={() => {
              onReset();
              setCheckedSubject(null);
              setCheckedSetting(null)
              setCheckedStatus(null);
            }}
          />
        </div>
        {/* <RangePicker
          showTime
          defaultValue={dayjs("2023-10-01 21:57:00", "2023-10-03 21:57:00")}
          className="col-11 mx-auto mt-3 py-2"
          onChange={(e) => {
            console.log(e.target.value);
          }}
        /> */}
      </div>
    </>
  );
};
