import { useFormik } from "formik";
import React, { useState } from "react";
import { toast } from "react-toastify";
import { Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { axiosClient } from "src/axios/AxiosClient";
import { BaseButton } from "src/components/Base/BaseButton/BaseButton";
import { BaseCheckbox } from "src/components/Base/BaseCheckbox/BaseCheckbox";
import { BaseInputField } from "src/components/Base/BaseInputField/BaseInputField";
import { SelectInputSetting } from "src/components/Base/BaseSelectInput/SelectInputSetting";
import { SelectInputSubject } from "src/components/Base/BaseSelectInput/SelectInputSubject";
import { SelectInputUser } from "src/components/Base/BaseSelectInput/SelectInputUser";
import { BaseTextArea } from "src/components/Base/BaseTextArea/BaseTextArea";
import { swalWithBootstrapButtons } from "src/enum/Swal";
import * as Yup from "yup";
function uuid() {
  var temp_url = URL.createObjectURL(new Blob());
  var uuid = temp_url.toString();
  URL.revokeObjectURL(temp_url);
  return uuid.substr(uuid.lastIndexOf("/") + 1); // remove prefix (e.g. blob:null/, blob:www.test.com/, ...)
}
export const NewClass = ({ semesters, subjects, teachers, fetchData, searchParams }) => {
  const formik = useFormik({
    initialValues: {
      class_code: '',
      class_name: '',
      description: '',
      status: 1,
      subject_id: '',
      semester_id: '',
      teacher_id: '',
    },
    validationSchema: Yup.object({
      class_code: Yup.string().required("Class Code is required").max(100, "Class Code must be lower than 100 characters"),
      class_name: Yup.string().required("Class Name is required").max(255, "Class Name must be lower than 255 characters"),
      subject_id: Yup.string().required("Subject Code is required"),
      semester_id: Yup.string().required("Semester is required"),
      teacher_id: Yup.string().required("Teacher is required"),
    }),
    onSubmit: async (values) => {
      swalWithBootstrapButtons
      .fire({
        title: "Are you sure?",
        text: `Are you sure to add new Class?`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, update it!",
        cancelButtonText: "No, cancel!",
        reverseButtons: true,
      })
      .then(async (result) => {
        console.log(values);
        if (result.isConfirmed) {
          const { data, err } = await axiosClient.post(`/Class`, values);
          if (err) {
            toast.error(err.response.data.Message);
            return;
          } else {
            toast.success("Add Successful!");
            fetchData(searchParams);
            swalWithBootstrapButtons.fire(
              "Updated!",
              "User has been added!.",
              "success"
            );
            toggle()
          }
        } else {
          {
            swalWithBootstrapButtons.fire(
              "Cancelled",
              "Your imaginary file is safe :)",
              "error"
            );
          }
        }
      });
    },
  });

  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);
  return (
    <>
      <BaseButton
        nameTitle="my-auto ms-3 px-3 py-2 col-lg-3 col-md-3 mb-1 float-end addNewBtn"
        onClick={toggle}
        color="warning"
        value="Add New"
      />
      <Modal isOpen={modal} toggle={toggle} size="lg" centered>
        <form onSubmit={formik.handleSubmit} autoComplete="off">
          <ModalHeader toggle={toggle}>Add New Class</ModalHeader>

          <ModalBody className="row">
            <div className="col-md-6 col-sm-12 px-3">
              <BaseInputField
                type="text"
                id="class_code"
                name="class_code"
                label="Class Code"
                placeholder="Enter Class Code"
                onBlur={formik.handleBlur}
                value={formik.values.class_code}
                onChange={formik.handleChange}
                classNameInput={
                  formik.errors.class_code && formik.touched.class_code
                    ? "is-invalid"
                    : ""
                }
                important="true"
              />
              {formik.errors.class_code && formik.touched.class_code ? (
                <p className="errorMsg"> {formik.errors.class_code} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div>
            <div className="col-md-6 col-sm-12 px-3">
              <BaseInputField
                type="text"
                id="class_name"
                name="class_name"
                label="Class Name"
                placeholder="Enter Class Name"
                value={formik.values.class_name}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                classNameInput={
                  formik.errors.class_name && formik.touched.class_name
                    ? "is-invalid"
                    : ""
                }
                important="true"
              />
              {formik.errors.class_name && formik.touched.class_name ? (
                <p className="errorMsg"> {formik.errors.class_name} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div>
            <div className="col-md-6 col-sm-12 px-3">
              <SelectInputSubject
                label="Subject Code"
                id="subject_id"
                placeholder="Subject Code"
                defaultValue={formik.values.subject_name}
                options={subjects}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                status={
                  formik.errors.subject_id && formik.touched.subject_id
                    ? "error"
                    : ""
                }
                important="true"
                isFilter={false}
                formik={formik}
              />
              {formik.errors.subject_id && formik.touched.subject_id ? (
                <p className="errorMsg"> {formik.errors.subject_id} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div>
            <div className="col-md-6 col-sm-12 px-3">
              <SelectInputSetting
                label="Semester"
                id="semester_id"
                placeholder="Semester"
                defaultValue={formik.values.semester_name}
                options={semesters}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                status={
                  formik.errors.semester_id && formik.touched.semester_id
                    ? "error"
                    : ""
                }
                important="true"
                isFilter={false}
                formik={formik}
              />
              {formik.errors.semester_id && formik.touched.semester_id ? (
                <p className="errorMsg"> {formik.errors.semester_id} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div>
            
            <div className="col-md-12 col-sm-12 px-3">
              <SelectInputUser
                label="Teacher"
                id="teacher_id"
                placeholder="Teacher"
                defaultValue={formik.values.teacher_name}
                options={teachers}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                status={
                  formik.errors.teacher_id && formik.touched.teacher_id
                    ? "error"
                    : ""
                }
                important="true"
                isFilter={false}
                formik={formik}
              />
              {formik.errors.teacher_id && formik.touched.teacher_id ? (
                <p className="errorMsg"> {formik.errors.teacher_id} </p>
              ) : (
                <p className="hiddenMsg">acb</p>
              )}
            </div>
            <div className="col-md-12 col-sm-12 px-3">
              <BaseTextArea
                formik={formik}
                label="Description"
                placeholder="Description"
                important="false"
                row='4'
              />
            </div>
            <div className="col-md-6 col-sm-12 mt-1 px-3 mt-1">
              {/* <BaseRadio label="Status" important="true" formik={formik} type="status"/> */}
              <BaseCheckbox formik={formik} type="status" />
            </div>
          </ModalBody>
          <ModalFooter>
            <BaseButton
              type="reset"
              value="Reset"
              color="dark"
              onClick={() => formik.resetForm()}
            />
            <BaseButton
              className="ms-3"
              type="submit"
              value="Add New"
              color="secondary"
            />
          </ModalFooter>
        </form>
      </Modal>
    </>
  );
};
