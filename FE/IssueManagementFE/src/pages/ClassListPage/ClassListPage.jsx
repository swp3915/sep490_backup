import React, { useEffect, useState } from "react";
import { axiosClient } from "src/axios/AxiosClient";
import { ClassTable } from "./components/ClassTable/ClassTable";
import { Box, Grid } from "@mui/material";
import { NavbarDashboard } from "src/components/NavbarDashboard/NavbarDashboard";
import { NewClass } from "./components/NewClass/NewClass";
import {
  FilterOutlined,
  LoadingOutlined,
  ReloadOutlined,
} from "@ant-design/icons";
import { BaseSearch } from "src/components/Base/BaseSearch/BaseSearch";
import { FilterClass } from "./components/FilterClass/FilterClass";
import { ConditionEnum } from "src/enum/Enum";
import { ToastContainer } from "react-toastify";
import { searchUtils, filterUtils } from "/src/utils/handleSearchFilter";
import { BaseFilter } from "src/components/Base/BaseFilter/BaseFilter";
const searchClass = [
  {
    id: "class_code",
    value: "Class Code",
  },
  {
    id: "class_name",
    value: "Class Name",
  },
  {
    id: "subject_code",
    value: "Subject Code",
  },
  {
    id: "subject_name",
    value: "Subject Name",
  },
];
export const ClassListPage = () => {
  const [classes, setClasses] = useState({
    totalRecord: 0,
    data: [],
    summary: "",
  });
  const [dropdown, setDropdown] = useState(false);
  const [loading, setLoading] = useState(false);
  const [checkedSearchSelect, setCheckedSearchSelect] = useState(null);
  const [checkedSearchInput, setCheckedSearchInput] = useState(null);
  const [semesters, setSemesters] = useState([]);
  const [subjects, setSubjects] = useState([]);
  const [teachers, setTeachers] = useState([]);
  const [searchParams, setSearchParams] = useState({
    pageNumber: 1,
    pageSize: 10,
    sortString: "",
    filterConditions: [],
  });

  const [semesterParams, setSemesterParams] = useState([
    {
      field: "data_group",
      value: "2",
      condition: ConditionEnum.Equal,
    },
  ]);

  const [teacherParams, setTeacherParams] = useState({
    field: "data_group",
    value: "2",
    condition: ConditionEnum.Equal,
  });

  const fetchData = async (searchParams) => {
    const { data: classList } = await axiosClient.post(
      "/Class/GetByPaging",
      searchParams
    );
    setClasses(classList);
    setLoading(false);
  };

  const fetchDataSelect = async () => {
    const { data: systemSettingArr } = await axiosClient.post(
      `/Setting/GetFilterData?sortString=display_order ASC`,
      semesterParams
    );
    setSemesters(systemSettingArr);

    const { data: subjectArr } = await axiosClient.post(
      "/Subject/GetFilterData?sortString=created_date ASC",
      []
    );
    setSubjects(subjectArr);

    const { data: roleList } = await axiosClient.post(
      `/Setting/GetFilterData?sortString=created_date ASC`,
      [
        {
          field: "setting_value",
          value: "Teacher",
          condition: ConditionEnum.Equal,
        },
      ]
    );

    const { data: userArr } = await axiosClient.post(
      "/User/GetFilterData?sortString=created_date ASC",
      [
        {
          field: "setting_id",
          value: roleList[0].setting_id,
          condition: ConditionEnum.Equal,
        },
      ]
    );

    console.log(userArr);

    setTeachers(userArr);
  };

  const onPageChange = (pageNumber) => {
    const newSearchParams = { ...searchParams, pageNumber: pageNumber };
    setSearchParams(newSearchParams);
    fetchData(newSearchParams);
  };

  const onSearch = (filter) => {
    searchUtils(filter, searchParams, setSearchParams, fetchData);
  };

  const onFilter = (filter) => {
    filterUtils(filter, searchParams, setSearchParams, fetchData);
  };

  const onReset = () => {
    // const filterConditions = searchParams.filterConditions.filter(
    //   (obj) => obj.field !== 'status' && obj.field !== 'setting_id'
    // );
    const newSearchParams = {
      ...searchParams,
      filterConditions: [],
    };
    setSearchParams(newSearchParams);
    setCheckedSearchSelect(null);
    setCheckedSearchInput(null);
    fetchData(newSearchParams);
  };

  const onResetSearchSelect = (value) => {
    setCheckedSearchSelect(value);
  };

  const onResetSearchInput = (value) => {
    setCheckedSearchInput(value);
  };
  useEffect(() => {
    fetchDataSelect();
    fetchData(searchParams);
  }, []);

  return (
    <>
      <ToastContainer autoClose="2000" theme="colored" />
      <NavbarDashboard
        position="class"
        dashboardBody={
          <Box className="box w-100 d-flex flex-column flexGrow_1">
            <div className="col-xl-12">
              <div className="card custom-card">
                <div className="card-body">
                  <div className="contact-header">
                    <div className="row align-items-center justify-content-between d-flex flex-row">
                      <div className="col-xl-1 col-l h5 fw-bold mb-0 pe-0 d-flex flex-row flexGrow_1">
                        Class List
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="card custom-card mb-0 flexGrow_1">
              <div className="card-body d-flex flex-column">
                <div className="row">
                  <div className="col-lg-7 col-md-3 my-auto">
                    <BaseSearch
                      className="col-lg-9 col-md-8 p-0 m-0"
                      placeholderInput="Search here..."
                      placeholderSelect="Search by"
                      options={searchClass}
                      onSearch={onSearch}
                      checkedSearchSelect={checkedSearchSelect}
                      onResetSearchSelect={onResetSearchSelect}
                      checkedSearchInput={checkedSearchInput}
                      onResetSearchInput={onResetSearchInput}
                    />
                  </div>
                  <div className="col-lg-5 col-md-8 mt-sm-0 mt-2 position-relative align-items-center float-end">
                    <NewClass
                      semesters={semesters}
                      subjects={subjects}
                      teachers={teachers}
                      fetchData={fetchData}
                      searchParams={searchParams}
                    />

                    <div className="col-lg-7 float-end me-5 d-flex h-100 justify-content-end">
                      <BaseFilter
                        icon={<FilterOutlined className="filterIcon" />}
                        filterBody={
                          <div className="cardDropdown" style={{ zIndex: 1 }}>
                            <div className="card custom-card mb-0">
                              <div className="card-body filterCard">
                                <FilterClass
                                  subjects={subjects}
                                  semesters={semesters}
                                  onFilter={onFilter}
                                  fetchData={fetchData}
                                  searchParams={searchParams}
                                  onReset={onReset}
                                />
                              </div>
                            </div>
                          </div>
                        }
                      />
                      {loading ? (
                        <LoadingOutlined
                          className="filterIcon ms-4 float-end"
                          disabled
                        />
                      ) : (
                        <ReloadOutlined
                          className="filterIcon ms-4 float-end"
                          onClick={() => {
                            setLoading(true);
                            let reLoad = {
                              ...searchParams,
                              filterConditions: [],
                            };
                            setSearchParams(reLoad);
                            fetchData(reLoad);
                          }}
                        />
                      )}
                    </div>
                  </div>
                </div>
                <Grid container className="m-0 flexGrow_1">
                  <Grid
                    item
                    md={12}
                    sm={12}
                    xs={12}
                    className="d-flex flex-column"
                  >
                    {/* <UserTable
                    users={users}
                    setUsers={setUsers}
                    roles={roles}
                    setRoles={setRoles}
                  /> */}
                    <ClassTable
                      classes={classes}
                      semesters={semesters.data}
                      searchParams={searchParams}
                      onPageChange={onPageChange}
                      fetchData={fetchData}
                    />
                  </Grid>
                </Grid>
              </div>
            </div>
          </Box>
        }
      />
    </>
  );
};
