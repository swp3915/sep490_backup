import React from "react";
import { CardSetting } from "src/components/Card/CardSetting/CardSetting";

const RoleTabSetting = ({
  setting,
  fetchData,
  searchParams,
  handleChangeStatus,
}) => {
  return (
    <>
      <CardSetting
        layoutCard="col-xl-3 col-lg-4 col-md-6 col-sm-4 col-xs-2"
        nameTitle="pending"
        roleName={setting.setting_value}
        settingGroup={setting.data_group === 1 ? "Role" : ""}
        displayOrder={setting.display_order}
        description={setting.description}
        status={setting.status}
        setting={setting}
        fetchData={fetchData}
        searchParams={searchParams}
        handleChangeStatus={handleChangeStatus}
      />
    </>
  );
};

export default RoleTabSetting;
