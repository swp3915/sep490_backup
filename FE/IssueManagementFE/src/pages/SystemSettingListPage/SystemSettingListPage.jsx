import {
  FilterOutlined,
  LoadingOutlined,
  ReloadOutlined,
} from "@ant-design/icons";
import { useEffect, useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import { axiosClient } from "src/axios/AxiosClient";
import { BasePagination } from "src/components/Base/BasePagination/BasePagination";
import { BaseSearch } from "src/components/Base/BaseSearch/BaseSearch";
import { NavbarDashboard } from "src/components/NavbarDashboard/NavbarDashboard";
import { ConditionEnum } from "src/enum/Enum";
import Swal from "sweetalert2";
import "./SystemSettingListPage.scss";
import { FilterSetting } from "./components/FilterSetting/FilterSetting";
import { NewSystemSetting } from "./components/NewSystemSetting/NewSystemSetting";
import PermittedEmailTabSetting from "./components/PermittedEmailTabSetting/PermittedEmailTabSetting";
import RoleTabSetting from "./components/RoleTabSetting/RoleTabSetting";
import SemesterTabSetting from "./components/SemesterTabSetting/SemesterTabSetting";
import { Box } from "@mui/material";
import { swalWithBootstrapButtons } from "src/enum/swal";

const searchSetting = [
  {
    id: "setting_value",
    value: "Setting Value",
  },
];
const filter = {
  field: "",
  value: "",
  condition: ConditionEnum.Equal,
};
const SystemSettingListPage = () => {
  const [tab, setTab] = useState("role");
  const [settings, setSettings] = useState({
    totalRecord: 0,
    data: [],
    summary: "",
  });

  const [dropdown, setDropdown] = useState(false);
  const [loading, setLoading] = useState(false);
  const [checkedSearchSelect, setCheckedSearchSelect] = useState(null);
  const [checkedSearchInput, setCheckedSearchInput] = useState(null);
  const [searchParams, setSearchParams] = useState({
    pageNumber: 1,
    pageSize: 8,
    sortString: "",
    filterConditions: [],
  });

  const fetchData = async (searchParams) => {
    const { data: settingList } = await axiosClient.post(
      "/Setting/GetByPaging",
      searchParams
    );
    setSettings(settingList);
    setLoading(false);
    // console.log(settingList.data);
  };

  const handleChangeStatus = async (settingId, status) => {
    const settingIdArr = [];
    settingIdArr.push(settingId);
    swalWithBootstrapButtons
      .fire({
        title: "Are you sure?",
        text: "Are you sure to update setting status?",
        icon: "warning",
        showCancelButton: true,
        reverseButtons: true,
        confirmButtonText: "Yes,update it!",
        cancelButtonText: "No, cancel!",
      })
      .then(async (result) => {
        // console.log(result);
        if (result.isConfirmed) {
          const { err } = await axiosClient.post(
            `/Setting/UpdateStatus?status=${status}`,
            settingIdArr
          );

          if (err) {
            toast.error("Change fail!");
            return;
          } else {
            toast.success("Change Successfully!");

            fetchData(searchParams);
          }
        }
      });
  };

  const onPageChange = (pageNumber) => {
    const newSearchParams = { ...searchParams, pageNumber: pageNumber };
    setSearchParams(newSearchParams);
    fetchData(newSearchParams);
  };
  const filterConditions = [];

  const filterDataGroup = (value) => {
    const filter = {
      field: "data_group",
      value: value,
      condition: ConditionEnum.Equal,
    };
    filterConditions.push(filter);
    const newSearchParams = {
      ...searchParams,
      filterConditions: filterConditions,
    };
    setSearchParams(newSearchParams);
    fetchData(newSearchParams);
    // console.log(newSearchParams);
  };

  const onFilter = (filter) => {
    console.log(searchParams);
    const filterConditions = searchParams.filterConditions.filter(
      (obj) => obj.field !== filter.field
    );
    if (filter.value !== "all") {
      filterConditions.push(filter);
    }
    console.log(filter.field);
    const newSearchParams = {
      ...searchParams,
      filterConditions: filterConditions,
    };
    console.log(newSearchParams);
    setSearchParams(newSearchParams);
    // fetchData(newSearchParams);
  };

  const onSearch = (filter) => {
    if (filter.field === "") {
      return;
    }
    // console.log(filter);
    const filterConditions = searchParams.filterConditions.filter(
      (obj) => obj.condition !== ConditionEnum.Like
    );
    filterConditions.push(filter);
    const newSearchParams = {
      ...searchParams,
      filterConditions: filterConditions,
    };
    // console.log(newSearchParams);
    setSearchParams(newSearchParams);
    fetchData(newSearchParams);
  };

  const onReset = () => {
    // const filterConditions = searchParams.filterConditions.filter(
    //   (obj) => obj.field !== 'status' && obj.field !== 'setting_id'
    // );
    let value = 0;
    tab === "role" ? (value = 1) : "";
    tab === "semester" ? (value = 2) : "";
    tab === "domain" ? (value = 3) : "";
    const newSearchParams = {
      ...searchParams,
      filterConditions: [
        {
          field: "data_group",
          value: value,
          condition: ConditionEnum.Equal,
        },
      ],
    };
    setSearchParams(newSearchParams);
    setCheckedSearchSelect(null);
    setCheckedSearchInput(null);
    fetchData(newSearchParams);
  };

  const onResetSearchSelect = (value) => {
    setCheckedSearchSelect(value);
  };

  const onResetSearchInput = (value) => {
    setCheckedSearchInput(value);
  };

  useEffect(() => {
    filterDataGroup(1);
    // fetchData(searchParams);
  }, []);

  return (
    <>
      <ToastContainer autoClose="2000" theme="colored" />
      <NavbarDashboard
        dashboardBody={
          // <div className="col-xl-12 body-card">
          <Box className="box w-100 d-flex flex-column flexGrow_1">
            <div className="col-xl-12">
              <div className="card custom-card">
                <div className="card-body">
                  <div className="contact-header">
                    <div className="row align-items-center justify-content-between d-flex flex-row">
                      <div className=" align-items-center justify-content-between col-xl-1 col-l h5 fw-bold mb-0 pe-0 d-flex flex-row flexGrow_1">
                        <div>
                          <h6 className="fw-bold mb-0 title-setting">
                            System Setting List
                          </h6>
                        </div>
                        <div>
                          <ul
                            className="nav nav-tabs nav-tabs-header mb-0 d-sm-flex d-block"
                            role="tablist"
                          >
                            <li className="nav-item m-1">
                              <a
                                className="nav-link pointer active"
                                data-bs-toggle="tab"
                                onClick={() => {
                                  setTab("role");
                                  filterDataGroup(1);
                                }}
                              >
                                Role
                              </a>
                            </li>
                            <li className="nav-item pointer m-1">
                              <a
                                className="nav-link"
                                data-bs-toggle="tab"
                                onClick={() => {
                                  setTab("semester");
                                  filterDataGroup(2);
                                }}
                              >
                                Semester
                              </a>
                            </li>
                            <li className="nav-item pointer m-1">
                              <a
                                className="nav-link"
                                data-bs-toggle="tab"
                                onClick={() => {
                                  setTab("domain");
                                  filterDataGroup(3);
                                }}
                              >
                                Permitted Email Domains
                              </a>
                            </li>
                          </ul>
                        </div>
                        <div className="d-flex mt-sm-0 mt-2 align-items-center float-right">
                          <div className="btn-list"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xl-12">
              <div className="card custom-card mb-0 flexGrow_1">
                <div
                  className="row card-body   d-flex flex-column"
                  // style={{ backgroundColor: "#eeeeee" }}
                >
                  <div>
                    <div className="row mt-sm-0 mt-2 mb-3 align-items-center justify-content-between ">
                      <div className="col-lg-7 col-md-3 my-auto">
                        <BaseSearch
                          className="col-lg-9 col-md-8 p-0 m-0"
                          placeholderInput="Search here..."
                          placeholderSelect="Search by"
                          options={searchSetting}
                          onSearch={onSearch}
                          checkedSearchSelect={checkedSearchSelect}
                          onResetSearchSelect={onResetSearchSelect}
                          checkedSearchInput={checkedSearchInput}
                          onResetSearchInput={onResetSearchInput}
                        />
                      </div>
                      <div className="col-lg-5 col-md-8 mt-sm-0 mt-2 position-relative align-items-center float-end ">
                        {/* <div className=" align-items-center float-end "> */}
                        <NewSystemSetting
                          fetchData={fetchData}
                          searchParams={searchParams}
                        />
                        {/* </div> */}
                        <div className="col-lg-7 float-end me-5 mt-2 d-flex h-100 justify-content-end">
                          <FilterOutlined
                            className={
                              dropdown === true
                                ? "filterIcon iconActive position-relative my-auto float-end me-3"
                                : "filterIcon position-relative my-auto float-end me-3"
                            }
                            onClick={() => setDropdown(!dropdown)}
                            // onBlur={() => setDropdown(false)}
                          />
                          {loading ? (
                            <LoadingOutlined
                              className="filterIcon ms-4 float-end"
                              disabled
                            />
                          ) : (
                            <ReloadOutlined
                              className="filterIcon ms-4 float-end"
                              onClick={() => {
                                setLoading(true);
                                fetchData(searchParams);
                              }}
                            />
                          )}
                        </div>
                        <div
                          className={
                            dropdown === false
                              ? "position-absolute cardDropdown bottom-0 end-0 shadow d-none"
                              : "position-absolute cardDropdown bottom-0 end-0 shadow"
                          }
                        >
                          <div className="card custom-card mb-0">
                            <div className="card-body">
                              <FilterSetting
                                settings={settings}
                                onFilter={onFilter}
                                fetchData={fetchData}
                                searchParams={searchParams}
                                onReset={onReset}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="tab-content task-tabs-container flexGrow_1">
                    <div
                      className="tab-pane show active p-0"
                      id="all-tasks"
                      role="tabpanel"
                      // style={{ minHeight: "380px" }}
                    >
                      <div
                        className={tab === "role" ? "row" : "row d-none"}
                        id="tasks-container"
                      >
                        {settings.data.map((setting) => (
                          <RoleTabSetting
                            setting={setting}
                            key={setting.setting_id}
                            fetchData={fetchData}
                            searchParams={searchParams}
                            handleChangeStatus={handleChangeStatus}
                          />
                        ))}
                      </div>
                      <div
                        className={tab === "semester" ? "row" : "row d-none"}
                        id="tasks-container"
                      >
                        {settings.data.map((setting) => (
                          <SemesterTabSetting
                            setting={setting}
                            key={setting.setting_id}
                            handleChangeStatus={handleChangeStatus}
                          />
                        ))}
                      </div>
                      <div
                        className={tab === "domain" ? "row" : "row d-none"}
                        id="tasks-container"
                      >
                        {settings.data.map((setting) => (
                          <PermittedEmailTabSetting
                            setting={setting}
                            key={setting.setting_id}
                            handleChangeStatus={handleChangeStatus}
                          />
                        ))}
                      </div>
                    </div>
                  </div>
                  <BasePagination
                    pageNumber={searchParams.pageNumber}
                    onPageChange={(pageNumber) => {
                      onPageChange(pageNumber);
                    }}
                    pageSize={searchParams.pageSize}
                    totalRecord={settings.totalRecord}
                  />
                </div>
              </div>
            </div>
          </Box>
          // </div>
        }
      />
    </>
  );
};

export default SystemSettingListPage;
