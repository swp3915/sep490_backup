import { Box, Grid } from "@mui/material";
import { ToastContainer } from "react-toastify";
import { NavbarDashboard } from "src/components/NavbarDashboard/NavbarDashboard";
import { ProjectTable } from "./ProjectTable/ProjectTable";
import { axiosClient } from "src/axios/AxiosClient";
import { useEffect, useState } from "react";
import { SelectInputSetting } from "src/components/Base/BaseSelectInput/SelectInputSetting";
import { ConditionEnum } from "src/enum/Enum";
import { SelectInputSubject } from "src/components/Base/BaseSelectInput/SelectInputSubject";
import { SelectInputClass } from "src/components/Base/BaseSelectInput/SelectInputClass";
import { SelectInputProject } from "src/components/Base/BaseSelectInput/SelectInputProject";
const searchClass = [
  {
    id: "class_code",
    value: "Class Code",
  },
  {
    id: "class_name",
    value: "Class Name",
  },
  {
    id: "subject_code",
    value: "Subject Code",
  },
  {
    id: "subject_name",
    value: "Subject Name",
  },
];
export const ProjectListPage = () => {
  const [loading, setLoading] = useState(false);
  const [semesters, setSemesters] = useState([]);
  const [subjects, setSubjects] = useState([]);
  const [classes, setClasses] = useState({
    totalRecord: 0,
    data: [],
    summary: "",
  });
  const [projects, setProjects] = useState({
    totalRecord: 0,
    data: [],
    summary: "",
  });
  const [searchParams, setSearchParams] = useState({
    pageNumber: 1,
    pageSize: 10,
    sortString: "",
    filterConditions: [],
  });
  const fetchData = async (searchParams) => {
    const { data: userList } = await axiosClient.post(
      "/Project/GetByPaging",
      searchParams
    );
    setProjects(userList);
    setLoading(false);
  };

  const fetchDataSelect = async () => {
    const { data: systemSettingArr } = await axiosClient.post(
      `/Setting/GetFilterData?sortString=display_order ASC`,
      [
        {
          field: "data_group",
          value: "2",
          condition: ConditionEnum.Equal,
        },
      ]
    );
    setSemesters(systemSettingArr);

    const { data: subjectArr } = await axiosClient.post(
      "/Subject/GetFilterData?sortString=created_date ASC",
      []
    );
    setSubjects(subjectArr);

    const { data: classArr } = await axiosClient.post("/Class/GetByPaging", {
      pageNumber: 1,
      pageSize: 10,
      sortString: "",
      filterConditions: [],
    });
    setClasses(classArr);
  };

  useEffect(() => {
    fetchData(searchParams);
    fetchDataSelect();
  }, []);
  console.log(classes.data);
  return (
    <>
      <ToastContainer autoClose="2000" theme="colored" />
      <NavbarDashboard
        position="project"
        dashboardBody={
          <Box className="box w-100 d-flex flex-column flexGrow_1">
            <div className="col-xl-12">
              <div className="card custom-card">
                <div className="card-body">
                  <div className="contact-header">
                    <div className="row align-items-center justify-content-between d-flex flex-row">
                      <div className="col-xl-1 col-l h5 fw-bold mb-0 pe-0 d-flex flex-row flexGrow_1">
                        Project List
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="card custom-card mb-0 flexGrow_1">
              <div className="card-body d-flex flex-column">
                <div className="row">
                  <div className="col-lg-7 col-md-3 my-auto">
                    {/* <BaseSearch
                      className="col-lg-9 col-md-8 p-0 m-0"
                      placeholderInput="Search here..."
                      placeholderSelect="Search by"
                      options={searchClass}
                      onSearch={onSearch}
                      checkedSearchSelect={checkedSearchSelect}
                      onResetSearchSelect={onResetSearchSelect}
                      checkedSearchInput={checkedSearchInput}
                      onResetSearchInput={onResetSearchInput}
                    /> */}
                    <div className="d-flex row">
                      <div className="col-md-3 col-sm-6 px-3">
                        <SelectInputSetting
                          id="semester_id"
                          placeholder="Semester"
                          options={semesters}
                          isFilter={false}
                        />
                      </div>
                      <div className="col-md-3 col-sm-6 px-3">
                        <SelectInputSubject
                          id="subject_id"
                          placeholder="Subject Code"
                          options={subjects}
                          isFilter={false}
                        />
                      </div>
                      <div className="col-md-3 col-sm-6 px-3">
                        <SelectInputClass
                          id="class_id"
                          placeholder="Class Code"
                          options={classes.data}
                          isFilter={false}
                        />
                      </div>
                      <div className="col-md-3 col-sm-6 px-3">
                        <SelectInputProject
                          id="project_id"
                          placeholder="Project Code"
                          options={projects.data}
                          isFilter={false}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-5 col-md-8 mt-sm-0 mt-2 position-relative align-items-center float-end">
                    {/* <NewClass
                      semesters={semesters}
                      subjects={subjects}
                      teachers={teachers}
                      fetchData={fetchData}
                      searchParams={searchParams}
                    /> */}

                    <div className="col-lg-7 float-end me-5 d-flex h-100 justify-content-end">
                      {/* <BaseFilter
                        icon={<FilterOutlined className="filterIcon" />}
                        filterBody={
                          <div className="cardDropdown" style={{ zIndex: 1 }}>
                            <div className="card custom-card mb-0">
                              <div className="card-body filterCard">
                                <FilterClass
                                  subjects={subjects}
                                  semesters={semesters}
                                  onFilter={onFilter}
                                  fetchData={fetchData}
                                  searchParams={searchParams}
                                  onReset={onReset}
                                />
                              </div>
                            </div>
                          </div>
                        }
                      /> */}
                      {/* {loading ? (
                        <LoadingOutlined
                          className="filterIcon ms-4 float-end"
                          disabled
                        />
                      ) : (
                        <ReloadOutlined
                          className="filterIcon ms-4 float-end"
                          onClick={() => {
                            setLoading(true);
                            let reLoad = {
                              ...searchParams,
                              filterConditions: [],
                            };
                            setSearchParams(reLoad);
                            fetchData(reLoad);
                          }}
                        />
                      )} */}
                    </div>
                  </div>
                </div>
                <Grid container className="m-0 flexGrow_1">
                  <Grid
                    item
                    md={12}
                    sm={12}
                    xs={12}
                    className="d-flex flex-column"
                  >
                    {/* <ClassTable
                      classes={classes}
                      semesters={semesters.data}
                      searchParams={searchParams}
                      onPageChange={onPageChange}
                      fetchData={fetchData}
                    /> */}
                    <ProjectTable
                      projects={projects}
                      searchParams={searchParams}
                      fetchData={fetchData}
                    />
                  </Grid>
                </Grid>
              </div>
            </div>
          </Box>
        }
      />
    </>
  );
};
