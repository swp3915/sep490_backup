import {
  FilterOutlined,
  LoadingOutlined,
  ReloadOutlined,
} from "@ant-design/icons";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import { useEffect, useState } from "react";
import { axiosClient } from "src/axios/AxiosClient";
import { BaseSearch } from "src/components/Base/BaseSearch/BaseSearch";
import { NavbarDashboard } from "src/components/NavbarDashboard/NavBarDashboard";
import { ConditionEnum } from "src/enum/Enum";
import "./UserListPage.scss";
import { FilterUser } from "./components/FilterUser/FilterUser";
import { NewUser } from "./components/NewUser/NewUser";
import { UserTable } from "./components/UserTable/UserTable";
import { ToastContainer } from "react-toastify";
import { searchUtils, filterUtils } from "/src/utils/handleSearchFilter";
import { FilterDrop } from "./FilterDrop";
import { BaseFilter } from "src/components/Base/BaseFilter/BaseFilter";
const searchUser = [
  {
    id: "fullname",
    value: "Fullname",
  },
  {
    id: "email",
    value: "Email",
  },
  {
    id: "phone_number",
    value: "Phone",
  },
];
const filter = {
  field: "",
  value: "",
  condition: ConditionEnum.Equal,
};

export const UserListPage = () => {
  const [users, setUsers] = useState({
    totalRecord: 0,
    data: [],
    summary: "",
  });
  const [roles, setRoles] = useState([]);
  const [dropdown, setDropdown] = useState(false);
  const [loading, setLoading] = useState(false);
  const [checkedSearchSelect, setCheckedSearchSelect] = useState(null);
  const [checkedSearchInput, setCheckedSearchInput] = useState(null);
  const [roleParams, setRoleParams] = useState([
    {
      field: "data_group",
      value: "1",
      condition: 1,
    },
  ]);
  const [searchParams, setSearchParams] = useState({
    pageNumber: 1,
    pageSize: 10,
    sortString: "",
    filterConditions: [],
  });

  // const searchParams = {
  //   pageNumber: 1,
  //   pageSize: 5,
  //   sortString: "",
  //   filterConditions: [],
  // };

  const fetchData = async (searchParams) => {
    const { data: userList } = await axiosClient.post(
      "/User/GetByPaging",
      searchParams
    );
    setUsers(userList);
    setLoading(false);
  };

  const fetchDataSelect = async () => {
    const { data: roleArr } = await axiosClient.post(
      `/Setting/GetFilterData?sortString=display_order ASC`,
      roleParams
    );
    setRoles(roleArr);
  };

  const onPageChange = (pageNumber) => {
    const newSearchParams = { ...searchParams, pageNumber: pageNumber };
    setSearchParams(newSearchParams);
    fetchData(newSearchParams);
  };

  const onSearch = (filter) => {
    searchUtils(filter, searchParams, setSearchParams, fetchData);
  };

  const onFilter = (filter) => {
    filterUtils(filter, searchParams, setSearchParams, fetchData);
  };

  const onReset = () => {
    // const filterConditions = searchParams.filterConditions.filter(
    //   (obj) => obj.field !== 'status' && obj.field !== 'setting_id'
    // );
    const newSearchParams = {
      ...searchParams,
      filterConditions: [],
    };
    setSearchParams(newSearchParams);
    setCheckedSearchSelect(null);
    setCheckedSearchInput(null);
    fetchData(newSearchParams);
  };

  const onResetSearchSelect = (value) => {
    setCheckedSearchSelect(value);
  };

  const onResetSearchInput = (value) => {
    setCheckedSearchInput(value);
  };

  useEffect(() => {
    // const interval = 3000;
    // const apiCallInterval = setInterval(fetchData(searchParams), interval);
    // return () => {
    // clearInterval(apiCallInterval);
    // };
    fetchDataSelect();
    fetchData(searchParams);
  }, []);

  return (
    <>
      <ToastContainer autoClose="2000" theme="colored" />
      <NavbarDashboard
        position="user"
        dashboardBody={
          <Box className="box w-100 d-flex flex-column flexGrow_1">
            <div className="col-xl-12">
              <div className="card custom-card">
                <div className="card-body">
                  <div className="contact-header">
                    <div className="row align-items-center justify-content-between d-flex flex-row">
                      <div className="col-xl-1 col-l h5 fw-bold mb-0 pe-0 d-flex flex-row flexGrow_1">
                        User List
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="card custom-card mb-0 flexGrow_1">
              <div className="card-body d-flex flex-column">
                <div className="row">
                  <div className="col-lg-7 col-md-3 my-auto">
                    <BaseSearch
                      className="col-lg-9 col-md-8 p-0 m-0"
                      placeholderInput="Search here..."
                      placeholderSelect="Search by"
                      options={searchUser}
                      onSearch={onSearch}
                      checkedSearchSelect={checkedSearchSelect}
                      onResetSearchSelect={onResetSearchSelect}
                      checkedSearchInput={checkedSearchInput}
                      onResetSearchInput={onResetSearchInput}
                    />
                  </div>
                  <div className="col-lg-5 col-md-8 mt-sm-0 mt-2 position-relative align-items-center float-end">
                    <NewUser
                      roles={roles}
                      fetchData={fetchData}
                      searchParams={searchParams}
                    />
                    <div className="col-lg-7 float-end me-5 d-flex h-100 justify-content-end">
                      <BaseFilter
                        icon={<FilterOutlined className="filterIcon" />}
                        filterBody={
                          <div className="cardDropdown" style={{ zIndex: 1 }}>
                            <div className="card custom-card mb-0">
                              <div className="card-body filterCard">
                                <FilterUser
                                  roles={roles}
                                  onFilter={onFilter}
                                  fetchData={fetchData}
                                  searchParams={searchParams}
                                  onReset={onReset}
                                />
                              </div>
                            </div>
                          </div>
                        }
                      />
                      {loading ? (
                        <LoadingOutlined
                          className="filterIcon ms-4 float-end"
                          disabled
                        />
                      ) : (
                        <ReloadOutlined
                          className="filterIcon ms-4 float-end"
                          onClick={() => {
                            setLoading(true);
                            let reLoad = {
                              ...searchParams,
                              filterConditions: [],
                            };
                            setSearchParams(reLoad);
                            fetchData(reLoad);
                          }}
                        />
                      )}
                    </div>
                  </div>
                </div>
                <Grid container className="m-0 flexGrow_1">
                  <Grid
                    item
                    md={12}
                    sm={12}
                    xs={12}
                    className=" d-flex flex-column"
                  >
                    <UserTable
                      users={users}
                      roles={roles}
                      searchParams={searchParams}
                      onPageChange={onPageChange}
                      fetchData={fetchData}
                    />
                  </Grid>
                </Grid>
              </div>
            </div>
          </Box>
        }
      />
    </>
  );
};
