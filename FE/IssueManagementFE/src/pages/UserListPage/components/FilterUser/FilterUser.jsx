import React, { useRef, useState } from "react";
import { DatePicker } from "antd";
import dayjs from "dayjs";
import { SelectInputStatus } from "src/components/Base/BaseSelectInput/SelectInputStatus";
import { SelectInputSetting } from "src/components/Base/BaseSelectInput/SelectInputSetting";
import { BaseButton } from "src/components/Base/BaseButton/BaseButton";
import { ConditionEnum } from "src/enum/Enum";

const { RangePicker } = DatePicker;
const status = [
  {
    value: 1,
    label: "Active",
  },
  {
    value: 0,
    label: "Inactive",
  },
];
export const FilterUser = ({
  roles,
  onFilter,
  fetchData,
  searchParams,
  onReset,
}) => {
  const [checkedSetting, setCheckedSetting] = useState();
  const [checkedStatus, setCheckedStatus] = useState();
  const onChangeSetting = (value) => {
    setCheckedSetting(value)
  }
  const onChangeStatus = (value) => {
    setCheckedStatus(value)
  }
  return (
    <>
      <div className="d-flex row">
        <SelectInputSetting
          label="Role"
          id="setting_id"
          classNameDiv="col-11 mx-auto p-0"
          placeholder="Role"
          options={roles}
          isFilter={true}
          onFilter={onFilter}
          checked={checkedSetting}
          onChange={onChangeSetting}
        />
        <SelectInputStatus
          label="Status"
          id="status"
          classNameDiv="col-11 mx-auto p-0 mt-2"
          placeholder="Status"
          options={status}
          isFilter={true}
          onFilter={onFilter}
          checked={checkedStatus}
          onChange={onChangeStatus}
        />
        <div className="col-11 mx-auto p-0 mt-3">
          <BaseButton
            nameTitle="float-end my-0 ms-3 cardBtn"
            value="Save"
            color="success"
            onClick={() => fetchData(searchParams)}
          />
          <BaseButton
            nameTitle="float-end my-0 cardBtn"
            type="button"
            value="Reset"
            color="dark"
            onClick={() => {
              onReset()
              setCheckedSetting(null)
              setCheckedStatus(null)
            }}
          />
        </div>

        {/* <RangePicker
          showTime
          defaultValue={dayjs("2023-10-01 21:57:00", "2023-10-03 21:57:00")}
          className="col-11 mx-auto mt-3 py-2"
          onChange={(e) => {
            console.log(e.target.value);
          }}
        /> */}
      </div>
    </>
  );
};
