import { useNavigate } from "react-router";
import { ToastContainer } from "react-toastify";
import { NavbarDashboard } from "src/components/NavbarDashboard/NavbarDashboard";

export const IssueListPage = () => {
  const navigate = useNavigate();

  return (
    <>
      <ToastContainer autoClose="2000" theme="colored" />
      <NavbarDashboard
        position="issue"
        dashboardBody={
          <div className="d-flex flex-column flexGrow_1">
            <div className="col-xl-12">
              <div className="card custom-card">
                <div className="card-body p-0">
                  <div className="d-flex p-3 align-items-center justify-content-between">
                    <div>
                      <h6 className="fw-bold mb-0 title-setting">Issue List</h6>
                    </div>
                    <div>
                      <ul
                        className="nav nav-tabs nav-tabs-header mb-0 d-sm-flex d-block"
                        role="tablist"
                      >
                        <li className="nav-item m-1">
                          <a className="nav-link pointer active">Requirement</a>
                        </li>
                        <li className="nav-item pointer m-1">
                          <a className="nav-link">Q&A</a>
                        </li>
                        <li className="nav-item pointer m-1">
                          <a className="nav-link">Tasks</a>
                        </li>
                        <li className="nav-item pointer m-1">
                          <a className="nav-link">Defects</a>
                        </li>
                        <li className="nav-item pointer m-1">
                          <a className="nav-link">Issue</a>
                        </li>
                      </ul>
                    </div>
                    <div className="btn-list"></div>
                  </div>
                </div>
              </div>
            </div>
            <div className="card custom-card mb-0 flexGrow_1">
              <div className="card-body d-flex flex-column"></div>
            </div>
          </div>
        }
      />
    </>
  );
};
