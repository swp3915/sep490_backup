import { LockOutlined, UserOutlined } from "@ant-design/icons";
import { useFormik } from "formik";
// import { gapi } from "gapi-script";
import { MDBContainer, MDBIcon } from "mdb-react-ui-kit";
import { useEffect } from "react";
// import { useDispatch } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import { Form, Input, Label } from "reactstrap";
// import { setLoggedInUser } from "src/app/feature/user/UserSlice";
import { axiosClient } from "src/axios/AxiosClient";
import { BaseButton } from "src/components/Base/BaseButton/BaseButton";
import { BaseInputField } from "src/components/Base/BaseInputField/BaseInputField";
import { ModalCmpt } from "src/components/Modal/ModalCmpt";
import * as Yup from "yup";
import "./UserLoginPage.scss";
import ResetPassword from "./components/ResetPassword/ResetPassword";

const UserLoginPage = () => {
  // const clientId =
  //   "436820330351-7d5cjnnng5dgaioda7ej2ab5aed0kor0.apps.googleusercontent.com";
  const navigate = useNavigate();
  // const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      loginInput: null,
      passwordInput: null,
      isRemember: false,
    },
    validationSchema: Yup.object({
      loginInput: Yup.string()
        .required("Email is required")
        .matches(
          /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/,
          "Please enter a valid email address"
        ),
      passwordInput: Yup.string()
        .required("Password is required")
        .matches(
          /^(?=.*[a-zA-Z])(?=.*\d)(?=.*[!@#$%^&*()_+])[a-zA-Z\d!@#$%^&*()_+]{8,19}$/,
          "Password must be 8-19 characters and contain at least one letter, one number and a special character"
        ),
    }),
    onSubmit: async (values) => {
      const { isRemember, ...account } = values;
      const { data, err } = await axiosClient.post("/User/login", account);
      if (err) {
        toast.error(err.response.data);
      } else {
        // if (isRemember) {
        //   localStorage.setItem("user", data);
        // }
        localStorage.setItem("user", data);
        // dispatch(setLoggedInUser(data));
        toast.success("Sign in successful!!!");

        setTimeout(() => {
          navigate("/user-list");
        }, 2000);
      }
      // console.log(values);
      // console.log(data);
    },
  });

  useEffect(() => {
    // function start() {
    //   gapi.client.init({
    //     clientId: clientId,
    //     scope: "",
    //   });
    // }
    // gapi.load("client:auth2", start);
  });

  return (
    <>
      <ToastContainer autoClose="2000" theme="colored" />
      <Form
        className="background-login"
        onSubmit={formik.handleSubmit}
        autoComplete="off"
      >
        <MDBContainer className="p-3 flex-column sign-in__body">
          <div className="d-flex flex-row mt-5 mb-4">
            <MDBIcon fas icon="cubes fa-3x me-3" style={{ color: "#ff6219" }} />
            <span className="h1 fw-bold mb-0">IMS</span>
          </div>
          <div className="d-flex flex-row ">
            <p className="lead fw-normal mb-0">Sign in with</p>
            <BaseButton
              nameTitle="rounded-circle btnLogin_logo mx-3 position-relative"
              color="danger"
              value={
                <MDBIcon
                  fab
                  icon="google"
                  className="position-absolute top-50 start-50 translate-middle"
                />
              }
            />
            <BaseButton
              nameTitle="rounded-circle btnLogin_logo me-3 facebook position-relative"
              color="primary"
              value={
                <MDBIcon
                  fab
                  icon="facebook-f"
                  className="position-absolute top-50 start-50 translate-middle"
                />
              }
            />
            <BaseButton
              nameTitle="rounded-circle btnLogin_logo position-relative"
              color="secondary"
              value={
                <MDBIcon
                  fab
                  icon="twitter"
                  className="position-absolute top-50 start-50 translate-middle"
                />
              }
            />
          </div>
          <div className="divider d-flex align-items-center my-3">
            <p className="text-center  mx-3 mb-0">or continue with</p>
          </div>
          <BaseInputField
            type="text"
            id="loginInput"
            name="loginInput"
            value={formik.values.loginInput}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            placeholder="Enter Email"
            label="Email"
            classNameDiv="col-12 mb-2 "
            important="true"
            icon={<UserOutlined style={{ color: "gray" }} />}
            tooltip="invalid-feedback"
            tooltipName={formik.errors.loginInput}
            classNameInput={
              formik.errors.loginInput && formik.touched.loginInput
                ? "is-invalid"
                : ""
            }
          />

          {formik.errors.loginInputt && formik.touched.loginInput ? (
            <p className="errorMsg"> {formik.errors.loginInput} </p>
          ) : (
            <p className="hiddenMsg">acb</p>
          )}
          <BaseInputField
            type="password"
            id="passwordInput"
            name="passwordInput"
            value={formik.values.passwordInput}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            placeholder="Enter password"
            label="Password"
            classNameDiv="col-12 mb-2 "
            important="true"
            icon={<LockOutlined style={{ color: "gray" }} />}
            classNameInput={
              formik.errors.passwordInput && formik.touched.passwordInput
                ? "is-invalid"
                : ""
            }
          />
          {formik.errors.passwordInput && formik.touched.passwordInput ? (
            <p className="errorMsg"> {formik.errors.passwordInput} </p>
          ) : (
            <p className="hiddenMsg">acb</p>
          )}
          <div className="d-flex justify-content-between mb-4">
            <Label>
              <Input
                type="checkbox"
                style={{
                  borderColor: "gray",
                  marginRight: "10px",
                  width: "0.7rem",
                  height: "0.75rem",
                }}
                name="isRemember"
                checked={formik.values.isRemember}
                onChange={formik.handleChange}
              />
              Remember me
            </Label>
            <ModalCmpt
              classNameBtn="btn btn-light btn-wave waves-effect waves-light px-5 ms-3 my-auto"
              btnToggle="Forgot password?"
              isAnchor={true}
              variant="outline"
              isFooter={false}
              isHeader={false}
              modalBody={<ResetPassword />}
              isImage={true}
              imgSrc="https://cdn-icons-png.flaticon.com/512/6195/6195699.png"
              size="md"
            />
          </div>
          <BaseButton
            color="danger"
            value="SIGN IN"
            nameTitle="login-form-button"
          />
          {/* <Login />
          <Logout /> */}
          <div className="text-center">
            <p>
              Don't have an account?{" "}
              <NavLink
                to="/register"
                style={{
                  cursor: "pointer",
                  fontSize: "13px",
                  color: "#3b71ca",
                  textDecoration: "none",
                }}
              >
                Register
              </NavLink>
            </p>
          </div>
        </MDBContainer>
      </Form>
    </>
  );
};

export default UserLoginPage;
