const ConditionEnum = {
  Equal: 1,
  NotEqual: 2,
  Like: 3,
  NotLike: 4,
  In: 5,
  NotIn: 6,
  IsNull: 7,
  IsNotNull: 8,
  StartsWith: 9,
  EndsWith: 10,
};

const StatusEnum = {
  Inactive: 0,
  Active: 1,
  Pending: 2,
}

const UserEnum = {
  Register: 1,
  Add: 2,
}

const SettingEnum = {
  Role: 1,
  Semester: 2,
  Domain: 3,
}

const IssueSettingEnum = {
  IssueType: 1,
  IssueStatus: 2,
  WorkProcess: 3,
}

export { ConditionEnum, StatusEnum, UserEnum, SettingEnum, IssueSettingEnum };
