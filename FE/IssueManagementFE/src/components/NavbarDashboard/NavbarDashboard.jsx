import {
  CodeOutlined,
  GroupOutlined,
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  ProjectOutlined,
  ReadOutlined,
  SettingOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Button, ConfigProvider, Layout, Menu, theme } from "antd";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router";
import { NavBarAvatar } from "./NavbarAvatar/NavBarAvatar";
import "./NavbarDashboard.scss";

const { Header, Content, Footer, Sider } = Layout;

function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}

export const NavbarDashboard = ({ dashboardBody, position }) => {
  const [mgLeft, setMgLeft] = useState(false);
  const [fixed, setFixed] = useState(false);
  const [collapsed, setCollapsed] = useState(false);
  const navigate = useNavigate();
  const items = [
    {
      type: "divider",
    },
    getItem("User Management", "user", <UserOutlined />),
    getItem("Subject Management", "subject", <ReadOutlined />),
    getItem("Class Management", "class", <GroupOutlined />),
    getItem("Project Management", "project", <ProjectOutlined />),
    getItem("Issue Management", "issue", <CodeOutlined />),
    getItem("System Setting Management", "setting", <SettingOutlined />),
  ];

  const handleBreakpoint = () => {
    // Define your breakpoint logic here
    const isSmallScreen = window.innerWidth <= 1168; // Adjust the breakpoint as needed
    // console.log(isSmallScreen, window.innerWidth);
    setCollapsed(isSmallScreen);
  };

  useEffect(() => {
    // Initial check and add event listener
    handleBreakpoint();
    window.addEventListener("resize", handleBreakpoint);

    // Clean up the event listener when the component unmounts
    return () => {
      window.removeEventListener("resize", handleBreakpoint);
    };
  }, []);
  const onClick = (e) => {
    e.key === "user" ? navigate("/user-list") : "";
    e.key === "subject" ? navigate("/subject-list") : "";
    e.key === "class" ? navigate("/class-list") : "";
    e.key === "project" ? navigate("/project-list") : "";
    e.key === "issue" ? navigate("/issue-list") : "";
    e.key === "setting" ? navigate("/system-setting-list") : "";
  };
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  return (
    <ConfigProvider
      theme={{
        token: {
          colorPrimary: "#ec8550f0",
        },
      }}
    >
      <ConfigProvider
        theme={{
          components: {
            Layout: {
              triggerBg: "none",
              triggerColor: "black",
            },
          },
        }}
      >
        <Layout hasSider>
          <Sider
            trigger={null}
            collapsible
            collapsed={collapsed}
            onCollapse={(value) => setCollapsed(value)}
            width="260px"
            className="siderDashboard"
            style={{
              // overflow: "auto",
              backgroundColor: "rgba(0, 0, 0, 0.05)",
              position: "fixed",
              // position: fixed !== false ? "" : "fixed",
              border: "1px solid rgba(0, 0, 0, 0.1)",
              left: 0,
              top: 0,
              bottom: 0,
              height: "100vh",
            }}
          >
            <div className="demo-logo-vertical" />
            {/* <div className="main-sidebar-header">
          
        </div> */}

            <div
              className={
                collapsed
                  ? "logoDashboard d-flex position-fixed widthUnCollapse"
                  : "logoDashboard d-flex position-fixed widthCollapse"
              }
            >
              <img
                src="/src/images/logoIMS.png"
                className="toggle-dark logoIMS"
                alt="logo"
              ></img>
            </div>

            <Menu
              onClick={onClick}
              mode="inline"
              defaultSelectedKeys={[position]}
              items={items}
              defaultOpenKeys={["sub1"]}
              style={{
                fontFamily: "Roboto, Helvetica, Arial, sans-serif !important",
                backgroundColor: "rgba(0, 0, 0, 0.05)",
                borderColor: "rgba(0, 0, 0, 0.05)",
                paddingTop: "64px",
                height: "100vh",
                fontSize: "16px",
              }}
            ></Menu>
          </Sider>
          <Layout
            className="site-layout"
            style={{
              marginLeft: collapsed ? 80 : 260,
              transition: "all 0.2s",
              minHeight: "100vh",
            }}
          >
            <Header
              className="shadow headerDashboard"
              style={{
                position: "sticky",
              }}
            >
              <Button
                type="text"
                icon={collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
                onClick={() => {
                  setCollapsed(!collapsed);
                  // setFixed(!fixed);
                  // setMgLeft(!mgLeft);
                }}
                style={{
                  fontSize: "16px",
                  width: 64,
                  height: 64,
                }}
              />
              <NavBarAvatar />
            </Header>
            <Content className="navbarContent">{dashboardBody}</Content>
            {/* <Footer
              style={{
                textAlign: "center",
              }}
            >
              Welcome to IMS!
            </Footer> */}
          </Layout>
        </Layout>
      </ConfigProvider>
    </ConfigProvider>
  );
};
