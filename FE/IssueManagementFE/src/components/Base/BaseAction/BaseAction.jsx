import { EditOutlined, SettingOutlined } from "@ant-design/icons";
import { Tooltip } from "antd";
import { useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import { axiosClient } from "src/axios/AxiosClient";
import { BaseButton } from "src/components/Base/BaseButton/BaseButton";
import { StatusEnum } from "src/enum/Enum";
import { swalWithBootstrapButtons } from "src/enum/swal";
import { UserDetails } from "src/pages/UserListPage/components/UserDetails/UserDetails";
import Swal from "sweetalert2";
export const BaseAction = ({
  option,
  optionId,
  modalCmpt,
  roles,
  fetchData,
  searchParams,
  type,
  onClick,
}) => {
  const [modal, setModal] = useState(false);

  const handleChangeStatus = async (optionId, status) => {
    const userIdArr = [];
    userIdArr.push(optionId);
    swalWithBootstrapButtons
      .fire({
        title: "Are you sure?",
        text: `Are you sure to update ${type} Status?`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, update it!",
        cancelButtonText: "No, cancel!",
        reverseButtons: true,
      })
      .then(async (result) => {
        console.log(result);
        if (result.isConfirmed) {
          const { data, err } = await axiosClient.post(
            `/${type}/UpdateStatus?status=${status}`,
            userIdArr
          );

          swalWithBootstrapButtons.fire(
            "Updated!",
            "User has been updated!.",
            "success"
          );
          if (err) {
            toast.error("Change fail!");
            return;
          } else {
            toast.success("Change Successful!");
            fetchData(searchParams);
          }
        } else {
          {
            swalWithBootstrapButtons.fire(
              "Cancelled",
              "Your imaginary file is safe :)",
              "error"
            );
          }
        }
      });
  };
  return (
    <>
      <div className="d-flex justify-content-around">
        <Tooltip title="Details" placement="top" color="#ffc107" size="large">
          <div>
            <BaseButton
              icon={<EditOutlined />}
              variant="outline"
              nameTitle="px-3 py-1"
              color="warning"
              onClick={onClick === undefined ? () => setModal(!modal) : onClick}
            />
          </div>
        </Tooltip>
        {option.status === 1 ? (
          <Tooltip
            title="Inactive"
            placement="top"
            color="#dc3545"
            size="large"
          >
            <div>
              <BaseButton
                icon={<SettingOutlined />}
                variant="outline"
                color="danger"
                nameTitle="px-3 py-1"
                onClick={() =>
                  handleChangeStatus(optionId, StatusEnum.Inactive)
                }
              />
            </div>
          </Tooltip>
        ) : (
          ""
        )}
        {option.status === 0 ? (
          <Tooltip title="Active" placement="top" color="#198754" size="large">
            <div>
              <BaseButton
                icon={<SettingOutlined />}
                variant="outline"
                color="success"
                nameTitle="px-3 py-1"
                onClick={() => handleChangeStatus(optionId, StatusEnum.Active)}
              />
            </div>
          </Tooltip>
        ) : (
          ""
        )}

        {type === "User" ? (
          <UserDetails
            roles={roles}
            modal={modal}
            setModal={setModal}
            user={option}
            fetchData={fetchData}
            searchParams={searchParams}
          />
        ) : (
          ""
        )}
      </div>
    </>
  );
};
