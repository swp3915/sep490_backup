import React, { useState } from "react";
import { Radio } from "antd";
export const BaseRadio = ({ classNameDiv, important, label, type, formik }) => {

  return (
    <>
      <div className={classNameDiv}>
        <label htmlFor="input-placeholder" className="form-label mt-1 me-2">
          {label}
          {important === "true" ? (
            <span className="ms-1" style={{ color: "red" }}>
              *
            </span>
          ) : (
            ""
          )}
        </label>
        <div className="input-group mt-2">
          <Radio.Group
            onChange={(e) => {
              formik.setFieldValue(`${type}`, e.target.value);
            }}
            value={formik.values.status}
          >
            <Radio value={1}>Active</Radio>
            <Radio value={0}>Inactive</Radio>
          </Radio.Group>
        </div>
      </div>
    </>
  );
};
