import { ColorPicker } from "antd";
import React, { useMemo, useState } from "react";
import "./BaseColorPicker.scss";

export const BaseColorPicker = () => {
  const [colorHex, setColorHex] = useState("#1677ff");
  const [formatHex, setFormatHex] = useState("hex");
  const hexString = useMemo(
    () => (typeof colorHex === "string" ? colorHex : colorHex.toHexString()),
    [colorHex]
  );
  console.log(hexString);
  return (
    <>
      <ColorPicker
        showText
        format={formatHex}
        value={colorHex}
        onChange={setColorHex}
        onFormatChange={setFormatHex}
      />
    </>
  );
};
