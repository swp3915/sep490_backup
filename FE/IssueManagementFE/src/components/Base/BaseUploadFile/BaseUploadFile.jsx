import React, { useState } from "react";
import { InboxOutlined, UploadOutlined } from "@ant-design/icons";
import { message, Upload } from "antd";
import { BaseButton } from "../BaseButton/BaseButton";
import { DownloadExcelTemplate } from "src/components/DownloadExcel/DownloadExcel";
import { axiosClient } from "src/axios/AxiosClient";
const { Dragger } = Upload;

export const BaseUploadFile = ({ importFile, classId }) => {
  const [fileExcel, setFileExcel] = useState();
  const props = {
    name: "file",
    multiple: true,
    action: "https://run.mocky.io/v3/435e224c-44fb-4773-9faf-380c5e6a2188",
    onChange(info) {
      const { status } = info.file;
      if (status !== "uploading") {
        console.log(info.file, info.fileList);
      }
      if (status === "done") {
        message.success(`${info.file.name} file uploaded successfully.`);

        // Convert the uploaded file to a File object
        convertToFile(info.file, importFile);
      } else if (status === "error") {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
    onDrop(e) {
      console.log("Dropped files", e.dataTransfer.files);
    },
  };

  const convertToFile = (file, callback) => {
    // Use the File constructor to create a File object
    const fileObject = new File([file], file.name, { type: file.type });
    // Pass the File object to the callback function
    // callback(fileObject);
    setFileExcel(fileObject);
  };

  const handleImport = async () => {
    const formData = new FormData();
    formData.append("file", fileExcel);
    // formData.append("class_id", classId);
    console.log(fileExcel instanceof File);
    const { data, err } = await axiosClient.post(
      `/ClassStudent/Import/${classId}`,
      formData
    );
    console.log(data, err);
  };

  return (
    <>
      <Dragger {...props} className="mb-4">
        <p className="ant-upload-drag-icon">
          <UploadOutlined />
        </p>
        <p className="ant-upload-text">
          Click or drag a file to this area to upload
        </p>
        <p className="ant-upload-hint">
          Support for a single or bulk upload. Strictly prohibited from
          uploading company data or other banned files.
        </p>
      </Dragger>
      <div>
        <DownloadExcelTemplate excelFile="Danhsachsinhvien.xlsx" />
        <BaseButton
          nameTitle="mt-3 w-25 float-end"
          type="button"
          value="Import"
          color="secondary"
          onClick={() => handleImport()}
        />
      </div>
    </>
  );
};
