import { DatePicker } from "antd";
import React, { useState } from "react";
import moment from "moment";
import dayjs from "dayjs";
import customParseFormat from "dayjs/plugin/customParseFormat";
import { ConditionEnum } from "src/enum/Enum";
dayjs.extend(customParseFormat);
export const BaseDatePicker = ({
  id,
  className,
  label,
  important,
  isFilter,
  status,
  onFilter,
  onBlur,
  name,
}) => {
  const [selectedValue, setSelectedValue] = useState([]);
  const [selectedDate, setSelectedDate] = useState(null);

  const handleRangePickerChangeJSON = (value, dateString) => {
    const formattedStartDate = moment(dateString, "MM/DD/YYYY").toISOString();
    //   setSelectedValue([formattedStartDate, formattedEndDate]);
    console.log(formattedStartDate);
    return formattedStartDate;
  };

  let filter = {};
  if (id === "from_date") {
    filter = {
      field: "",
      value: "",
      condition: ConditionEnum.Equal,
    };
  } else if (id === "to_date") {
    filter = {
      field: "",
      value: "",
      condition: ConditionEnum.Equal,
    };
  }
//   const filter = {
//     field: "",
//     value: "",
//     condition: ConditionEnum.Equal,
//   };
  const dateFormat = "DD/MM/YYYY";
  return (
    <>
      <label htmlFor="input-placeholder" className="form-label mt-1 me-2">
        {label}
        {important === "true" ? (
          <span className="ms-1" style={{ color: "red" }}>
            *
          </span>
        ) : (
          ""
        )}
      </label>
      <DatePicker
        id={id}
        status={status}
        className={className}
        name={name}
        style={{ borderRadius: "4px" }}
        format={dateFormat}
        // defaultValue={value === "" ? null : dayjs(value, dateFormat)}
        onChange={(value, dateString) => {
          if (isFilter) {
            const newFilter = {
              ...filter,
              field: "status",
              value: handleRangePickerChangeJSON(value, dateString),
            };
            onFilter(newFilter);
          }
        }}
        onBlur={onBlur}
      />
    </>
  );
};
