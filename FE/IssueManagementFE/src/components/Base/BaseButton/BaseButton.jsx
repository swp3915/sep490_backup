import React from "react";

export const BaseButton = ({
  color,
  disabled,
  variant,
  onClick,
  value,
  nameTitle,
  icon,
  type,
}) => {
  const className = `${nameTitle} btn btn-wave waves-effect waves-light btn-${
    variant === "outline" ? variant + "-" : ""
  }${color}${variant === "light" ? "-" + variant : ""}`;
  return (
    <>
      <button type={type} onClick={onClick} className={className}>
        {value} <span>{icon}</span>
      </button>
    </>
  );
};
