import { Select } from "antd";
import { useState } from "react";
import { ConditionEnum } from "src/enum/Enum";
const { Option } = Select;

export const SelectInputSettingGroup = ({
  label,
  classNameDiv,
  important,
  defaultValue,
  options,
  onChange,
  placeholder,
  disabled,
  id,
  isFilter,
  formik,
  onFilter,
  handleCheckDomain,
  status,
  onBlur,
  checked,
}) => {
  // const roleArr = [];
  // for (let index = 0; index < options.length; index++) {
  //   const element = options[index];
  //   roleArr.push({
  //     value: type === "role" ? element.setting_id : element.value,
  //     label: type === "role" ? element.setting_value : element.label,
  //   });
  // }
  const filter = {
    field: "",
    value: "",
    condition: ConditionEnum.Equal,
  };
  {
    defaultValue === 1 ? (defaultValue = "Role") : "";
  }
  {
    defaultValue === 2 ? (defaultValue = "Semester") : "";
  }
  {
    defaultValue === 3 ? (defaultValue = "Email Domain") : "";
  }
  return (
    <>
      <div className={classNameDiv}>
        <label htmlFor="input-placeholder" className="form-label mt-1">
          {label}
          {important === "true" ? (
            <span className="ms-1" style={{ color: "red" }}>
              *
            </span>
          ) : (
            ""
          )}
        </label>
        <Select
          showSearch
          className="selectDesign"
          defaultValue={defaultValue}
          value={checked}
          id={id}
          status={status}
          onBlur={onBlur}
          placeholder={placeholder}
          disabled={disabled}
          onChange={(value, object) => {
            if (isFilter) {
              const newFilter = {
                ...filter,
                field: value,
                value: object.key,
              };
              onFilter(newFilter);
              onChange(value);
            } else {
              formik.setFieldValue(`${id}`, object.key);
              handleCheckDomain(object.key);
            }
            // console.log(filter);
          }}
        >
          {isFilter === true ? (
            <Option key="all" value="all">
              All{" "}
            </Option>
          ) : (
            ""
          )}

          {options.map((option) => (
            <Option key={option.value} value={option.label}>
              {option.label}
            </Option>
          ))}
        </Select>
      </div>
    </>
  );
};
