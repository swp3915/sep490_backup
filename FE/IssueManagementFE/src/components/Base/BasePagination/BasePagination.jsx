import { ConfigProvider, Pagination } from "antd";
import React from "react";

export const BasePagination = ({
  pageNumber,
  pageSize,
  onPageChange,
  totalRecord,
}) => {
  return (
    <>
      <ConfigProvider
        theme={{
          token: {
            colorPrimary: "#ec8550f0",
          },
        }}
      >
        {" "}
        <Pagination
          className="text-center"
          defaultPageSize={pageSize}
          current={pageNumber}
          total={totalRecord}
          pageSizeOptions={["6", "12", "18", "24"]}
          showSizeChanger
          showQuickJumper
          showTotal={(total) => `Total ${total} records`}
          onChange={(pageIndex) => {
            onPageChange(pageIndex);
            console.log(pageSize)
            // fetchData()
          }}
        />
      </ConfigProvider>
    </>
  );
};
