import { BaseButton } from "../Base/BaseButton/BaseButton";

export const DownloadExcelTemplate = ({ excelFile }) => {
  const downloadExcelTemplate = () => {
    // Đường dẫn tới tệp Excel mẫu
    const templateFileURL = `/src/excel/${excelFile}`;

    // Tạo một liên kết ảo
    const link = document.createElement("a");
    link.href = templateFileURL;
    link.download = "excel_template.xlsx"; // Tên của tệp khi tải về
    link.click();
  };

  return (
    <>
      <BaseButton
        nameTitle="mt-3 w-25"
        type="button"
        value="Template"
        color="light"
        onClick={downloadExcelTemplate}
      />
    </>
  );
};
