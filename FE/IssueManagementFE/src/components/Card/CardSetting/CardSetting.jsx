import { DeleteOutlined, SettingOutlined } from "@ant-design/icons";
import { BaseButton } from "src/components/Base/BaseButton/BaseButton";
import { SystemSettingDetails } from "src/pages/SystemSettingListPage/components/SystemSettingDetails/SystemSettingDetails";
import "./CardSetting.scss";
// import TextTruncate from "react-text-truncate";
import { Tooltip } from "antd";
import TextTruncate from "react-text-truncate";
// import TextTruncate from "react-text-truncate";
export const CardSetting = ({
  nameTitle,
  roleName,
  settingGroup,
  displayOrder,
  description,
  status,
  layoutCard,
  setting,
  fetchData,
  searchParams,
  handleChangeStatus,
}) => {
  const className = ` card shadow custom-card task-${nameTitle}-card`;
  const taskCard = `${layoutCard} task-card `;
  return (
    <>
      <div className={taskCard}>
        <div className={className}>
          <div className="card-body  p-2">
            <div className="justify-content-between flex-wrap gap-2">
              <div>
                <p
                  className="fw-semibold mb-3 d-flex align-items-center"
                  style={{ fontWeight: "bold" }}
                >
                  {roleName}
                </p>
                <p className="mb-3">
                  Setting Group :{" "}
                  <span className="fs-12 mb-1 text-muted">{settingGroup}</span>
                </p>
                <p className="mb-3">
                  Display Order :{" "}
                  <span className="fs-12 mb-1 text-muted">{displayOrder}</span>
                </p>
                <p className="mb-2 text truncate">
                  <span>Description: </span>
                  <span className="fs-12 mb-1 text-muted">
                    <TextTruncate
                      line={1}
                      element="span"
                      truncateText="…"
                      text={description}
                    />
                  </span>
                </p>
              </div>
              <div>
                <div className="btn-list d-flex m-0">
                  <div className="m-0 mt-2">
                    <SystemSettingDetails
                      setting={setting}
                      fetchData={fetchData}
                      searchParams={searchParams}
                    />
                  </div>

                  {setting.status === 1 ? (
                    <Tooltip
                      title="Inactive"
                      placement="top"
                      color="
                    #dc3545"
                      size="large"
                    >
                      <div className="m-0 mt-2">
                        <BaseButton
                          icon={<SettingOutlined />}
                          variant="outline"
                          nameTitle="px-2 py-1 "
                          color="danger"
                          onClick={() => {
                            handleChangeStatus(setting.setting_id, 0);
                          }}
                        />
                      </div>
                    </Tooltip>
                  ) : (
                    ""
                  )}
                  {setting.status === 0 ? (
                    <Tooltip
                      title="Active"
                      placement="top"
                      color="
                    #26BF94"
                      size="large"
                    >
                      <div className="m-0 mt-2">
                        <BaseButton
                          icon={<SettingOutlined />}
                          variant="outline"
                          nameTitle="px-2 py-1"
                          color="success"
                          onClick={() => {
                            handleChangeStatus(setting.setting_id, 1);
                          }}
                        />
                      </div>
                    </Tooltip>
                  ) : (
                    ""
                  )}
                </div>
                {status === 1 ? (
                  <span className="badge bg-success d-block mb-2">active</span>
                ) : (
                  <span className="badge bg-danger d-block  mb-2">
                    inactive
                  </span>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
