import React from "react";
import { Navigate } from "react-router";
import useAuth from "src/hooks/useAuth";
export const AuthoRoutes = ({ path, element, role }) => {
  const { accessToken, isLogin, currentUser } = useAuth();

  return (
    <>
      {isLogin() && currentUser?.role === role ? (
        element
      ) : (
        <Navigate to="/sign-in" />
      )}
    </>
  );
};
