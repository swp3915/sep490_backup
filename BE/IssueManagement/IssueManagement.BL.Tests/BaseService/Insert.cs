﻿using IssueManagement.BL.Tests.BaseService.Fakes;
using IssueManagement.Common.Entities;
using IssueManagement.Common.Exception;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.BL.Tests.BaseService
{
    [TestFixture]
    public class Insert : BaseServiceBaseTest
    {

        [Test(Author = "KBNGUYET", Description = "Test hàm update dữ liệu")]
        public void ConfirmDataReturn_ReturnFalse()
        {
            int ex = 0;
            // mock
            _repoMock.Setup(x => x.Insert(It.IsAny<object>())).Returns(ex);
            // run
            var rs = _serviceInherits.Object.Insert(It.IsAny<object>());
            // verify
            Assert.AreEqual(rs, ex > 0 ? true : false);
        }

        [Test(Author = "KBNGUYET", Description = "Test hàm update dữ liệu")]
        public void ConfirmDataReturn_ReturnTrue()
        {
            // init
            int ex = 1;
            User obj = new User
            {
                fullname = "KBnguyet"
            };
            // mock
            _repoMock.Setup(x => x.Insert(obj)).Returns(ex);
            // run
            var rs = _serviceInherits.Object.Insert(obj);
            // verify
            Assert.AreEqual(rs, ex > 0 ? true : false);
        }

        [Test(Author = "KBNGUYET", Description = "Test hàm update dữ liệu")]
        public void ConfirmDataReturn_ThrowException()
        {
            // init
            User obj = new User
            {
                fullname = "KBnguyet"
            };
            // mock
            _repoMock.Setup(x => x.Insert(obj)).Throws(_exception);
            // run
            var rs = Assert.Throws<Exception>(() => _serviceInherits.Object.Insert(obj));
            // verify
            Assert.AreEqual(rs, _exception);
        }

    }
}
