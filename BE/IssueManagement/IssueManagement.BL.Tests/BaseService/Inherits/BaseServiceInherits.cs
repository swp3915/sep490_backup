﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IssueManagement.BL.BaseService;
using IssueManagement.Common.UnitOfWork;
using IssueManagement.Common.Utilities;
using IssueManagement.DL.BaseRepository;

namespace IssueManagement.BL.Tests.BaseService.Inherits
{
    public class BaseServiceInherits : IssueManagement.BL.BaseService.BaseService<object, object>
    {
        public BaseServiceInherits(IBaseRepository<object, object> baseRepository, IUnitOfWork unitOfWork, CommonUtility commonUtility) : base(baseRepository, unitOfWork, commonUtility)
        {
        }

        public void ValidateData(object entity, int action = -1)
        {
            base.ValidateData(entity, action);
        }

        public void ValidateReferenceField(object entity)
        {
            base.ValidateReferenceField(entity);
        }

    }
}
