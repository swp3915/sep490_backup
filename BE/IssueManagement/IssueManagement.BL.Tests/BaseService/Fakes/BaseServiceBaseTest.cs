﻿using IssueManagement.BL.Tests.BaseService.Inherits;
using IssueManagement.Common.Exception;
using IssueManagement.Common.UnitOfWork;
using IssueManagement.Common.Utilities;
using IssueManagement.DL.BaseRepository;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.BL.Tests.BaseService.Fakes
{
    public abstract class BaseServiceBaseTest
    {
        protected Mock<IBaseRepository<object, object>> _repoMock;
        protected Mock<IUnitOfWork> _unitOfWork;
        protected Mock<BaseServiceInherits> _serviceInherits;
        protected Mock<CommonUtility> _commonUtility;
        protected Exception _exception;

        [SetUp]
        public virtual void Setup()
        {
            _repoMock = new Mock<IBaseRepository<object, object>>();
            _unitOfWork = new Mock<IUnitOfWork>();
            _commonUtility = new Mock<CommonUtility>();
            _exception = new Exception();
            _serviceInherits = new Mock<BaseServiceInherits>(_repoMock.Object, _unitOfWork.Object, _commonUtility.Object);
           
        }
    }
}
