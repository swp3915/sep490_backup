﻿using IssueManagement.BL.Tests.BaseService.Fakes;
using IssueManagement.Common.Entities;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.BL.Tests.BaseService
{
    [TestFixture]
    public class Update : BaseServiceBaseTest
    {

        [Test(Author = "KBNGUYET", Description = "Test hàm update dữ liệu")]
        public void ConfirmDataReturn_ReturnFalse()
        {
            int ex = 0;
            // mock
            _repoMock.Setup(x => x.Update(It.IsAny<Guid>(), It.IsAny<object>())).Returns(ex);
            // run
            var rs = _serviceInherits.Object.Update(It.IsAny<Guid>(), It.IsAny<object>());
            // verify
            Assert.AreEqual(rs, ex > 0 ? true : false);
        }

        [Test(Author = "KBNGUYET", Description = "Test hàm update dữ liệu")]
        public void ConfirmDataReturn_ReturnTrue()
        {
            // init
            int ex = 1;
            User obj = new User
            {
                fullname = "KBnguyet"
            };
            // mock
            _repoMock.Setup(x => x.Update(It.IsAny<Guid>(), obj)).Returns(ex);
            // run
            var rs = _serviceInherits.Object.Update(It.IsAny<Guid>(), obj);
            // verify
            Assert.AreEqual(rs, ex > 0 ? true : false);
        }

        [Test(Author = "KBNGUYET", Description = "Test hàm update dữ liệu")]
        public void ConfirmDataReturn_ThrowException()
        {
            // init
            User obj = new User
            {
                fullname = "KBnguyet"
            };
            // mock
            _repoMock.Setup(x => x.Update(It.IsAny<Guid>(), obj)).Throws(_exception);
            // run
            var rs = Assert.Throws<Exception>(() => _serviceInherits.Object.Update(It.IsAny<Guid>(), obj));
            // verify
            Assert.AreEqual(rs, _exception);
        }
    }
}
