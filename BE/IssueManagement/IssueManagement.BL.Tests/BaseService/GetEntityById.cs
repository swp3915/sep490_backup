﻿using IssueManagement.BL.Tests.BaseService.Fakes;
using IssueManagement.Common.Model;
using IssueManagement.Common.Pagination;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.BL.Tests.BaseService
{
    [TestFixture]
    public class GetEntityById : BaseServiceBaseTest
    {

        [Test(Author = "KBNGUYET", Description = "Test hàm lấy dữ liệu bản ghi theo id, có trả về dữ liệu")]
        public void ConfirmDataReturn_ReturnModel()
        {
            // init
            Object model = new Object();
            // mock
            _repoMock.Setup(x => x.GetEntityById(It.IsAny<Guid>())).Returns(model);
            // run
            var rs = _serviceInherits.Object.GetEntityById(It.IsAny<Guid>());
            // verify
            Assert.IsNotNull(rs);
            Assert.AreEqual(model, rs);
        }

        [Test(Author = "KBNGUYET", Description = "Test hàm lấy dữ liệu bản ghi theo id, trả về null")]
        public void ConfirmDataReturn_ReturnNull()
        {
            // init
            Object model = null;
            // mock
            _repoMock.Setup(x => x.GetEntityById(It.IsAny<Guid>())).Returns(model);
            // run
            var rs = _serviceInherits.Object.GetEntityById(It.IsAny<Guid>());
            // verify
            Assert.IsNull(rs);
        }

        [Test(Author = "KBNGUYET", Description = "Test hàm lấy dữ liệu bản ghi theo id, xảy ra lỗi")]
        public void ConfirmDataReturn_ReturnThrowException()
        {
            // mock
            _repoMock.Setup(x => x.GetEntityById(It.IsAny<Guid>())).Throws(_exception);
            // run
            var rs = Assert.Throws<Exception>(() => _serviceInherits.Object.GetEntityById(It.IsAny<Guid>()));
            // verify
            Assert.AreEqual(_exception, rs);
        }
    }
}
