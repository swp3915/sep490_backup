﻿using IssueManagement.BL.Tests.BaseService.Fakes;
using IssueManagement.Common.Model;
using IssueManagement.Common.Pagination;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.BL.Tests.BaseService
{
    [TestFixture]
    public class GetByPaging: BaseServiceBaseTest
    {

        [Test(Author = "KBNGUYET", Description = "Test hàm lấy danh sách theo phân trang, có trả về dữ liệu")]
        public void ConfirmDataReturn_ReturnModel()
        {
            // init
            PagingModel model = new PagingModel();
            // mock
            _repoMock.Setup(x => x.GetByPaging(It.IsAny<PagingParam>())).Returns(model);
            // run
            var rs = _serviceInherits.Object.GetByPaging(It.IsAny<PagingParam>());
            // verify
            Assert.IsNotNull(rs);
            Assert.AreEqual(model, rs);
        }

        [Test(Author = "KBNGUYET", Description = "Test hàm lấy danh sách theo phân trang, trả về null")]
        public void ConfirmDataReturn_ReturnNull()
        {
            // init
            PagingModel model = null;
            // mock
            _repoMock.Setup(x => x.GetByPaging(It.IsAny<PagingParam>())).Returns(model);
            // run
            var rs = _serviceInherits.Object.GetByPaging(It.IsAny<PagingParam>());
            // verify
            Assert.IsNull(rs);
        }

        [Test(Author = "KBNGUYET", Description = "Test hàm lấy danh sách theo phân trang, exception xảy ra")]
        public void ConfirmDataReturn_ThrowException()
        {
            // mock
            _repoMock.Setup(x => x.GetByPaging(It.IsAny<PagingParam>())).Throws(_exception);
            // run
            var rs = Assert.Throws<Exception>(() => _serviceInherits.Object.GetByPaging(It.IsAny<PagingParam>()));
            // verify
            Assert.AreEqual(_exception, rs);
        }
    }
}
