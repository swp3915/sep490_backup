﻿namespace IssueManagement.DL.Database
{
    /// <summary>
    /// Lớp này dùng để lấy chuỗi kết nối đến database
    /// </summary>
    /// CreatedBy: NguyetKTB (28/09/2023)
    public class DatabaseContext
    {
        #region Property
        /// <summary>
        /// Chuỗi kết nối đến database
        /// </summary>
        public static string ConnectionString;
        #endregion

    }
}
