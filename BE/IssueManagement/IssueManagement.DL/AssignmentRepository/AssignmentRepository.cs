﻿using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using IssueManagement.Common.UnitOfWork;
using IssueManagement.DL.BaseRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.DL.AssignmentRepository
{
    public class AssignmentRepository : BaseRepository<Assignment, AssignmentDto>, IAssignmentRepository
    {
        public AssignmentRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
