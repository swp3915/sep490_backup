﻿using Dapper;
using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using IssueManagement.Common.Enumeration;
using IssueManagement.Common.Exception;
using IssueManagement.Common.Model;
using IssueManagement.Common.Pagination;
using IssueManagement.Common.Resources;
using IssueManagement.Common.UnitOfWork;
using IssueManagement.Common.Utilities;
using IssueManagement.DL.Database;
using Microsoft.VisualBasic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Transactions;
using static Dapper.SqlMapper;
using static IssueManagement.Common.Attributes.TableAttribute;

namespace IssueManagement.DL.BaseRepository
{
    public class BaseRepository<T, TDto> : IBaseRepository<T, TDto> where T : class
    {

        public readonly IUnitOfWork _unitOfWork;

        public BaseRepository(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Lấy ra connection
        /// </summary>
        /// <returns></returns>
        /// Created By: NguyetKTB 28/09/2023
        public DbConnection GetConnection()
        {
            return _unitOfWork.GetConnection();
        }

        #region GET

        /// <summary>
        /// Lấy danh sách thông tin bản ghi theo lọc
        /// </summary>
        /// <param name="filterConditions"></param>
        /// <returns></returns>
        public IEnumerable<TDto> GetFilterData(List<FilterCondition>? filterConditions, string sortString)
        {
            PagingParam pagingParam = new PagingParam
            {
                FilterConditions = filterConditions
            };
            sortString = string.IsNullOrEmpty(sortString) ? " ORDER BY created_date ASC " : " ORDER BY " + sortString;
            string filterString = BuildPagingCondition<TDto>(pagingParam);
            string procName = DatabaseUtility.GetProcdureName<T>(MSProcdureName.GetFilterData);
            var parameters = new DynamicParameters();
            parameters.Add("filterString", filterString);
            parameters.Add("sortString", sortString);
            var connection = GetConnection();
            var result = connection.Query<TDto>(procName, parameters, commandType: CommandType.StoredProcedure);
            return result;
        }


        /// <summary>
        /// Lấy ra thông tin bản ghi theo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// created by: NGUYETKTB 28/09/2023
        public TDto GetEntityById(Guid id)
        {
            string procName = DatabaseUtility.GetProcdureName<T>(MSProcdureName.GetById);
            string primaryKey = AttributeUtility.GetPrimaryKeyName<T>();
            var parameters = new DynamicParameters();
            parameters.Add("id", id);
            var connection = GetConnection();
            try
            {
                TDto entity = connection.QueryFirstOrDefault<TDto>(procName, parameters, commandType: CommandType.StoredProcedure);
                return entity;
            }
            catch
            {
                throw;
            }
        }


        /// <summary>
        /// Lấy ra danh sách bản ghi theo paging và filter
        /// </summary>
        /// <param name="pagingParam"></param>
        /// <returns></returns>
        /// created by: NGUYETKTB 28/09/2023
        public virtual PagingModel GetByPaging(PagingParam pagingParam)
        {
            pagingParam.SortString = string.IsNullOrEmpty(pagingParam.SortString) ? " ORDER BY created_date ASC " : " ORDER BY " + pagingParam.SortString;
            string procName = DatabaseUtility.GetProcdureName<T>(MSProcdureName.GetByPaging);
            PagingModel pagingModel = new PagingModel();
            string conditionQuery = BuildPagingCondition<T>(pagingParam);
            var parameters = new DynamicParameters();
            parameters.Add("pageSize", pagingParam.PageSize);
            parameters.Add("pageNumber", (pagingParam.PageNumber - 1) * pagingParam.PageSize);
            parameters.Add("whereCondition", conditionQuery);
            parameters.Add("sortString", pagingParam.SortString);

            var connection = GetConnection();
            var result = connection.QueryMultiple(procName, parameters, commandType: CommandType.StoredProcedure);
            return HandlePagingModel((GridReader)result);
        }

        /// <summary>
        /// 
        /// </summary>cond
        /// <param name="fields"></param>
        /// <returns></returns>
        public IEnumerable<T> GetListByFields(Dictionary<string, object> fields)
        {
            string tableName = AttributeUtility.GetTableName<T>(typeof(TableNameAttribute));
            string query = "";
            query = "SELECT * FROM " + tableName + " WHERE ";
            // Duyệt qua các phần tử trong fields
            foreach (KeyValuePair<string, object> entry in (Dictionary<string, object>)fields)
            {
                string key = entry.Key;
                object value = entry.Value;

                query += key + " = '" + value + "'";
                query += " AND ";
            }
            if (query.Trim().EndsWith("AND"))
            {
                // Nếu có, loại bỏ "AND" từ cuối chuỗi
                query = query.Substring(0, query.Length - 4);
            }
            var connection = GetConnection();
            var list = connection.Query<T>(query);
            return list;
        }

        /// <summary>
        /// Kiểm tra trường bảng này có reference bảng khác hay không
        /// </summary>
        /// <param name="referenceType">Type reference tới</param>
        /// <param name="propertyInfo">Thông tin property là foreign key</param>
        /// <returns></returns>
        public bool CheckReferenceEntity<T>(Type referenceType, PropertyInfo propertyInfo, object entity)
        {
            {
                string query = BuildReferenceQuery(referenceType, propertyInfo, entity);
                var connection = GetConnection();
                var list = connection.Query(query);
                return list.Count() > 0;
            }
        }
        #endregion 

        #region INSERT
        /// <summary>
        /// Thực hiện thêm mới
        /// </summary>
        /// <param name="entity">dữ liệu thêm mới</param>
        /// <returns></returns>
        /// created by: NGUYETKTB 28/09/2023
        public int Insert(T entity)
        {
            string query = GetInsertQuery(entity.GetType(), entity);
            var connection = GetConnection();
            DynamicParameters dynamicParams = DatabaseUtility.GetParametersFromQuery(query, connection, entity);
            try
            {
                var rowEffects = connection.Execute(query, dynamicParams);
                return rowEffects;
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region UPDATE

        /// <summary>
        /// Thực hiện cập nhật thông tin bản ghi
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int Update(Guid entityId, T entity)
        {
            string query = GetUpdateQuery(entity.GetType(), entity, entityId);
            var connection = GetConnection();
            DynamicParameters dynamicParameters = DatabaseUtility.GetParametersFromQuery(query, connection, entity);
            try
            {
                var rowEffects = connection.Execute(query, dynamicParameters);
                return rowEffects;
            }
            catch (Exception ex) { }
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Thực hiện cập nhật trạng thái của danh sách bản ghi
        /// </summary>
        /// <param name="ids">id của các bản ghi</param>
        /// <param name="status">trạng thái cần cập nhật</param>
        /// <returns></returns>
        /// created by: NGUYETKTB 28/09/2023
        public int UpdateStatus(string[] ids, int status)
        {
            string tableName = AttributeUtility.GetTableName<T>(typeof(TableNameAttribute));
            string primaryKey = AttributeUtility.GetPrimaryKeyName<T>();
            string query = $"UPDATE {tableName} SET status = @stt WHERE FIND_IN_SET ({primaryKey} , @ids) ";
            var parameters = new DynamicParameters();
            parameters.Add("stt", status);
            parameters.Add("ids", string.Join(",", ids));
            // mở transaction
            var transaction = _unitOfWork.GetTransaction();
            var connection = GetConnection();
            try
            {
                var rowEffects = connection.Execute(query, parameters, transaction: transaction);
                _unitOfWork.Commit();
                return rowEffects;
            }
            catch
            {
                _unitOfWork.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Xóa nhiều bản ghi
        /// kbnguyet 24.10.2023
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public int DeleteMultiple(string[] ids)
        {
            string tableName = AttributeUtility.GetTableName<T>(typeof(TableNameAttribute));
            string primaryKey = AttributeUtility.GetPrimaryKeyName<T>();
            string query = $" DELETE FROM {tableName} WHERE FIND_IN_SET ({primaryKey} , @ids);";
            var parameters = new DynamicParameters();
            parameters.Add("@ids", string.Join(",", ids));
            var transaction = _unitOfWork.GetTransaction();
            var connection = GetConnection();
            try
            {
                var rowEffects = connection.Execute(query, parameters, transaction: transaction);
                _unitOfWork.Commit();
                return rowEffects;
            }
            catch
            {
                _unitOfWork.Rollback();
                throw;
            }
        }
        #endregion

        #region VIRTUAL METHOD

        /// <summary>
        /// Thực hiện build câu lệnh thêm mới dữ liệu
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        /// <exception cref="IMSException"></exception>
        protected virtual string GetInsertQuery(Type type, T entity)
        {
            if (type == null)
            {
                throw new IMSException("GetInsertQuery type is invalid");
            }
            // lấy ra tên bảng
            string tableName = AttributeUtility.GetTableName<T>(typeof(TableNameAttribute));
            var query = new StringBuilder($"INSERT INTO {tableName} (");
            var values = new StringBuilder("VALUES (");
            // lấy ra các properties không có not mapped attribute
            var propertiesWithoutNotMapped = AttributeUtility.GetPropertiesWithoutAttribute(type, typeof(NotMappedAttribute));
            foreach (var property in propertiesWithoutNotMapped)
            {
                var nullableTypeName = Nullable.GetUnderlyingType(property.PropertyType)?.Name;
                var objTypeName = property.PropertyType.Name;
                object value = property.GetValue(entity); // Lấy giá trị của thuộc tính từ đối tượng của bạn
                // 1: là guid và giá trị guid.empty hoặc null
                if (value is Guid gValue && gValue == Guid.Empty)
                {
                    continue;
                }
                else if (value == null)
                {
                    continue; // Bỏ qua thuộc tính không phải kiểu Guid có giá trị null
                }
                // Thêm giá trị của thuộc tính vào phần VALUES, Query của câu lệnh SQL
                query.Append($"{property.Name}, ");
                values.Append($"@{property.Name}, ");
            }
            // Loại bỏ dấu phẩy cuối cùng trong câu lệnh SQL
            query.Length -= 2;
            values.Length -= 2;

            query.Append(") ");
            values.Append(")");

            query.Append(values.ToString());
            return query.ToString();
        }

        /// <summary>
        /// Thực hiện build câu lệnh update dữ liệu
        /// </summary>
        /// <param name="type"></param>
        /// <param name="entity"></param>
        /// <param name="entity_id"></param>
        /// <returns></returns>
        protected virtual string GetUpdateQuery(Type type, T entity, Guid entity_id)
        {
            //lấy ra primary key
            string primaryKey = AttributeUtility.GetPrimaryKeyName<T>();
            //lấy ra tên bảng
            string tableName = AttributeUtility.GetTableName<T>(typeof(TableNameAttribute));
            var query = new StringBuilder($"UPDATE {tableName} SET ");
            // lấy ra các trường không có attribute not map và primary key
            var fields = AttributeUtility.GetPropertiesWithoutAttribute(entity.GetType(), typeof(PrimaryKeyAttribute), typeof(NotMappedAttribute));
            if (fields.Count > 0)
            {
                query.Append(string.Join(",", fields.Select(x => $"{x.Name} = @{x.Name}")));
                query.Append($" WHERE {primaryKey} = @{primaryKey}");
            }
            return query.ToString();
        }


        /// <summary>
        /// Thực hiện build câu query kiểm tra reference trường dữ liệu nào đó
        /// </summary>
        /// <param name="referenceType"></param>
        /// <param name="propertyInfo"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        protected virtual string BuildReferenceQuery(Type referenceType, PropertyInfo propertyInfo, object entity)
        {
            if (referenceType == null || propertyInfo == null || entity == null)
            {
                return String.Empty;
            }
            // lấy ra tên bảng chính
            var primaryTableName = AttributeUtility.GetTableName<T>(typeof(TableNameAttribute));
            // lấy ra tên bảng reference
            var referenceTableName = referenceType.Name.ToLower();
            //lấy ra primary key của reference table
            PropertyInfo primaryKey = AttributeUtility.GetPropertyWithAttribute(referenceType, typeof(PrimaryKeyAttribute)).FirstOrDefault();

            StringBuilder query = new StringBuilder("");
            query.Append($"SELECT * FROM {referenceTableName} s WHERE s.{primaryKey.Name} = '{propertyInfo.GetValue(entity)}'");
            return query.ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pagingParam"></param>
        /// <returns></returns>
        protected virtual string BuildPagingCondition<T>(PagingParam pagingParam)
        {
            var filterQuery = "";
            if (pagingParam != null)
            {
                if (pagingParam.FilterConditions != null)
                {
                    foreach (var filter in pagingParam.FilterConditions)
                    {
                        filterQuery += ProcessConditionColumn<T>(filter);
                        filterQuery += " AND ";
                    }
                    // xử lí nếu thừa AND ở cuối

                    if (filterQuery.Trim().EndsWith("AND"))
                    {
                        filterQuery = filterQuery.Trim().Substring(0, filterQuery.Length - 4);
                    }
                }
            }
            return filterQuery;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        protected virtual string ProcessConditionColumn<T>(FilterCondition filter)
        {
            //lấy ra tên bảng
            string tableName = AttributeUtility.GetTableName<T>(typeof(TableNameAttribute));
            var condition = new StringBuilder("");
            if (!string.IsNullOrEmpty(filter.Field))
            {
                switch (filter.Condition)
                {
                    case (int)Condition.Equal:
                        condition.Append($"{tableName}.{filter.Field} = '{filter.Value}'");
                        break;
                    case (int)Condition.NotEqual:
                        condition.Append($"{tableName}.{filter.Field} != '{filter.Value}'");
                        break;
                    case (int)Condition.Like:
                        condition.Append($"{tableName}.{filter.Field} LIKE '%{filter.Value}%'");
                        break;
                    case (int)Condition.NotIn:
                        break;
                    case (int)Condition.IsNull:
                        condition.Append($"{tableName}.{filter.Field} IS NULL ");
                        break;
                    case (int)Condition.IsNotNull:
                        condition.Append($"{tableName}.{filter.Field} IS NOT NULL ");
                        break;
                }
            }
            return condition.ToString();
        }

        /// <summary>
        /// Hàm thực hiện xử lí dữ liệu paging trả về
        /// </summary>
        /// <param name="gridReader">grid reader</param>
        /// <returns>
        /// Thông tin phân trang theo entity
        /// </returns>
        /// Created By: NguyetKTB (15/05/2023)
        protected virtual PagingModel HandlePagingModel(GridReader gridReader)
        {
            return new PagingModel()
            {
                Data = gridReader.Read<TDto>(),
                TotalRecord = gridReader.Read<int>().FirstOrDefault(),
            };
        }
        #endregion

    }
}
