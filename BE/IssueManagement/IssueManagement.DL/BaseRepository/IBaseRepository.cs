﻿using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using IssueManagement.Common.Model;
using IssueManagement.Common.Pagination;
using System.Data.Common;
using System.Reflection;

namespace IssueManagement.DL.BaseRepository
{
    public interface IBaseRepository<T, TDto> where T : class
    {

        #region DB CONNECTION

        /// <summary>
        /// Get database connection
        /// </summary>
        /// <returns></returns>
        /// Created By: NguyetKTB 28/09/2023
        public DbConnection GetConnection();
        #endregion

        #region GET METHOD

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterConditions"></param>
        /// <returns></returns>
        IEnumerable<TDto> GetFilterData(List<FilterCondition>? filterConditions, string sortString);

        /// <summary>
        /// Lấy ra thông tin bản ghi theo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// created by: NGUYETKTB 28/09/2023
        public TDto GetEntityById(Guid id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fields"></param>
        /// <returns></returns>
        public IEnumerable<T> GetListByFields(Dictionary<string, object> fields);

        /// <summary>
        /// Lấy ra danh sách bản ghi theo paging và filter
        /// </summary>
        /// <param name="pagingParam"></param>
        /// <returns></returns>
        /// created by: NGUYETKTB 28/09/2023
        public PagingModel GetByPaging(PagingParam pagingParam);

        /// <summary>
        /// Kiểm tra trường bảng này có reference bảng khác hay không
        /// </summary>
        /// <param name="referenceType"></param>
        /// <param name="propertyInfo"></param>
        /// <returns></returns>
        public bool CheckReferenceEntity<T>(Type referenceType, PropertyInfo propertyInfo, object entity);
        #endregion

        #region UPDATE
        /// <summary>
        /// Thực hiện cập nhật thông tin bản ghi
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int Update(Guid entityId, T entity);

        /// <summary>
        /// Thực hiện cập nhật trạng thái của danh sách bản ghi
        /// </summary>
        /// <param name="ids">id của các bản ghi</param>
        /// <param name="status">trạng thái cần cập nhật</param>
        /// <returns></returns>
        /// created by: NGUYETKTB 28/09/2023
        public int UpdateStatus(string[] ids, int status);

        /// <summary>
        /// Xóa nhiều bản ghi
        /// kbnguyet 24.10.2023
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public int DeleteMultiple(string[] ids);
        #endregion

        #region INSERT
        /// <summary>
        /// Thực hiện thêm mới
        /// </summary>
        /// <param name="entity">dữ liệu thêm mới</param>
        /// <returns></returns>
        /// created by: NGUYETKTB 28/09/2023
        public int Insert(T entity);
        #endregion

        #region DELETE
        #endregion
    }
}
