﻿using Dapper;
using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using IssueManagement.Common.Enumeration;
using IssueManagement.Common.Model;
using IssueManagement.Common.Pagination;
using IssueManagement.Common.UnitOfWork;
using IssueManagement.Common.Utilities;
using IssueManagement.DL.BaseRepository;
using IssueManagement.DL.Database;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Dapper.SqlMapper;
using static IssueManagement.Common.Attributes.TableAttribute;

namespace IssueManagement.DL.ClassStudentRepository
{
    public class ClassStudentRepository : BaseRepository<ClassStudent, ClassStudentDto>, IClassStudentRepository
    {
        public ClassStudentRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public int InsertMultiple(List<ClassStudent> classStudents)
        {
            string procName = DatabaseUtility.GetProcdureName<ClassStudent>(MSProcdureName.InsertMultiple);
            var classStudentList = JsonConvert.SerializeObject(classStudents);
            var connection = GetConnection();
            var transaction = _unitOfWork.GetTransaction();
            try
            {
                var result = connection.Execute(procName, new { ClassStudents = classStudentList }, commandType: CommandType.StoredProcedure, transaction: transaction);
                _unitOfWork.Commit();
                return result;
            }
            catch
            {
                _unitOfWork.Rollback();
                throw;
            }
        }

        public IEnumerable<StudentExcelModel> GetStudentExcelModel()
        {
            string tableName = "class_student_excel";
            string query = "SELECT * FROM " + tableName;
            var connection = GetConnection();
            var list = connection.Query<StudentExcelModel>(query);
            return list;
        }

        public PagingModel GetStudents(PagingParam pagingParam)
        {
            string procName = "Proc_User_GetStudents";
            pagingParam.SortString = string.IsNullOrEmpty(pagingParam.SortString) ? " ORDER BY created_date ASC " : " ORDER BY " + pagingParam.SortString;
            PagingModel pagingModel = new PagingModel();
            string conditionQuery = base.BuildPagingCondition<User>(pagingParam);
            var parameters = new DynamicParameters();
            parameters.Add("pageSize", pagingParam.PageSize);
            parameters.Add("pageNumber", (pagingParam.PageNumber - 1) * pagingParam.PageSize);
            parameters.Add("whereCondition", conditionQuery);
            parameters.Add("sortString", pagingParam.SortString);

            var connection = GetConnection();
            var result = connection.QueryMultiple(procName, parameters, commandType: CommandType.StoredProcedure);
            return new PagingModel()
            {
                Data = result.Read<UserDto>(),
                TotalRecord = result.Read<int>().FirstOrDefault(),
            };
        }

        protected override string ProcessConditionColumn<ClassStudent>(FilterCondition filter)
        {
            string query = base.ProcessConditionColumn<ClassStudent>(filter);

            if (filter.Field.Equals("class_id") && filter.Condition == (int)Condition.NotIn)
            {
                query += $"user.user_id NOT IN ( SELECT class_student.student_id FROM class_student where class_student.class_id = '{filter.Value}')";

            }
            return query;

        }
    }
}
