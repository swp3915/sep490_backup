﻿using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using IssueManagement.Common.Model;
using IssueManagement.Common.Pagination;
using IssueManagement.DL.BaseRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.DL.ClassStudentRepository
{
    public interface IClassStudentRepository : IBaseRepository<ClassStudent, ClassStudentDto>
    {
        int InsertMultiple(List<ClassStudent> classStudents);

        IEnumerable<StudentExcelModel> GetStudentExcelModel();

        PagingModel GetStudents(PagingParam pagingParam);
    }
}
