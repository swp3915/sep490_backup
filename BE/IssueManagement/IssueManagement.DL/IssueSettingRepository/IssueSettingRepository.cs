﻿using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using IssueManagement.Common.UnitOfWork;
using IssueManagement.DL.BaseRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.DL.IssueSettingRepository
{
    public class IssueSettingRepository : BaseRepository<IssueSetting, IssueSettingDto>, IIssueSettingRepository
    {
        public IssueSettingRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
