﻿using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using IssueManagement.Common.UnitOfWork;
using IssueManagement.DL.BaseRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.DL.SubjectRepository
{
    public class SubjectRepository : BaseRepository<Subject, SubjectDto>, ISubjectRepository
    {
        public SubjectRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
