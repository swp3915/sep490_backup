﻿using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using IssueManagement.Common.Model;
using IssueManagement.Common.Pagination;
using IssueManagement.Common.UnitOfWork;
using IssueManagement.DL.BaseRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.DL.ClassRepository
{
    public class ClassRepository : BaseRepository<Class, ClassDto>, IClassRepository
    {
        public ClassRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
