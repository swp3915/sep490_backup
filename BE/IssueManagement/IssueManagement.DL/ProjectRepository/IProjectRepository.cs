﻿using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using IssueManagement.DL.BaseRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.DL.ProjectRepository
{
    public interface IProjectRepository : IBaseRepository<Project, ProjectDto>
    {
    }
}
