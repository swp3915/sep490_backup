﻿using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using IssueManagement.Common.Model;
using IssueManagement.DL.BaseRepository;

namespace IssueManagement.DL.UserRepository
{
    public interface IUserRepository : IBaseRepository<User, UserDto>
    {
    }
}
