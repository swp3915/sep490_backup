﻿using IssueManagement.Common.Entities;
using IssueManagement.DL.BaseRepository;
using IssueManagement.Common.UnitOfWork;
using Dapper;
using IssueManagement.Common.Model;
using System.Text.RegularExpressions;
using IssueManagement.Common.EntityDto;

namespace IssueManagement.DL.UserRepository
{
    public class UserRepository : BaseRepository<User, UserDto>, IUserRepository
    {
        public UserRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
