﻿using Dapper;
using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using IssueManagement.Common.UnitOfWork;
using IssueManagement.Common.Utilities;
using IssueManagement.DL.BaseRepository;
using IssueManagement.DL.UserRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using static Dapper.SqlMapper;
using static IssueManagement.Common.Attributes.TableAttribute;

namespace IssueManagement.DL.SettingRepository
{
    public class SettingRepository : BaseRepository<Setting, SettingDto>, ISettingRepository
    {
        public SettingRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

    }
}
