﻿using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using IssueManagement.Common.UnitOfWork;
using IssueManagement.DL.BaseRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.DL.MilestoneRepository
{
    public class MilestoneRepository : BaseRepository<Milestone, MilestoneDto>, IMilestoneRepository
    {
        public MilestoneRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
