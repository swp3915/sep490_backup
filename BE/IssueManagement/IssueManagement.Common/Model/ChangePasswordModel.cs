﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.Common.Model
{
    /// <summary>
    /// Đây là class mô tả form đổi mật khẩu
    /// </summary>
    public class ChangePasswordModel
    {
        public string Email { get; set; }
        public string? OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
