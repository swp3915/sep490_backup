﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.Common.Model
{
    public class PagingModel
    {
        #region PROPERTY
        public int? TotalRecord { get; set; }

        public object Data { get; set; }

        public object? Summary { get; set; }
        #endregion
    }
}
