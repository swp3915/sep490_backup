﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.Common.Model
{
    /// <summary>
    /// Đây là class mô tả form login
    /// </summary>
    public class LoginModel
    {
        public string LoginInput { get; set; }
        public string PasswordInput { get; set; }
    }
}
