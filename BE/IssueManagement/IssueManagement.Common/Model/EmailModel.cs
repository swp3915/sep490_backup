﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.Common.Model
{
    public class EmailModel
    {
        /// <summary>
        /// người nhận
        /// </summary>
        public string To { get; set; }

        /// <summary>
        /// Tên người nhận
        /// </summary>
        public string ToName { get; set; }

        /// <summary>
        /// tiêu đề mail
        /// </summary>
        public string? Subject { get; set; }
        /// <summary>
        /// template hiển thị
        /// </summary>
        public string? Template { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string? Content { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string? Url { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? Action { get; set; }
    }
}
