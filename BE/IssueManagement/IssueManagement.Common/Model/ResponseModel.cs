﻿namespace IssueManagement.Common.Model
{
    /// <summary>
    /// Lớp bao gồm các thông tin sẽ được trả về cho người dùng
    /// </summary> 
    /// Created By: NguyetKTB (15/05/2023)
    public class ResponseModel
    {

        #region properties

        /// <summary>
        /// Thông báo cho dev
        /// </summary> 
        /// Created By: NguyetKTB (15/05/2023)
        public string Message { get; set; }

        /// <summary>
        /// Dữ liệu muốn trả về
        /// </summary> 
        /// Created By: NguyetKTB (15/05/2023)
        public object? Data { get; set; }

        /// <summary>
        /// Thông báo lỗi muốn trả về (nếu có)
        /// </summary> 
        /// Created By: NguyetKTB (15/05/2023)
        public object? Error { get; set; }

        #endregion

        #region CONSTRUCTOR
        /// <summary>
        /// Hàm khởi tạo
        /// </summary>
        public ResponseModel()
        {
        }
        public ResponseModel( string userMsg, object? data, object? error = null)
        {
            Message = userMsg;
            Data = data;
            Error = error;
        }
        #endregion

    }
}
