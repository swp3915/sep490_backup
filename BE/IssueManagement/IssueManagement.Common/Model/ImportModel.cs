﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.Common.Model
{
    public class ImportModel
    {

        public FormFile FormFile { get; set; }

        public Guid? Id { get; set; }
    }
}
