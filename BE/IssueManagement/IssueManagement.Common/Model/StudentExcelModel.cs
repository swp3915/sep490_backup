﻿using IssueManagement.Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.Common.Model
{
    public class StudentExcelModel : History
    {
        public Guid class_student_excel_id { get; set; } 

        public string field_name { get; set; }

        public string field_key { get; set; }

        public int field_type { get; set; }
    }
}
