﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.Common.Attributes
{
    public class TableAttribute : Attribute
    {
        /// <summary>
        /// Attribute TableName
        /// </summary>
        /// Created by: NguyetKTB (28/09/2023)
        [AttributeUsage(AttributeTargets.Class)]
        public class TableNameAttribute : Attribute
        {
            public string TableName { get; set; }

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="tableName">Tên bảng</param>
            /// Created by:NguyetKTB (28/09/2023)
            public TableNameAttribute(string tableName)
            {
                TableName = tableName;
            }
        }

        /// <summary>
        /// Primary key attribute
        /// </summary>
        /// Created by: NguyetKTB (28/09/2023)
        [AttributeUsage(AttributeTargets.Property)]
        public class PrimaryKeyAttribute : Attribute
        {
        }

        /// <summary>
        /// Attribute Unique
        /// </summary>
        /// Created by: NguyetKTB (23/05/2023)
        [AttributeUsage(AttributeTargets.Property)]
        public class UniqueAttribute : Attribute
        {

            public string ErrorMessage { get; set; }

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="errorMessage">Thông báo lỗi</param>
            /// Created by: 
            public UniqueAttribute(string errorMessage)
            {
                ErrorMessage = errorMessage;
            }
          
        }
        /// <summary>
        /// Attribute Unique
        /// </summary>
        /// Created by: HieuBQ (09/10/2023)
        [AttributeUsage(AttributeTargets.Property)]
        public class ReferenceAttribute : Attribute
        {
            public Type[] Types { get; set; } 

            public ReferenceAttribute(Type[] types)
            {
                Types = types;
            }

        }

        /// <summary>
        /// Not map attribute: trường đánh dấu các property không map trong database
        /// </summary>
        [AttributeUsage(AttributeTargets.Property)]
        public class NotMapAttribute : Attribute
        {
            public NotMapAttribute()
            {
            }

        }

    }
}
