﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.Common.Exception
{
    public class IMSException : IOException
    {

        public string UserMessage { get; set; }

        public IDictionary? Errors { get; set; }

        public object Result { get; set; }

        public IMSException(string errorMsg, IDictionary? errorData = null, object? result = null)
        {
            UserMessage = errorMsg;
            Errors = errorData;
            Result = result;
        }
        public IMSException() { }

        public override string? Message
        {
            get
            {
                return UserMessage;
            }
        }

        public override IDictionary? Data
        {
            get
            {
                return Errors;
            }
        }

    }
}
