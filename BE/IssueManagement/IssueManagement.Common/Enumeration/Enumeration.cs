﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.Common.Enumeration
{
    /// <summary>
    /// Liệt kê các hành động được thực hiện
    /// </summary>
    /// Created By: NguyetKTB (28/09/2023)
    public enum Action
    {
        Insert = 1,
        Update = 2,
        Delete = 3,
        Duplicate = 4,
        Import = 5,
        Export = 6,
    }

    /// <summary>
    /// Liệt kê các status code trả về
    /// </summary>
    /// Created By: NguyetKTB (28/09/2023)
    public enum StatusCode
    {
        /// <summary>
        /// Thành công
        /// </summary>
        Success = 200,

        /// <summary>
        /// Thêm thành công
        /// </summary>
        Created = 201,

        /// <summary>
        /// Không có dữ liệu trả về
        /// </summary>
        NoContent = 204,

        /// <summary>
        /// Lỗi validate
        /// </summary>
        BadRequest = 400,

        /// <summary>
        /// lỗi do exception
        /// </summary>
        Error = 500
    }

    /// <summary>
    /// Liệt kê các condition khi thực hiện lấy dữ liệu
    /// </summary>
    /// Created By: NguyetKTB (28/09/2023)
    public enum Condition
    {
        Equal = 1,
        NotEqual = 2,
        Like = 3,
        NotLike = 4,
        In = 5,
        NotIn = 6,
        IsNull = 7,
        IsNotNull = 8,
        StartsWith = 9,
        EndsWith = 10,
    }

    public enum ModelState
    {
        None = 0,
        Insert = 1,
        Update = 2,
        Delete = 3,
        Duplicate = 4,
    }

    /// <summary>
    /// Liệt kê các tên procedure
    /// </summary>
    /// Created By: NguyetKTB 15.10.2023
    public enum MSProcdureName
    {
        GetAll,
        /// <summary>
        /// Lấy dữ liệu theo id
        /// </summary>
        GetById,
        /// <summary>
        /// Update dữ liệu
        /// </summary>
        Update,
        /// <summary>
        /// Xóa dữ liệu
        /// </summary>
        DeleteById,
        /// <summary>
        /// Lấy dữ liệu thông qua paging và filter
        /// </summary>
        GetByPaging,
        /// <summary>
        /// Thêm mới dữ liệu
        /// </summary>
        Insert,
        /// <summary>
        /// 
        /// </summary>
        UpdateMultipleStatus,
        /// <summary>
        /// Thêm mới danh sách dữ liệu
        /// </summary>
        InsertMultiple,
        /// <summary>
        /// 
        /// </summary>
        GetFilterData,
    }

    public enum FieldType
    {
        Int = 1,
        String = 2,
        Decimal = 3,
        Guid = 4,
        DateTime = 5,
    }
    /// <summary>
    /// Liệt kê các loại hành động add user
    /// </summary>
    public enum AddUserAction
    {
        Add = 1,
        AdminAdd =2
    }

}
