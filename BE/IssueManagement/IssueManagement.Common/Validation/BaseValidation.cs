﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IssueManagement.Common.Validation
{
    public class BaseValidation : IBaseValidation
    {

        /// <summary>
        /// Kiểm tra định dạng số điện thoại
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        /// Created by: KBNGUYET 30/09/2023
        public Boolean PhoneValidation(string phone)
        {
            string regexPattern = @"^\d{11}$"; // bao gồm số là có 11 kí tự
            return Regex.IsMatch(phone, regexPattern);
        }

        /// <summary>
        /// Kiểm tra định dạng email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public Boolean EmailValidation(string email)
        {
            bool isValid = true;
            return isValid;
        }
    }
}
