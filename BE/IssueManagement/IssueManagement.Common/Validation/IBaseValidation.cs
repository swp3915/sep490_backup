﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.Common.Validation
{
    public interface IBaseValidation
    {
        public Boolean PhoneValidation(string phone);

        public Boolean EmailValidation(string email);
    }
}
