﻿using IssueManagement.Common.Model;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using MimeKit.Text;

namespace IssueManagement.Common.Utilities
{
    public class EmailUtility
    {
        public readonly IOptions<MailSettingsModel> _mailSettings;
        public EmailUtility(IOptions<MailSettingsModel> mailSettings)
        {
            _mailSettings = mailSettings;
        }

        /// <summary>
        /// Gửi email
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public bool SendMail(EmailModel request)
        {
            // điền thông tin của mail
            using (var message = new MailMessage())
            {
                message.From = new MailAddress(_mailSettings.Value.MailAddress);
                message.To.Add(new MailAddress(request.To));
                message.Subject = request.Subject;
                string htmlBody = System.IO.File.ReadAllText(request.Template);
                htmlBody = htmlBody.Replace("{{content}}", request.Content);
                htmlBody = htmlBody.Replace("{{url}}", request.Url);
                message.Body = htmlBody;
                message.IsBodyHtml = true;
                //lấy thông tin client bằng config
                using (var client = new SmtpClient())
                {
                    client.Host = _mailSettings.Value.SmtpServer;
                    client.Port = int.Parse(_mailSettings.Value.Port);
                    client.Credentials = new NetworkCredential(_mailSettings.Value.UserName, _mailSettings.Value.Password);
                    client.EnableSsl = true;
                    client.Send(message);
                }
            }
            return true;
        }
    }
}
