﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.Common.Utilities
{
    public class CommonUtility
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void AddValueToDictionary(Dictionary<string, List<string>> dictionary, string key, object value)
        {
            // kiểm tra key đã tồn tại trong dictionary chưa
            if (dictionary.ContainsKey(key))
            {
                // nếu rồi thì add thêm value vào value của key đó
                // Nếu giá trị value là chuỗi, ta lấy List tương ứng với key
                List<string> existingList = dictionary[key];
                // và thêm giá trị vào List đó
                existingList.Add(value as string);
            }
            else
            {
                // nếu chưa thì thêm mới key - value vào dictionary
                List<string> newList = new List<string>();

                // Kiểm tra nếu giá trị value là một chuỗi thì thêm vào List
                if (value is string)
                {
                    newList.Add(value as string);
                }

                // Thêm key và List vào dictionary
                dictionary.Add(key, newList);

            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="charSet"></param>
        /// <param name="random"></param>
        /// <returns></returns>
        public string GetRandomChar(string charSet, Random random)
        {
            return charSet[random.Next(charSet.Length)].ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GeneratePassword()
        {
            const string LowercaseChars = "abcdefghijklmnopqrstuvwxyz";
            const string UppercaseChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            const string NumericChars = "0123456789";
            const string SpecialChars = "!@#$%^&*()_+";
            Random random = new Random();

            // Chọn ít nhất một ký tự từ mỗi loại
            string password =
                GetRandomChar(LowercaseChars, random) +
                GetRandomChar(UppercaseChars, random) +
                GetRandomChar(NumericChars, random) +
                GetRandomChar(SpecialChars, random);

            // Tạo các ký tự ngẫu nhiên còn lại
            for (int i = 4; i < 12; i++)
            {
                string charSet = LowercaseChars + UppercaseChars + NumericChars + SpecialChars;
                password += GetRandomChar(charSet, random);
            }
            password = new string(password.ToCharArray().OrderBy(c => random.Next()).ToArray());

            return password;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="length"></param>
        /// <param name="isContainDigit"></param>
        /// <param name="isContainText"></param>
        /// <returns></returns>
        public static string RandomVerifyCode(int length, bool isContainDigit = false, bool isContainText = false)
        {
            // Create a Random object to generate random numbers.
            Random random = new Random();

            // Generate 6 random digits and concatenate them into a string.
            string randomString = "";
            if (isContainDigit)
            {
                for (int i = 0; i < length; i++)
                {
                    int randomDigit = random.Next(10); // Generates a random number between 0 and 9.
                    randomString += randomDigit;
                }
            }
            return randomString;
        }
    }
}
