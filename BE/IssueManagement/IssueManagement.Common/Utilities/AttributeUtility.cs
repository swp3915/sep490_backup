﻿using IssueManagement.Common.Exception;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static IssueManagement.Common.Attributes.TableAttribute;

namespace IssueManagement.Common.Utilities
{
    public class AttributeUtility
    {

        /// <summary>
        /// Lấy ra tên của bảng
        /// </summary>
        /// <typeparam name="T">Entity truyền vào</typeparam>
        /// <returns>Tên bảng</returns>
        /// CreatedBy: NguyetKTB 29/09/2023
        public static string GetTableName<T>(Type attributeType)
        {
            // attributeType của class
            var tableName = typeof(T).GetCustomAttributes(attributeType, true);
            if (tableName.Length > 0)
            {
                return ((TableNameAttribute)tableName[0]).TableName;
            }
            else
            {
                throw new IMSException(string.Format("Not found this attribute.", typeof(T).Name));
            }
        }

        /// <summary>
        /// Lấy property của entity là khóa chính
        /// </summary>
        /// <typeparam name="T">Entity truyền vào</typeparam>
        /// <returns>Property là khóa chính</returns>
        /// CreatedBy: NguyetKTB 29/09/2023
        public static string GetPrimaryKeyName<T>()
        {
            var result = GetPropertiesByAttribute<T>(typeof(PrimaryKeyAttribute));
            if (result.Count > 0)
            {
                return result[0].ToString();
            }
            else
            {
                throw new IMSException(string.Format("Not found this attribute.", typeof(T).Name));
            }
        }

        /// <summary>
        /// Lấy ra các property có attribute truyền vào
        /// </summary>
        /// <typeparam name="T">Entity truyền vào</typeparam>
        /// <param name="attributeType">Kiểu attribute cần lấy</param>
        /// <returns>Danh sách các property có attribute truyền vào</returns>
        /// CreatedBy: NguyetKTB 29/09/2023
        public static List<string> GetPropertiesByAttribute<T>(Type attributeType)
        {
            // lấy ra properties của T
            var properties = typeof(T).GetProperties();
            List<string> propertyNames = new List<string>();
            // duyệt danh sách properties
            foreach (var property in properties)
            {

                var attribute = (Attribute?)Attribute.GetCustomAttribute(property, attributeType);
                if (attribute != null)
                {
                    propertyNames.Add(property.Name);
                }
            }
            return propertyNames;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="attributeType"></param>
        /// <returns></returns>
        public static List<PropertyInfo> GetPropertyWithAttribute(Type entityType, Type attributeType)
        {
            List<PropertyInfo> propertyList = new List<PropertyInfo>();
            // Duyệt qua tất cả các thuộc tính của kiểu entityType
            foreach (PropertyInfo propertyInfo in entityType.GetProperties())
            {
                // Kiểm tra xem thuộc tính có gắn attributeType không
                if (Attribute.IsDefined(propertyInfo, attributeType))
                {
                    // Nếu có, trả về thuộc tính này
                    propertyList.Add(propertyInfo);
                }
            }
            // Nếu không tìm thấy, trả về null hoặc thực hiện hành động mặc định khác
            return propertyList;
        }

        /// <summary>
        /// Lấy ra danh sách properties với attribute truyền vào
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="attributeTypes"></param>
        /// <returns></returns>
        public static List<PropertyInfo> GetPropertiesWithAttributes(Type entityType, params Type[] attributeTypes)
        {
            List<PropertyInfo> propertiesWithAttributes = entityType.GetProperties()
                .Where(p => attributeTypes.All(a => p.GetCustomAttribute(a) != null))
                .ToList();

            return propertiesWithAttributes;
        }

        /// <summary>
        /// Lấy ra danh sách các properties loại trừ danh sách attribute nào đó
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="attributeTypes"></param>
        /// <returns></returns>
        public static List<PropertyInfo> GetPropertiesWithoutAttribute(Type entityType, params Type[] attributeTypes)
        {
            List<PropertyInfo> propertiesWithoutAttribute = entityType.GetProperties()
       .Where(property => attributeTypes.All(attrType => !property.IsDefined(attrType, false)))
       .ToList();
            return propertiesWithoutAttribute;
        }
    }
}
