﻿using IssueManagement.Common.Entities;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.Common.Utilities
{
    public class JwtUtility
    {
        public readonly IConfiguration _configuration;
        JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
        public JwtUtility(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// Tạo token khi đăng nhập
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public string CreateToken(User user, string role)
        {
            //Tạo token chứa thông tin cơ bản của User
            List<Claim> claims = new List<Claim>
            {
                new Claim("user_id", user.user_id.ToString()),
                new Claim("fullname", user.fullname),
                new Claim("role", role),
                new Claim("email", user.email)
            };
            var key = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(_configuration.GetSection("AppSettings:Token").Value));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature);
            var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: creds);
            var jwt = new JwtSecurityTokenHandler().WriteToken(token);
            return jwt;
        }

        /// <summary>
        /// kiểm tra token đã hết hạn hay chưa
        /// </summary>
        /// <param name="jwtToken"></param>
        /// <returns></returns>
        public bool IsTokenExpired(string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var jwtToken = tokenHandler.ReadToken(token) as JwtSecurityToken;

                if (jwtToken == null)
                {
                    // JWT không hợp lệ
                    return true;
                }

                var expiration = jwtToken.ValidTo;

                if (expiration < DateTime.UtcNow)
                {
                    // JWT đã hết hạn
                    return true;
                }

                // JWT chưa hết hạn
                return false;
            }
            catch (SecurityTokenExpiredException)
            {
                // JWT đã hết hạn
                return true;
            }
            catch (IOException)
            {
                // Lỗi trong quá trình kiểm tra
                return true;
            }
        }

        /// <summary>
        /// Tạo token cho việc reset password
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public string GenerateResetPasswordToken(User user)
        {
            List<Claim> claims = new List<Claim>
        {
            new Claim("email", user.email)
        };

            var key = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(_configuration.GetSection("AppSettings:Token").Value));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature);

            var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddMinutes(30), // Token hết hạn sau 30 phút
                signingCredentials: creds
            );

            var jwt = new JwtSecurityTokenHandler().WriteToken(token);
            return jwt;
        }

        /// <summary>
        /// Decode token để lấy dữ liệu
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public ClaimsPrincipal DecodeToken(string token)
        {
            tokenHandler.InboundClaimTypeMap.Clear();
            TokenValidationParameters validationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(_configuration.GetSection("AppSettings:Token").Value)), // Lấy secret key từ cấu hình
                ValidateIssuer = false,
                ValidateAudience = false,
                ClockSkew = TimeSpan.Zero
            };
            SecurityToken validatedToken;
            ClaimsPrincipal claimsPrincipal = tokenHandler.ValidateToken(token, validationParameters, out validatedToken);

            return claimsPrincipal;
        }
    }
}
