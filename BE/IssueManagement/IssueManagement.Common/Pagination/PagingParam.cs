﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.Common.Pagination
{
    public class PagingParam
    {
        #region Property
        public int PageNumber { get; set; }

        public int PageSize { get; set; }

        public string? SortString { get; set; }

        public List<FilterCondition>? FilterConditions { get; set; }
        #endregion
    }
}
