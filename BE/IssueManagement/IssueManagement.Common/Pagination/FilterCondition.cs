﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.Common.Pagination
{
    public class FilterCondition
    {

        #region PROPERTY

        public string? Field { get; set; }

        public object? Value { get; set; }

        public int? Condition { get; set; }
        #endregion
    }
}
