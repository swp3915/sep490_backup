﻿using IssueManagement.Common.Entities;
using IssueManagement.Common.Enumeration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.Common.EntityDto
{
    public class UserDto : User
    {
        public string setting_value { get; set; }
    }
}
