﻿using IssueManagement.Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.Common.EntityDto
{
    public class AssignmentDto : Assignment
    {
        public string subject_code { get; set; }

        public string subject_name { get; set; }
    }
}
