﻿using IssueManagement.Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.Common.EntityDto
{
    public class ClassStudentDto : ClassStudent
    {
        public string student_name { get; set; }
        public string student_email { get; set; }
        public string student_phone { get; set; }
        public string class_code { get; set; }
        public string class_name { get; set; }
        public string project_code { get; set; }
        public string project_name { get; set; }
    }
}
