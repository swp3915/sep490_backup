﻿using IssueManagement.Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.Common.EntityDto
{
    public class ClassDto : Class
    {
        public string subject_code { get; set; }

        public string subject_name { get; set; }

        public string semester_name { get; set; }

        public string teacher_name { get; set; }

    }
}
