﻿using IssueManagement.Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.Common.EntityDto
{
    public class ProjectDto : Project
    {
        public string leader_name { get; set; }

        public string class_code { get; set; }

        public string class_name { get; set; }

    }
}
