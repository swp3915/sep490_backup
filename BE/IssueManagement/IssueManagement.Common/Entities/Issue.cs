﻿using static IssueManagement.Common.Attributes.TableAttribute;

namespace IssueManagement.Common.Entities
{
    [TableName("issue")]
    public class Issue : History
    {
        [PrimaryKey]
        public Guid issue_id { get; set; } = Guid.NewGuid();

        public Guid? issue_type { get; set; }

        public string? issue_type_value { get; set; }
        public Guid? issue_status { get; set; }

        public string? issue_status_value { get; set; }
        public Guid? work_process { get; set; }

        public string? work_process_value { get; set; }

        public DateTime? due_date { get; set; }
        public Guid assignee { get; set; }

        public string assignee_name { get; set; }
        public Guid project_id { get; set; }

        public string project_code { get; set; }

        public string project_name { get; set; }
        public Guid milestone_id { get; set; }

        public string milestone_name { get; set; }

        public string? description { get; set; }
    }
}
