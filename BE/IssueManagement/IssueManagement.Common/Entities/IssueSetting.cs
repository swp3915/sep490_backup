﻿using System.Text.Json.Nodes;
using static IssueManagement.Common.Attributes.TableAttribute;

namespace IssueManagement.Common.Entities
{
    [TableName("issue_setting")]
    public class IssueSetting : History
    {
        [PrimaryKey]
        public Guid issue_setting_id { get; set; } = Guid.NewGuid();
        public string issue_value { get; set; }
        public string issue_group { get; set; }
        public int status { get; set; }
        public string? style { get; set; } = "{}";

        [Reference(new Type[]
        {
            typeof(Subject) // môn học
        })]
        public Guid? subject_id { get; set; } = Guid.Empty;


        [Reference(new Type[]
        {
            typeof(Class) // môn học
        })]
        public Guid? class_id { get; set; } = Guid.Empty;


        [Reference(new Type[]
        {
            typeof(Project) // môn học
        })]
        public Guid? project_id { get; set; } = Guid.Empty;

        public string? description { get; set; }
    }
}
