﻿using static IssueManagement.Common.Attributes.TableAttribute;

namespace IssueManagement.Common.Entities
{
    [TableName("setting")]
    public class Setting : History
    {

        #region Properties
        [PrimaryKey]
        public Guid setting_id { get; set; }

        public string setting_value { get; set; }

        public int status { get; set; }

        public int data_group { get; set; }

        public int display_order { get; set; }

        public string? description { get; set; }

        #endregion
    }
}
