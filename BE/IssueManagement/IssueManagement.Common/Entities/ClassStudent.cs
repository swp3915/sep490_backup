﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static IssueManagement.Common.Attributes.TableAttribute;

namespace IssueManagement.Common.Entities
{
    [TableName("class_student")]
    public class ClassStudent : History
    {
        [PrimaryKey]
        public Guid class_student_id { get; set; } = Guid.NewGuid();

        [Reference(new Type[]{
            typeof(User) // môn học
        })]
        [Required]
        public Guid student_id { get; set; }

        [Reference(new Type[]{
            typeof(Class) // môn học
        })]
        [Required]
        public Guid class_id { get; set; }


        [Reference(new Type[]{
            typeof(Project) // môn học
        })]
        public Guid? project_id { get; set; } = Guid.Empty;
    }
}
