﻿using IssueManagement.Common.Attributes;
using IssueManagement.Common.Enumeration;
using IssueManagement.Common.Resources;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static IssueManagement.Common.Attributes.TableAttribute;

namespace IssueManagement.Common.Entities
{
    [TableName("user")]
    public class User : History
    {
        #region Properties

        [PrimaryKey]
        public Guid user_id { get; set; } = Guid.NewGuid();

        public string fullname { get; set; }


        [StringLength(100, MinimumLength = 6, ErrorMessage = "Password must have a minimum length of 6 characters.")]
        public string? password { get; set; }



        [Unique("Email already exists.")]
        [EmailAddress(ErrorMessage = "Invalid email format.")]
        public string email { get; set; }


        [RegularExpression(@"^[0-9]+$", ErrorMessage = "Phone Number is not valid.")]
        public string? phone_number { get; set; }

        public Guid setting_id { get; set; }

        public int status { get; set; }

        public string? note { get; set; }
        [NotMapped]
        public AddUserAction action { get; set; }


        #endregion
    }
}
