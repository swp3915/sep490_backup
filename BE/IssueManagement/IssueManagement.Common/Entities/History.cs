﻿namespace IssueManagement.Common.Entities
{
    /// <summary>
    /// Lớp chứa thông tin về ngày tạo và ngày chỉnh sửa
    /// </summary>
    /// created by: NguyetKTB (27/05/2023)
    public class History
    {
        #region Properties

        /// <summary>
        /// ngày tạo
        /// </summary>
        /// created by: NguyetKTB (27/05/2023)
        public DateTime? created_date { get; set; } = new DateTime();

        /// <summary>
        /// người tạo
        /// </summary>
        /// created by: NguyetKTB (27/05/2023)
        public string? created_by { get; set; }

        /// <summary>
        /// ngày sửa
        /// </summary>
        /// created by: NguyetKTB (27/05/2023)
        public DateTime? modified_date { get; set; } = new DateTime();

        /// <summary>
        /// người sửa
        /// </summary>
        /// created by: NguyetKTB (27/05/2023)
        public string? modified_by { get; set; }
        #endregion
    }
}
