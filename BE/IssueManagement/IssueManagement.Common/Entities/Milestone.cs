﻿using static IssueManagement.Common.Attributes.TableAttribute;

namespace IssueManagement.Common.Entities
{
    [TableName("milestone")]
    public class Milestone : History
    {
        [PrimaryKey]
        public Guid milestone_id { get; set; } = Guid.NewGuid();

        public string milestone_name { get; set; }

        public DateTime from_date { get; set; }

        public DateTime to_date { get; set; }

        public int status { get; set; }
        [Reference(new Type[]
{
            typeof(Class)
})]
        public Guid class_id { get; set; }
        [Reference(new Type[]
{
            typeof(Project)
})]

        public Guid project_id { get; set; }

        public string? description { get; set; }
    }
}
