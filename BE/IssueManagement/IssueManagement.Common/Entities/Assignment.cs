﻿using static IssueManagement.Common.Attributes.TableAttribute;

namespace IssueManagement.Common.Entities
{
    [TableName("assignment")]
    public class Assignment : History 
    {
        [PrimaryKey]
        public Guid assignment_id { get; set; }

        public string assignment_name { get; set; }

        public int status { get; set; }
        [Reference(new Type[]
{
            typeof(Subject) // môn học
})]
        public Guid subject_id { get; set; }
        public string? description { get; set; }

    }
}
