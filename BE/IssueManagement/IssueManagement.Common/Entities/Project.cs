﻿using static IssueManagement.Common.Attributes.TableAttribute;

namespace IssueManagement.Common.Entities
{
    [TableName("project")]
    public class Project : History
    {
        [PrimaryKey]
        public Guid project_id { get; set; }

        public string project_code { get; set; }

        public string project_name { get; set; }

        public int status { get; set; }
        public Guid leader_id { get; set; }
        [Reference(new Type[]
{
            typeof(Class)
})]
        public Guid class_id { get; set; }

        public string? description { get; set; }

    }
}
