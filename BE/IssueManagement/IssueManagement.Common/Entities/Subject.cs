﻿using static IssueManagement.Common.Attributes.TableAttribute;

namespace IssueManagement.Common.Entities
{
    [TableName("subject")]
    public class Subject : History
    {
        [PrimaryKey]
        public Guid subject_id { get; set; }

        public string subject_code { get; set; }

        public string subject_name { get; set; }

        public int status { get; set; }

        public string? description { get; set; }

        public Guid assignee_id { get; set; }

    }
}
