﻿using static IssueManagement.Common.Attributes.TableAttribute;

namespace IssueManagement.Common.Entities
{
    [TableName("class")]
    public class Class : History
    {
        [PrimaryKey]
        public Guid class_id { get; set; }

        public string class_code { get; set; }

        public string class_name { get; set; }

        public string? description { get; set; }

        public int status { get; set; }

        [Reference(new Type[]
        {
            typeof(Subject) // môn học
        })]
        public Guid subject_id { get; set; }

        [Reference(new Type[]
        {
            typeof(Setting) // môn học
        })]
        public Guid semester_id { get; set; }

        [Reference(new Type[]
       {
            typeof(User) // giáo viên
       })]
        public Guid teacher_id { get; set; }

    }
}
