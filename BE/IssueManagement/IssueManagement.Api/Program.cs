using IssueManagement.BL.BaseService;
using IssueManagement.BL.UserService;
using IssueManagement.DL.BaseRepository;
using IssueManagement.DL.UserRepository;
using IssueManagement.Common.UnitOfWork;
using IssueManagement.DL.Database;
using IssueManagement.Common.Validation;
using IssueManagement.DL.SettingRepository;
using IssueManagement.BL.SettingService;
using IssueManagement.Common.Utilities;
using IssueManagement.Api.Middleware;
using Microsoft.Extensions.Configuration;
using Microsoft.OpenApi.Models;
using IssueManagement.Common.Model;
using MailKit;
using IssueManagement.BL.ClassService;
using IssueManagement.BL.ProjectService;
using IssueManagement.BL.SubjectService;
using IssueManagement.DL.ClassRepository;
using IssueManagement.DL.ProjectRepository;
using IssueManagement.DL.SubjectRepository;
using Microsoft.Extensions.DependencyInjection;
using IssueManagement.BL.SubjectService;
using IssueManagement.BL.ClassStudentService;
using IssueManagement.DL.ClassStudentRepository;
using IssueManagement.BL.MilestoneService;
using IssueManagement.DL.MilestoneRepository;
using IssueManagement.BL.IssueSettingService;
using IssueManagement.DL.IssueSettingRepository;
using IssueManagement.BL.AssignmentService;
using IssueManagement.DL.AssignmentRepository;

var builder = WebApplication.CreateBuilder(args);

// Services cors
var MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddCors(options =>
{
    options.AddPolicy(MyAllowSpecificOrigins, policy =>
    {
        policy.AllowAnyOrigin()
        .AllowAnyHeader()
        .AllowAnyMethod();
    });
});
builder.Services.AddScoped(typeof(IBaseService<,>), typeof(BaseService<,>)); // DI BaseService v�o IBaseService th�ng qua constructor
builder.Services.AddScoped(typeof(IBaseRepository<,>), typeof(BaseRepository<,>)); // DI BaseRepository v�o IBaseRepository th�ng qua constructor

builder.Services.AddScoped<IUserService, UserService>();
builder.Services.AddScoped<IUserRepository, UserRepository>();

builder.Services.AddScoped<ISettingService, SettingService>();
builder.Services.AddScoped<ISettingRepository, SettingRepository>();

builder.Services.AddScoped<ISubjectService, SubjectService>();
builder.Services.AddScoped<ISubjectRepository, SubjectRepository>();

builder.Services.AddScoped<IProjectService, ProjectService>();
builder.Services.AddScoped<IProjectRepository, ProjectRepository>();

builder.Services.AddScoped<IClassService, ClassService>();
builder.Services.AddScoped<IClassRepository, ClassRepository>();

builder.Services.AddScoped<IClassStudentService, ClassStudentService>();
builder.Services.AddScoped<IClassStudentRepository, ClassStudentRepository>();

builder.Services.AddScoped<IMilestoneService, MilestoneService>();
builder.Services.AddScoped<IMilestoneRepository,  MilestoneRepository>();

builder.Services.AddScoped<IIssueSettingService, IssueSettingService>();
builder.Services.AddScoped<IIssueSettingRepository, IssueSettingRepository>();

builder.Services.AddScoped<IAssignmentService, AssignmentService>();
builder.Services.AddScoped<IAssignmentRepository, AssignmentRepository>();

builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
builder.Services.AddSingleton<CommonUtility>();
builder.Services.AddScoped<IBaseValidation, BaseValidation>();
builder.Services.AddSingleton<JwtUtility>();
builder.Services.AddSingleton<EmailUtility>();

builder.Services.Configure<MailSettingsModel>(builder.Configuration.GetSection("MailSettings"));

builder.Services.AddAuthentication()
    .AddGoogle(googleOptions =>
    {
        IConfigurationSection googleAuthNSection = builder.Configuration.GetSection("Authentication:Google");
        googleOptions.ClientId = googleAuthNSection["ClientId"];
        googleOptions.ClientSecret = googleAuthNSection["ClientSecet"];
    });

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


var app = builder.Build();
DatabaseContext.ConnectionString = builder.Configuration.GetConnectionString("MySqlConnectionString");

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(options =>
    {
        options.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
    });
}
app.UseMiddleware(typeof(ErrorMiddleware));
app.UseHttpsRedirection();
app.UseCors(MyAllowSpecificOrigins);

app.UseAuthorization();

app.MapControllers();

app.Run();
