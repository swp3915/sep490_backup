﻿using IssueManagement.Common.Enumeration;
using IssueManagement.Common.Exception;
using IssueManagement.Common.Model;
using Newtonsoft.Json;

namespace IssueManagement.Api.Middleware
{
    public class ErrorMiddleware
    {
        #region PROPERTIES
        /// <summary>
        /// Luồng request.
        /// </summary>
        private readonly RequestDelegate _next;
        #endregion

        #region CONSTRUCTOR
        // <summary>
        /// Hàm khởi tạo
        /// </summary>
        /// <param name="next">Luồng request</param>
        public ErrorMiddleware(RequestDelegate next)
        {
            _next = next;
        }
        #endregion

        #region METHODS
        /// <summary>
        /// Hàm gọi của middleware
        /// </summary>
        /// <param name="context">Đối tượng HttpContext</param>
        /// Created By: NguyetKTB (10/07/2023)
        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        /// <summary>
        /// Hàm xử lý khi chương trình có lỗi xảy ra
        /// </summary>
        /// <param name="context">Đối tượng HttpContext</param>
        /// <param name="ex">Lỗi</param>
        /// <returns></returns>
        /// Created By: NguyetKTB (10/07/2023)
        private async Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            ResponseModel responseModel = new ResponseModel();
            switch (ex)
            {
                case IMSException:
                    IMSException msEx = (IMSException)ex;
                    responseModel.Message = msEx.UserMessage;
                    responseModel.Error = msEx.Errors;
                    responseModel.Data = msEx.Result;
                    context.Response.StatusCode = (int)StatusCode.BadRequest;
                    break;
                default:
                    responseModel.Message = ex.Message;
                    context.Response.StatusCode = (int)StatusCode.Error;
                    break;
            }
            var jsonResponse = JsonConvert.SerializeObject(responseModel);
            context.Response.ContentType = "application/json"; // Trả về kiểu dữ liệu json
            await context.Response.WriteAsync(jsonResponse); // Trả về kết quả
        }
        #endregion
    }
}
