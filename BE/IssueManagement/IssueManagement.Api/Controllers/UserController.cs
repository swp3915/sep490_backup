﻿using IssueManagement.BL.UserService;
using IssueManagement.Common.Entities;
using IssueManagement.Common.Resources;
using IssueManagement.Common.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using IssueManagement.Common.Utilities;
using IssueManagement.DL.UserRepository;
using Microsoft.Extensions.Options;
using System.Net.Mail;
using System.Net;
using IssueManagement.Common.EntityDto;

namespace IssueManagement.Api.Controllers
{
    public class UserController : BaseController<User, UserDto>
    {
        private readonly IUserService _userService;
        private readonly JwtUtility _jwtUtility;
        public UserController(IUserService userService, JwtUtility jwtUtility) : base(userService)
        {
            _userService = userService;
            _jwtUtility = jwtUtility;
        }

        /// <summary>
        /// Thực hiện đăng kí tài khoản
        /// </summary>
        /// <param name="fullname"></param>
        /// <param name="email"></param>
        /// <param name="phone"></param>
        /// <param name="password"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        [HttpPost("register")]
        public IActionResult Register(User user)
        {
            var rs = _userService.Register(user);
            if (!rs.IsNullOrEmpty())
            {
                return Ok(rs);
            }
            return BadRequest(IMSResource.Msg_Exception);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("login")]
        public IActionResult Login(LoginModel model)
        {
            var rs = _userService.Authenticate(model);
            if (rs == null)
            {
                return BadRequest("Username or password is incorrect.");
            }
            return Ok(rs);
        }
        /// <summary>
        /// Thực hiện đổi mật khẩu
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("change-password")]
        public IActionResult ChangePassword([FromBody] ChangePasswordModel model)
        {
            bool rs = _userService.ChangePassword(model.Email, model.OldPassword, model.NewPassword);
            if (rs)
            {
                return Ok("Password changed successfully!");
            }
            return BadRequest();
        }
        /// <summary>
        /// Thực hiện gửi link đến trang đổi mật khẩu
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpPost("send-mail")]
        public IActionResult SendMail(EmailModel model)
        {
            bool rs = _userService.SendMail(model);
            if (rs)
            {
                return Ok("Mail sent successfully! Please check your email.");
            }
            return BadRequest();

        }
        /// <summary>
        /// Thực hiện đổi mật khẩu sau nhấn vào link trong email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpPost("reset-password")]
        public IActionResult ResetPassword(string token, string newPassword)
        {
            bool rs = _userService.ResetPassword(token, newPassword);
            if (rs)
            {
                return Ok("Password changed successfully.");
            }
            return BadRequest();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpPost("google-signin")]
        public IActionResult GoogleLogin(string email, string name)
        {
            var rs = _userService.AuthenticateGoogle(email, name);
            if (rs == null)
            {
                return BadRequest();
            }
            return Ok(rs);
        }

    }
}
