﻿using IssueManagement.BL.BaseService;
using IssueManagement.BL.SettingService;
using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using Microsoft.AspNetCore.Mvc;

namespace IssueManagement.Api.Controllers
{
    public class SettingController : BaseController<Setting, SettingDto>
    {
        public SettingController(ISettingService settingService) : base(settingService)
        {
        }
    }
}
