﻿using IssueManagement.BL.BaseService;
using IssueManagement.BL.ClassService;
using IssueManagement.BL.SettingService;
using IssueManagement.BL.SubjectService;
using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using Microsoft.AspNetCore.Mvc;

namespace IssueManagement.Api.Controllers
{
    public class ClassController : BaseController<Class, ClassDto>
    {
        private readonly IClassService _classService;
        public ClassController(IClassService classService) : base(classService)
        {
            _classService = classService;
        }
    }
}
