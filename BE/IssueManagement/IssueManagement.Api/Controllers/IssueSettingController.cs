﻿using IssueManagement.BL.BaseService;
using IssueManagement.BL.IssueSettingService;
using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using Microsoft.AspNetCore.Mvc;

namespace IssueManagement.Api.Controllers
{
    public class IssueSettingController : BaseController<IssueSetting, IssueSettingDto>
    {
        public IssueSettingController(IIssueSettingService issueSettingService) : base(issueSettingService)
        {
        }

    }
}
