﻿using IssueManagement.BL.BaseService;
using IssueManagement.BL.ClassStudentService;
using IssueManagement.BL.UserService;
using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using IssueManagement.Common.Model;
using IssueManagement.Common.Pagination;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace IssueManagement.Api.Controllers
{
    public class ClassStudentController : BaseController<ClassStudent, ClassStudentDto>
    {
        private readonly IClassStudentService _classStudentService;
        public ClassStudentController(IClassStudentService classStudentService) : base(classStudentService)
        {
            _classStudentService = classStudentService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="classStudents"></param>
        /// <returns></returns>
        [HttpPost("InsertMultiple")]
        public IActionResult InsertMultiple(List<ClassStudent> classStudents)
        {
            if (classStudents == null) { return BadRequest(); }
            bool rs = _classStudentService.InsertMultiple(classStudents);
            return Ok(rs);
        }

        /// <summary>
        /// Export file excel chứa thông tin student
        /// </summary>
        /// <param name="filterConditions"></param>
        /// <returns></returns>
        [HttpPost("Export")]
        public IActionResult ExportExcel(List<FilterCondition> filterConditions)
        {
            var stream = _classStudentService.ExportExcel(filterConditions);
            string excelName = "Danhsachsinhvien.xlsx";
            Response.Headers.Add("Content-Disposition", "attachment; filename=" + excelName);
            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        }

        /// <summary>
        /// Import thông tin student từ 1 lớp
        /// </summary>
        /// <param name="formFile"></param>
        /// <returns></returns>
        [HttpPost("Import/{class_id}")]
        public IActionResult ImportExcel(Guid class_id, [FromForm] IFormFile formFile)
        {
            if (formFile == null) { return BadRequest(); };
            var fileName = Path.GetFileName(formFile.FileName);
            // nếu file không phải định dạng .xlsx thì trả về lỗi
            if (!Path.GetExtension(formFile.FileName).Equals(".xlsx", StringComparison.OrdinalIgnoreCase))
            {
                return BadRequest();
            }
            object data = _classStudentService.ImportExcel(formFile, (Guid)class_id);
            return Ok(data);
        }


        /// <summary>
        /// Lấy ra danh sách students không thuộc class nào
        /// </summary>
        /// <param name="pagingParam"></param>
        /// <returns></returns>
        [HttpPost("GetStudents")]
        public IActionResult GetStudents(PagingParam pagingParam)
        {
            PagingModel pagingModel = _classStudentService.GetStudents(pagingParam);
            return Ok(pagingModel);
        }
    }
}
