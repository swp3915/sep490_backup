﻿using IssueManagement.BL.ProjectService;
using IssueManagement.BL.SubjectService;
using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using Microsoft.AspNetCore.Mvc;

namespace IssueManagement.Api.Controllers
{
    public class SubjectController : BaseController<Subject, SubjectDto>
    {
        public SubjectController(ISubjectService subjectService) : base(subjectService)
        {
        }
    }
}
