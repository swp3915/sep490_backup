﻿using IssueManagement.BL.BaseService;
using IssueManagement.Common.Enumeration;
using IssueManagement.Common.Entities;
using Microsoft.AspNetCore.Mvc;
using IssueManagement.Common.Model;
using IssueManagement.Common.Resources;
using IssueManagement.Common.Pagination;

namespace IssueManagement.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class BaseController<T, TDto> : ControllerBase where T : class
    {

        #region FIELDS
        private readonly IBaseService<T, TDto> _baseService;
        #endregion

        #region CONSTRUCTOR
        public BaseController(IBaseService<T, TDto> baseService)
        {
            _baseService = baseService;
        }
        #endregion

        #region GET

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterConditions"></param>
        /// <param name="sortString"></param>
        /// <returns></returns>
        [HttpPost("GetFilterData")]
        public IActionResult GetByFilter(List<FilterCondition> filterConditions, string sortString)
        {
            IEnumerable<TDto> list = _baseService.GetByFilter(filterConditions, sortString);
            return Ok(list);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pagingParam"></param>
        /// <returns></returns>
        [HttpPost("GetByPaging")]
        public IActionResult GetByPaging(PagingParam pagingParam)
        {
            PagingModel pagingModel = _baseService.GetByPaging(pagingParam);
            return Ok(pagingModel);
        }

        /// <summary>
        /// Lấy ra dữ liệu bản ghi theo id truyền vào
        /// </summary>
        /// <param name="entity_id"></param>
        /// <returns></returns>
        /// Created by: KBNGUYET 01/10/2023
        [HttpGet("{entity_id}")]
        public IActionResult GetById(Guid entity_id)
        {
            TDto entity = _baseService.GetEntityById(entity_id);
            if (entity != null)
            {
                return Ok(entity);
            }
            return BadRequest(IMSResource.Msg_Empty_Data);
        }
        #endregion

        #region POST


        /// <summary>
        /// Thực hiện thêm mới dữ liệu
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        /// Created by: KBNGUYET 01/10/2023
        [HttpPost]
        public IActionResult Insert(T entity)
        {
            bool rs = _baseService.Insert(entity);
            if (!rs)
            {
                return BadRequest(IMSResource.Msg_Exception);
            }
            return Ok(IMSResource.Msg_Insert_Success);
        }

        /// <summary>
        /// Thực hiện cập nhật thông tin dữ liệu
        /// </summary>
        /// <param name="entity_id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        /// Created by: KBNGUYET 01/10/2023
        [HttpPut("{entity_id}")]
        public IActionResult Put([FromRoute] Guid entity_id, [FromBody] T entity)
        {
            bool rs = _baseService.Update(entity_id, entity);
            if (!rs)
            {
                return BadRequest(IMSResource.Msg_Exception);
            }
            return Ok(IMSResource.Msg_Update_Success);
        }


        /// <summary>
        /// Thực hiện cập nhật status của bản ghi
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        /// Created by: KBNGUYET 01/10/2023
        [HttpPost("UpdateStatus")]
        public IActionResult UpdateStatus(string[] ids, int status)
        {
            var rs = _baseService.UpdateStatus(ids, status);
            if (rs == ids.Length)
            {
                return Ok(IMSResource.Msg_Update_Success);
            }
            return BadRequest(IMSResource.Msg_Exception);
        }


        /// <summary>
        /// Xóa nhiều bản ghi theo id bản ghi truyền vào
        /// kbnguyet 27.10.2023
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpDelete("DeleteMultiple")]
        public IActionResult DeleteMultiple(string[] ids)
        {
            var rs = _baseService.DeleteMultiple(ids);
            if (rs == ids.Length)
            {
                return Ok("Delete records successfully!");
            }
            return BadRequest("Delete records failed!");
        }
        #endregion

    }
}
