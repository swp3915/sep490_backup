﻿using IssueManagement.BL.BaseService;
using IssueManagement.BL.MilestoneService;
using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using Microsoft.AspNetCore.Mvc;

namespace IssueManagement.Api.Controllers
{
    public class MilestoneController : BaseController<Milestone, MilestoneDto>
    {
        public MilestoneController(IMilestoneService milestoneService) : base(milestoneService)
        {
        }

    }
}
