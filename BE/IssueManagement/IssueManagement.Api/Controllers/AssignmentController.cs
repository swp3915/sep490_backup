﻿using IssueManagement.BL.AssignmentService;
using IssueManagement.BL.BaseService;
using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using Microsoft.AspNetCore.Mvc;

namespace IssueManagement.Api.Controllers
{
    public class AssignmentController : BaseController<Assignment, AssignmentDto>
    {
        private readonly IAssignmentService _assignmentService;
        public AssignmentController(IAssignmentService assignmentService) : base(assignmentService)
        {
            _assignmentService = assignmentService;
        }
    }
}
