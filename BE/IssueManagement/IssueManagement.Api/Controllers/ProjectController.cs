﻿using IssueManagement.BL.BaseService;
using IssueManagement.BL.ProjectService;
using IssueManagement.BL.SettingService;
using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using Microsoft.AspNetCore.Mvc;

namespace IssueManagement.Api.Controllers
{
    public class ProjectController : BaseController<Project, ProjectDto>
    {
        public ProjectController(IProjectService projectService) : base(projectService)
        {
        }
    }
}
