﻿using IssueManagement.BL.BaseService;
using IssueManagement.Common.Entities;
using IssueManagement.DL.BaseRepository;
using IssueManagement.Common.UnitOfWork;
using IssueManagement.Common.Validation;
using System.Net.Mail;
using IssueManagement.BL.SettingService;
using System.Numerics;
using IssueManagement.Common.Utilities;
using System.Linq.Expressions;
using IssueManagement.Common.Resources;
using IssueManagement.Common.Exception;
using IssueManagement.DL.UserRepository;
using IssueManagement.Common.Model;
using System.Text.RegularExpressions;
using MimeKit;
using Org.BouncyCastle.Asn1.Pkcs;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.Extensions.Options;
using System.Net;
using System.Text;
using IssueManagement.Common.EntityDto;
using IssueManagement.DL.SettingRepository;
using static System.Net.WebRequestMethods;
using static Dapper.SqlMapper;
using System;

namespace IssueManagement.BL.UserService
{
    public class UserService : BaseService<User, UserDto>, IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IBaseValidation _baseValidation;
        private readonly ISettingService _settingService;
        private readonly CommonUtility _commonUtility;
        private readonly JwtUtility _jwtUtility;
        private readonly IConfiguration _configuration;
        private readonly EmailUtility _emailUtility;
        private readonly ISettingRepository _settingRepository;
        public UserService(IUserRepository userRepository, IUnitOfWork unitOfWork, IBaseValidation baseValidation, ISettingService settingService, CommonUtility commonUtility, JwtUtility jwtUtility, IConfiguration configuration, EmailUtility emailUtility, ISettingRepository settingRepository) : base(userRepository, unitOfWork, commonUtility)
        {
            _userRepository = userRepository;
            _baseValidation = baseValidation;
            _settingService = settingService;
            _commonUtility = commonUtility;
            _jwtUtility = jwtUtility;
            _configuration = configuration;
            _emailUtility = emailUtility;
            _settingRepository = settingRepository;
        }

        /// <summary>
        /// Thực hiện xác thực tài khoản
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <exception cref="IMSException"></exception>
        /// Created by: HieuBQ 03/10/2023
        public string Authenticate(LoginModel request)
        {
            ValidateLogin(request);
            if (errors.Count > 0)
            {
                throw new IMSException(IMSResource.Msg_Exception, (System.Collections.IDictionary?)errors, null);
            }
            Dictionary<string, object> properties = new Dictionary<string, object>();
            properties.Add("email", request.LoginInput);
            IEnumerable<User> users = _userRepository.GetListByFields(properties);
            if (users.Count() == 0)
            {
                return null;
            }
            User user = users.First();
            Dictionary<string, object> settingProperties = new Dictionary<string, object>();
            settingProperties.Add("setting_id", user.setting_id);
            IEnumerable<Setting> settingValues = _settingRepository.GetListByFields(settingProperties);
            Setting setting = settingValues.First();
            if (BCrypt.Net.BCrypt.Verify(request.PasswordInput, user.password) != true)
                return null;
            string token = _jwtUtility.CreateToken(users.First(), setting.setting_value);
            return token;

        }
        #region OVERRIDE METHOD

        /// <summary>
        /// Thực hiện kiểm tra dữ liệu để đăng kí
        /// </summary>
        /// <param name="entity"></param>
        /// Created By: NguyetKTB (30/09/2023)
        protected override void ValidateCustom(User entity, int action = -1)
        {
            // email đúng định dạng trong bảng setting
            string[] parts = entity.email.Split('@');
            if (parts.Length != 2)
            {
                _commonUtility.AddValueToDictionary((Dictionary<string, List<string>>)errors, "email", "Invalid email format.");
            }
            else
            {
                // kiểm tra hậu tố đó có đúng với system setting đang được định dạng không
                string suffix = parts[1];
                // khai báo danh sách để query
                Dictionary<string, object> properties = new Dictionary<string, object>();
                properties.Add("setting_value", suffix);
                properties.Add("data_group", 3);
                if (_settingService.GetListByFields(properties).Count() == 0)
                {
                    _commonUtility.AddValueToDictionary((Dictionary<string, List<string>>)errors, "email", "Invalid email format.");
                }
            }

            // setting_id (hay còn gọi là role) phải tồn tại và data_group phải là 1 (ROLE = 1)
            // khai báo danh sách để query
            Dictionary<string, object> roleProperties = new Dictionary<string, object>();
            roleProperties.Add("setting_id", entity.setting_id);
            roleProperties.Add("data_group", 1);
            if (_settingService.GetListByFields(roleProperties).Count() == 0)
            {
                _commonUtility.AddValueToDictionary((Dictionary<string, List<string>>)errors, "role", string.Format(IMSResource.Msg_NotExist, "Role"));
            }
        }


        protected override void HandleEntityBeforeInsert(User user)
        {
            base.HandleEntityBeforeInsert(user);
            // nếu validate true thì thực hiện băm password
            switch (user.action)
            {
                case Common.Enumeration.AddUserAction.Add:
                    string userPassword = BCrypt.Net.BCrypt.HashPassword(user.password);
                    if (!string.IsNullOrEmpty(userPassword))
                    {
                        user.password = userPassword;
                    }
                    break;
                case Common.Enumeration.AddUserAction.AdminAdd:
                    string randomPassword = _commonUtility.GeneratePassword();
                    EmailModel emailModelAdd = new EmailModel
                    {
                        To = user.email,
                        ToName = user.fullname,
                        Subject = "[IMS] Registered sucessfully",
                        Template = "../IssueManagement.Common/EmailTemplate/RegisterGoogleTemplate.html",
                        Url = "http://localhost:5173/sign-in",
                        Action = 1,
                        Content = randomPassword,
                    };
                    _emailUtility.SendMail(emailModelAdd);
                    if (!string.IsNullOrEmpty(randomPassword))
                    {
                        user.password = BCrypt.Net.BCrypt.HashPassword(randomPassword);
                    }
                    break;
            }
        }


        protected override void HandleEntityBeforeUpdate(User user)
        {
            // nếu validate true thì thực hiện băm password
            string userPassword = BCrypt.Net.BCrypt.HashPassword(user.password);
            if (!string.IsNullOrEmpty(userPassword))
            {
                user.password = userPassword;
            }
        }

        /// <summary>
        /// Thực hiện validate đăng nhập
        /// </summary>
        /// <param name="model"></param>
        /// /// Created by: HieuBQ 03/10/2023
        protected void ValidateLogin(LoginModel model)
        {
            string loginString = model.LoginInput;
            string passwordString = model.PasswordInput;

            string[] parts = loginString.Split('@');
            if (parts.Length != 2)
            {
                _commonUtility.AddValueToDictionary((Dictionary<string, List<string>>)errors, "email", "Email is not valid.");
            }
            else
            {
                // kiểm tra hậu tố đó có đúng với system setting đang được định dạng không
                string suffix = parts[1];
                // khai báo danh sách để query
                Dictionary<string, object> properties = new Dictionary<string, object>();
                properties.Add("setting_value", suffix);
                properties.Add("data_group", 3);
                IEnumerable<Setting> settings = _settingService.GetListByFields(properties);
                if (settings.Count() == 0)
                {
                    _commonUtility.AddValueToDictionary((Dictionary<string, List<string>>)errors, "email", string.Format(IMSResource.Msg_Invalid, "Email"));
                }
            }

        }

        /// <summary>
        /// Thực hiện đổi mật khẩu
        /// </summary>
        /// <param name="email"></param>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        /// <exception cref="IMSException"></exception>
        /// Created by: HieuBQ 03/10/2023

        public bool ChangePassword(string email, string oldPassword, string newPassword)
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            properties.Add("email", email);
            IEnumerable<User> users = _userRepository.GetListByFields(properties);
            if (users.Count() == 0)
            {
                _commonUtility.AddValueToDictionary((Dictionary<string, List<string>>)errors, "user", "User does not exist!");
                return false;
            }
            if (oldPassword.Length < 8 || newPassword.Length < 8)
            {
                _commonUtility.AddValueToDictionary((Dictionary<string, List<string>>)errors, "password", string.Format(IMSResource.Msg_Invalid_Length, "password", "8 characters"));
            }
            if (BCrypt.Net.BCrypt.Verify(oldPassword, users.First().password) != true)
            {
                _commonUtility.AddValueToDictionary((Dictionary<string, List<string>>)errors, "password", "Old Password is incorrect!");
            }
            if (errors.Count > 0)
            {
                throw new IMSException(IMSResource.Msg_Exception, (System.Collections.IDictionary?)errors, null);
            }
            User user = users.First();
            user.password = newPassword;
            return Update(user.user_id, user);
        }
        /// <summary>
        /// Thực hiện đổi mật khẩu
        /// </summary>
        /// <param name="email"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        /// Created by: HieuBQ 07/10/2023

        public bool SendMail(EmailModel model)
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            properties.Add("email", model.To);
            bool isValid = false;
            switch (model.Action)
            {
                case 1:
                    IEnumerable<User> users = _userRepository.GetListByFields(properties);
                    if (users.Count() == 0)
                    {
                        _commonUtility.AddValueToDictionary((Dictionary<string, List<string>>)errors, "user", "User does not exist!");
                        return false;
                    }
                    User user = users.First();
                    String token = _jwtUtility.GenerateResetPasswordToken(user);
                    EmailModel emailModel = new EmailModel
                    {
                        To = user.email,
                        Subject = "[IMS] Reset Password",
                        Template = "../IssueManagement.Common/EmailTemplate/ResetPasswordTemplate.html",
                        Content = user.fullname,
                        Url = "http://localhost:5173/forgot-password?token=" + token,
                    };
                    isValid = _emailUtility.SendMail(emailModel);
                    break;
            }
            return isValid;
        }
        /// <summary>
        /// Thực hiện đổi mật khẩu
        /// </summary>
        /// <param name="token"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        /// <exception cref="IMSException"></exception>

        public bool ResetPassword(string token, string newPassword)
        {
            if (_jwtUtility.IsTokenExpired(token))
            {
                return false;
            }
            ClaimsPrincipal claimsPrincipal = _jwtUtility.DecodeToken(token);
            Claim emailClaim = claimsPrincipal.FindFirst("email");
            if (emailClaim != null)
            {
                string email = emailClaim.Value;
                Dictionary<string, object> properties = new Dictionary<string, object>();
                properties.Add("email", email);
                IEnumerable<User> users = _userRepository.GetListByFields(properties);
                if (users.Count() == 0)
                {
                    _commonUtility.AddValueToDictionary((Dictionary<string, List<string>>)errors, "user", "User does not exist!");
                    return false;
                }
                if (newPassword.Length < 8)
                {
                    _commonUtility.AddValueToDictionary((Dictionary<string, List<string>>)errors, "password", string.Format(IMSResource.Msg_Invalid_Length, "password", "8 characters"));
                }
                if (errors.Count > 0)
                {
                    throw new IMSException(IMSResource.Msg_Exception, (System.Collections.IDictionary?)errors, null);
                }
                User user = users.First();
                user.password = newPassword;
                return Update(user.user_id, user);
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <param name="name"></param>
        /// <returns></returns>

        public string AuthenticateGoogle(string email, string name)
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            properties.Add("email", email);
            IEnumerable<User> users = _userRepository.GetListByFields(properties);
            if (users.Count() == 0)
            {
                User newUser = new User();
                newUser.user_id = new Guid();
                newUser.email = email;
                newUser.fullname = name;
                Dictionary<string, object> settingProperties = new Dictionary<string, object>();
                settingProperties.Add("setting_value", "Student");
                Setting role = _settingRepository.GetListByFields(settingProperties).First();
                newUser.setting_id = role.setting_id;
                string password = _commonUtility.GeneratePassword();
                newUser.password = password;
                Insert(newUser);
                EmailModel emailModel = new EmailModel();
                emailModel.To = email;
                emailModel.Subject = "[IMS] Registered sucessfully";
                emailModel.Template = "../IssueManagement.Common/EmailTemplate/RegisterGoogleTemplate.html";
                emailModel.Content = password;
                emailModel.Url = "http://localhost:5173/sign-in";
                emailModel.Action = 1;
                _emailUtility.SendMail(emailModel);
                string token = _jwtUtility.CreateToken(newUser, "Student");
                return token;
            }
            else
            {
                Dictionary<string, object> settingProperties = new Dictionary<string, object>();
                settingProperties.Add("setting_id", users.First().setting_id);
                IEnumerable<Setting> settingValues = _settingRepository.GetListByFields(settingProperties);
                Setting setting = settingValues.First();
                string token = _jwtUtility.CreateToken(users.First(), setting.setting_value);
                return token;
            }
            return null;
        }

        /// <summary>
        /// Thực hiện xử lí yêu cầu đăng ký tài khoản của người dùng
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public string Register(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            base.ValidateCommon(user, (int)Common.Enumeration.Action.Insert);
            // kiểm tra lỗi
            if (errors.Count > 0)
            {
                throw new IMSException(IMSResource.Msg_Exception, (System.Collections.IDictionary?)errors, null);
            }
            // thực hiện random chuỗi bất kì gồm 6 số
            string verifyCode = CommonUtility.RandomVerifyCode(6, true, false);
            // thực hiện gửi mail
            EmailModel emailModel = new EmailModel
            {
                To = user.email,
                ToName = "to IMS",
                Subject = "[IMS] Verify Your Account",
                Template = "../IssueManagement.Common/EmailTemplate/VerifyCodeTemplate.html",
                Content = verifyCode,
                Action = 2
            };
            if (_emailUtility.SendMail(emailModel))
            {
                return verifyCode;
            }
            return String.Empty;
        }


        #endregion
    }
}
