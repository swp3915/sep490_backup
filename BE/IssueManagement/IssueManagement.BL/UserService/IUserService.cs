﻿using IssueManagement.BL.BaseService;
using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using IssueManagement.Common.Model;

namespace IssueManagement.BL.UserService
{
    public interface IUserService : IBaseService<User, UserDto>
    {
        string Authenticate(LoginModel request);
        string AuthenticateGoogle(string email, string name);
        bool ChangePassword(string email, string oldPassword, string newPassword);
        bool SendMail(EmailModel model);
        bool ResetPassword(string token, string newPassword);
        string Register(User user);

    }
}
