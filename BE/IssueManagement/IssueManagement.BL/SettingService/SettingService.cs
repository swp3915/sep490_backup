﻿using IssueManagement.BL.BaseService;
using IssueManagement.BL.UserService;
using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using IssueManagement.Common.UnitOfWork;
using IssueManagement.Common.Utilities;
using IssueManagement.DL.BaseRepository;
using IssueManagement.DL.SettingRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.BL.SettingService
{
    public class SettingService : BaseService<Setting, SettingDto>, ISettingService
    {
        private ISettingRepository _settingRepository;
        public SettingService(ISettingRepository settingRepositoryy, IUnitOfWork unitOfWork, CommonUtility commonUtility) : base(settingRepositoryy, unitOfWork, commonUtility)
        {
            _settingRepository = settingRepositoryy;
        }


        public IEnumerable<Setting> GetListByFields(Dictionary<string, object> fields)
        {
            // mở transaction
            try
            {
                IEnumerable<Setting> list = _settingRepository.GetListByFields(fields);
                return list;
            }
            catch
            {
                throw;
            }
        }
    }
}
