﻿using IssueManagement.BL.BaseService;
using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using IssueManagement.Common.UnitOfWork;
using IssueManagement.Common.Utilities;
using IssueManagement.DL.BaseRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.BL.AssignmentService
{
    public class AssignmentService : BaseService<Assignment, AssignmentDto>, IAssignmentService
    {
        private readonly CommonUtility _commonUtility;
        public AssignmentService(IBaseRepository<Assignment, AssignmentDto> baseRepository, IUnitOfWork unitOfWork, CommonUtility commonUtility) : base(baseRepository, unitOfWork, commonUtility)
        {
            _commonUtility = commonUtility;
        }
    }
}
