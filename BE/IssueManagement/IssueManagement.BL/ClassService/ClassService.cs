﻿using IssueManagement.BL.BaseService;
using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using IssueManagement.Common.Resources;
using IssueManagement.Common.UnitOfWork;
using IssueManagement.Common.Utilities;
using IssueManagement.DL.BaseRepository;
using IssueManagement.DL.ProjectRepository;
using IssueManagement.DL.SettingRepository;
using IssueManagement.DL.SubjectRepository;
using IssueManagement.DL.UserRepository;
using Org.BouncyCastle.Asn1.Cms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static IssueManagement.Common.Attributes.TableAttribute;

namespace IssueManagement.BL.ClassService
{
    public class ClassService : BaseService<Class, ClassDto>, IClassService
    {
        private readonly CommonUtility _commonUtility;
        private readonly ISubjectRepository _subjectRepository;
        private readonly IUserRepository _userRepository;
        private readonly ISettingRepository _settingRepository;
        public ClassService(IBaseRepository<Class, ClassDto> baseRepository, IUnitOfWork unitOfWork, CommonUtility commonUtility, ISubjectRepository subjectRepository, IUserRepository userRepository, ISettingRepository settingRepository) : base(baseRepository, unitOfWork, commonUtility)
        {
            _commonUtility = commonUtility;
            _subjectRepository = subjectRepository;
            _userRepository = userRepository;
            _settingRepository = settingRepository;
        }
        protected override void ValidateCustom(Class entity, int action = -1)
        {
            //Kiểm tra setting có tồn tại hay không và setting phải có data_group = 2 (setting ở class là semester)
            Dictionary<string, object> settingProperties = new Dictionary<string, object>();
            settingProperties.Add("setting_id", entity.semester_id);
            // lấy ra thông tin setting ở db
            Setting setting = _settingRepository.GetListByFields(settingProperties).Count() > 0 ? _settingRepository.GetListByFields(settingProperties).First() : new Setting();
            // nếu data group = semester
            if (setting.data_group != 2)
            {
                _commonUtility.AddValueToDictionary((Dictionary<string, List<string>>)errors, "setting_id", string.Format(IMSResource.Msg_NotExist, "semester"));
            }
            //Kiểm tra teacher có tồn tại hay không và setting_value ="Teacher"
            Dictionary<string, object> userProperties = new Dictionary<string, object>();
            userProperties.Add("user_id", entity.teacher_id);
            User user = _userRepository.GetListByFields(userProperties).Count() > 0 ? _userRepository.GetListByFields(userProperties).First() : new User();
            // lấy ra thông tin setting id (setting ở đây là role với vai trò là teacher)
            Dictionary<string, object> roleProperties = new Dictionary<string, object>();
            roleProperties.Add("setting_id", user.setting_id);
            Setting role = _settingRepository.GetListByFields(roleProperties).Count() > 0 ? _settingRepository.GetListByFields(roleProperties).First() : new Setting();
            if (role.setting_value != "Teacher" || role.data_group != 1)
            {
                _commonUtility.AddValueToDictionary((Dictionary<string, List<string>>)errors, "teacher_id", string.Format(IMSResource.Msg_NotExist, "teacher"));
            }
        }


    }
}
