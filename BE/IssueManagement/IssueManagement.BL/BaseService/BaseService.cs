﻿using IssueManagement.DL.BaseRepository;
using IssueManagement.Common.UnitOfWork;
using IssueManagement.Common.Utilities;
using System.Reflection;
using IssueManagement.Common.Exception;
using IssueManagement.Common.Resources;
using IssueManagement.Common.Attributes;
using static IssueManagement.Common.Attributes.TableAttribute;
using IssueManagement.Common.Enumeration;
using IssueManagement.Common.Entities;
using System.ComponentModel.DataAnnotations;
using IssueManagement.Common.Model;
using IssueManagement.Common.Pagination;
using System.Globalization;
using IssueManagement.Common.EntityDto;

namespace IssueManagement.BL.BaseService
{
    public class BaseService<T, TDto> : IBaseService<T, TDto> where T : class
    {

        private readonly IBaseRepository<T, TDto> _baseRepository;
        public readonly IUnitOfWork _unitOfWork;
        private readonly CommonUtility _commonUtility;
        public IDictionary<string, List<string>> errors = new Dictionary<string, List<string>>(); // Danh sách chứa message lỗi

        #region CONSTRUCTOR
        public BaseService(IBaseRepository<T, TDto> baseRepository, IUnitOfWork unitOfWork, CommonUtility commonUtility)
        {
            _baseRepository = baseRepository;
            _unitOfWork = unitOfWork;
            _commonUtility = commonUtility;
        }

        #endregion

        #region GET METHOD

        /// <summary>
        /// Lấy ra danh sách theo filter c
        /// </summary>
        /// <param name="filterConditions"></param>
        /// <returns></returns>

        public IEnumerable<TDto> GetByFilter(List<FilterCondition>? filterConditions, string sortString)
        {
            IEnumerable<TDto> filterData = (IEnumerable<TDto>)_baseRepository.GetFilterData(filterConditions, sortString);
            return filterData;
        }

        /// <summary>
        /// Lấy ra danh sách bản ghi theo paging và filter
        /// </summary>
        /// <param name="pagingParam"></param>
        /// <returns></returns>
        /// created by: NGUYETKTB 28/09/2023
        public PagingModel GetByPaging(PagingParam pagingParam)
        {
            PagingModel pagingModel = _baseRepository.GetByPaging(pagingParam);
            return pagingModel;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fields"></param>
        /// <returns></returns>
        public IEnumerable<T> GetListByFields(Dictionary<string, object> fields)
        {
            IEnumerable<T> list = _baseRepository.GetListByFields(fields);
            return list;
        }

        /// <summary>
        /// Lấy ra thông tin bản ghi theo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// created by: NGUYETKTB 28/09/2023
        public TDto GetEntityById(Guid id)
        {
            // mở transaction
            TDto entity = _baseRepository.GetEntityById(id);
            return entity;
        }
        #endregion

        #region UPDATE
        /// <summary>
        /// Thực hiện cập nhật thông tin bản ghi
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        /// created by: NGUYETKTB 28/09/2023
        public bool Update(Guid entityId, T entity)
        {
            if (entity == null) return false;
            ValidateCommon(entity, (int)Common.Enumeration.Action.Update);
            // kiểm tra lỗi
            if (errors.Count > 0)
            {
                throw new IMSException(IMSResource.Msg_Exception, (System.Collections.IDictionary?)errors, null);
            }
            HandleEntityBeforeUpdate(entity);
            int row = _baseRepository.Update(entityId, entity);
            return row == 1;
        }

        /// <summary>
        /// Thực hiện cập nhật trạng thái của danh sách bản ghi
        /// </summary>
        /// <param name="ids">id của các bản ghi</param>
        /// <param name="status">trạng thái cần cập nhật</param>
        /// <returns></returns>
        /// created by: NGUYETKTB 28/09/2023
        public int UpdateStatus(string[] ids, int status)
        {
            var rs = _baseRepository.UpdateStatus(ids, status);
            return rs;
        }

        /// <summary>
        /// Thực hiện xóa nhiều bản ghi
        /// kbnguyet 24.10.2023
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public int DeleteMultiple(string[] ids)
        {

            var rs = _baseRepository.DeleteMultiple(ids);
            return rs;
        }
        #endregion

        #region INSERT

        /// <summary>
        /// Thực hiện thêm mới bản ghi
        /// </summary>
        /// <param name="entity">thông tin dữ liệu</param>
        /// <returns></returns>
        /// created by: NGUYETKTB 28/09/2023
        public bool Insert(T entity)
        {
            if (entity == null) return false;
            ValidateCommon(entity, (int)Common.Enumeration.Action.Insert);
            // kiểm tra lỗi
            if (errors.Count > 0)
            {
                throw new IMSException(IMSResource.Msg_Exception, (System.Collections.IDictionary?)errors, null);
            }
            // xử lí dữ liệu trước khi thêm mới
            HandleEntityBeforeInsert(entity);
            // thực hiện thêm mới
            int row = _baseRepository.Insert(entity);
            return row == 1;
        }

        #endregion
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="action"></param>
        public void ValidateCommon(T entity, int action = -1)
        {
            ValidateData(entity, action);
            ValidateReferenceField(entity);
            ValidateCustom(entity, action);
        }
        #region VALIDATE (đặt ở base để các hàm service khác có thể override)
        /// <summary>
        /// Thực hiện kiểm tra dữ liệu theo các attribute custom
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="action"></param>
        /// created by: NGUYETKTB 02/10/2023
        protected virtual void ValidateData(T entity, int action = -1)
        {
            if (entity == null)
            {
                throw new IMSException();
            }
            //lấy ra các property của entity
            var entityProperties = typeof(T).GetProperties();
            // duyệt các property
            foreach (var property in entityProperties)
            {
                // lấy ra value của property
                var propertyValue = property.GetValue(entity);
                // lấy ra các attribute của property
                var propertyAttributes = property.GetCustomAttributes(true);
                // duyệt qua các attribute
                Dictionary<string, object> fields = new Dictionary<string, object>();
                foreach (var attribute in propertyAttributes)
                {
                    switch (attribute)
                    {
                        case UniqueAttribute uniqueAttribute:
                            fields.Add(property.Name, propertyValue);
                            // lấy ra value trong db
                            IEnumerable<T> values = _baseRepository.GetListByFields(fields);
                            // TH 1: thêm mới thì chỉ cần kiểm tra không trùng lặp
                            if ((values.Count() > 0 && action == (int)Common.Enumeration.Action.Insert) ||
                                 (values.Count() > 1 && action == (int)Common.Enumeration.Action.Update))
                            {
                                _commonUtility.AddValueToDictionary((Dictionary<string, List<string>>)errors, property.Name, string.Format(IMSResource.Msg_Duplicate, "Email"));
                            }
                            // TH2: Cập nhật thì cần kiểm tra không trùng lặp với chính bản thân nó
                            else if (values.Count() == 1 && action == (int)Common.Enumeration.Action.Update)
                            {
                                // lấy ra primary key
                                string primaryKey = AttributeUtility.GetPrimaryKeyName<T>();
                                // lấy ra giá trị đầu của values
                                T firstValue = values.FirstOrDefault();
                                // lấy ra value của primary key của values đầu
                                var primaryKeyValue = (firstValue.GetType().GetProperty(primaryKey)).GetValue(firstValue);
                                // lấy ra value của primary key của entity
                                var entityPrimaryKey = (entity.GetType().GetProperty(primaryKey)).GetValue(entity);
                                // kiểm tra
                                if (!entityPrimaryKey.Equals(primaryKeyValue))
                                {
                                    _commonUtility.AddValueToDictionary((Dictionary<string, List<string>>)errors, property.Name, string.Format(IMSResource.Msg_Duplicate, "Email"));
                                }
                            }
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        protected virtual void ValidateReferenceField(T? entity)
        {
            Type entityType = typeof(T);
            // lấy ra properties có attribute reference + required
            List<PropertyInfo> properties = AttributeUtility.GetPropertiesWithAttributes(entityType, typeof(ReferenceAttribute), typeof(RequiredAttribute));
            foreach (PropertyInfo propertyInfo in properties)
            {
                // nếu value của properties khác null hoặc khác Guid.Empty
                object value = propertyInfo.GetValue(entity, null);
                if (value != null && (Guid)value != Guid.Empty)
                {
                    // lấy ra reference attribute
                    ReferenceAttribute referenceAttribute = (ReferenceAttribute)propertyInfo.GetCustomAttribute(typeof(ReferenceAttribute));
                    // lấy ra types của property
                    Type[] types = referenceAttribute.Types;
                    foreach (Type type in types)
                    {
                        // kiểm tra
                        bool isReference = _baseRepository.CheckReferenceEntity<T>(type, propertyInfo, entity);
                        if (!isReference)
                        {
                            _commonUtility.AddValueToDictionary((Dictionary<string, List<string>>)errors, propertyInfo.Name, string.Format(IMSResource.Msg_Invalid, propertyInfo.Name));
                        }

                    }
                }
            }
        }

        /// <summary>
        /// Thực hiện kiểm tra dữ liệu theo từng nghiệp vụ riêng hoặc custom thêm validate bằng các attribute
        /// </summary>
        /// <param name="entity"></param>
        /// created by: NGUYETKTB 28/09/2023
        protected virtual void ValidateCustom(T entity, int action = -1)
        {
            if (entity == null)
            {
                throw new IMSException();
            }
            //lấy ra các property của entity
            var entityProperties = typeof(T).GetProperties();
            // duyệt các property
        }


        /// <summary>
        /// Thực hiện xử lí dữ liệu chung trước khi thực hiện thêm mới
        /// </summary>
        /// <param name="entity"></param>
        ///  created by: NGUYETKTB 28/09/2023
        protected virtual void HandleEntityBeforeInsert(T entity)
        {
            // lấy ra primary key
            string primaryKey = AttributeUtility.GetPrimaryKeyName<T>();
            PropertyInfo propertyInfo = entity.GetType().GetProperty(primaryKey);
            if (propertyInfo != null)
            {
                propertyInfo.SetValue(entity, Guid.NewGuid());
            }
            // Duyệt qua tất cả thuộc tính của entity
            foreach (PropertyInfo prop in entity.GetType().GetProperties())
            {
                // Kiểm tra xem thuộc tính có kiểu Guid không
                // nếu property nào có value = Guid.Emty thì set value = null
                if (prop.PropertyType == typeof(Guid))
                {
                    Guid guidValue = (Guid)prop.GetValue(entity);

                    // Nếu giá trị là Guid.Empty, gán giá trị null
                    if (guidValue == Guid.Empty)
                    {
                        prop.SetValue(entity, null);
                    }
                }
            }
        }

        protected virtual void HandleEntityBeforeUpdate(T entity) { }

        protected virtual void HanldeEntityBeforeDelete(T entity) { }

        /// <summary>
        /// Thực hiện xử lí dữ liệu trong file import
        /// </summary>
        /// <param name="entity">thông tin tài sản</param>
        /// <param name="property">property của tài sản</param>
        /// <param name="field_name">tên của trường dữ liệu</param>
        /// <param name="value">giá trị trường dữ liệu</param>
        /// <param name="messages">danh sách lỗi</param>
        /// <exception cref="MISAException"></exception>
        /// Created By: NguyetKTB (02/06/2023)
        public void HandleDataImport(T entity, string field_name, int field_type, object? value, List<string> messages)
        {
            if (value != null)
            {
                var propertyInfo = entity.GetType().GetProperty(field_name);
                switch (field_type)
                {
                    case (int)FieldType.Int:
                        int intValue = Convert.ToInt32(value);
                        propertyInfo.SetValue(entity, intValue);
                        // set lại trường field key cho entity
                        break;
                    case (int)FieldType.Decimal:
                        break;
                    case (int)FieldType.DateTime:
                        DateTime dateTimeValue = DateTime.Parse(value.ToString(), CultureInfo.CreateSpecificCulture("fr-FR"));
                        propertyInfo.SetValue(entity, dateTimeValue);
                        break;
                    default:
                        string defaultValue = value?.ToString();
                        propertyInfo.SetValue(entity, defaultValue);
                        break;
                }
            }
        }
        #endregion
    }
}
