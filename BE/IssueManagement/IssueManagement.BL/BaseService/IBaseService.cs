﻿using IssueManagement.Common.EntityDto;
using IssueManagement.Common.Model;
using IssueManagement.Common.Pagination;

namespace IssueManagement.BL.BaseService
{
    public interface IBaseService<T, TDto> where T : class
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterConditions"></param>
        /// <returns></returns>
        IEnumerable<TDto> GetByFilter(List<FilterCondition>? filterConditions, string sortString);

        /// <summary>
        /// Lấy ra danh sách bản ghi theo paging và filter
        /// </summary>
        /// <param name="pagingParam"></param>
        /// <returns></returns>
        /// created by: NGUYETKTB 28/09/2023
        PagingModel GetByPaging(PagingParam pagingParam);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fields"></param>
        /// <returns></returns>
        IEnumerable<T> GetListByFields(Dictionary<string, object> fields);

        /// <summary>
        /// Lấy ra thông tin bản ghi theo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// created by: NGUYETKTB 28/09/2023
        TDto GetEntityById(Guid id);


        /// <summary>
        /// Thực hiện cập nhật trạng thái của danh sách bản ghi
        /// </summary>
        /// <param name="ids">id của các bản ghi</param>
        /// <param name="status">trạng thái cần cập nhật</param>
        /// <returns></returns>
        /// created by: NGUYETKTB 28/09/2023
        int UpdateStatus(string[] ids, int status);

        /// <summary>
        /// Thực hiện thêm mới bản ghi
        /// </summary>
        /// <param name="entity">thông tin dữ liệu</param>
        /// <returns></returns>
        /// created by: NGUYETKTB 28/09/2023
        bool Insert(T entity);

        /// <summary>
        /// Thực hiện cập nhật thông tin bản ghi
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        /// created by: NGUYETKTB 28/09/2023
        bool Update(Guid entityId, T entity);

        /// <summary>
        /// Thực hiện xóa nhiều bản ghi
        /// kbnguyet 24.10.2023
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        int DeleteMultiple(string[] ids);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="action"></param>
        void ValidateCommon(T entity, int action = -1);

    }
}
