﻿using IssueManagement.BL.BaseService;
using IssueManagement.BL.ProjectService;
using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using IssueManagement.Common.Exception;
using IssueManagement.Common.Pagination;
using IssueManagement.Common.Resources;
using IssueManagement.Common.UnitOfWork;
using IssueManagement.Common.Utilities;
using IssueManagement.Common.Enumeration;
using IssueManagement.DL.BaseRepository;
using IssueManagement.DL.ClassStudentRepository;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Dapper.SqlMapper;
using LicenseContext = OfficeOpenXml.LicenseContext;
using Microsoft.AspNetCore.Http;
using System.Globalization;
using Org.BouncyCastle.Asn1.Ocsp;
using IssueManagement.Common.Model;
using IssueManagement.BL.ClassService;
using static IssueManagement.Common.Attributes.TableAttribute;
using System.ComponentModel.DataAnnotations;
using IssueManagement.BL.UserService;
using IssueManagement.BL.SettingService;

namespace IssueManagement.BL.ClassStudentService
{
    public class ClassStudentService : BaseService<ClassStudent, ClassStudentDto>, IClassStudentService
    {
        private readonly IClassStudentRepository _classStudentRepository;
        private readonly IUserService _userService;
        private readonly IClassService _classService;
        private readonly IProjectService _projectService;
        private readonly ISettingService _settingService;
        private readonly CommonUtility _commonUtility;
        public ClassStudentService(IClassStudentRepository classStudentRepository, IUnitOfWork unitOfWork, CommonUtility commonUtility, IUserService userService, IProjectService projectService, IClassService classService, ISettingService settingService) : base(classStudentRepository, unitOfWork, commonUtility)
        {
            _classStudentRepository = classStudentRepository;
            _commonUtility = commonUtility;
            _userService = userService;
            _projectService = projectService;
            _classService = classService;
            _settingService = settingService;
    }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pagingParam"></param>
        /// <returns></returns>
        public PagingModel GetStudents(PagingParam pagingParam)
        {
            PagingModel pagingModel = _classStudentRepository.GetStudents(pagingParam);
            return pagingModel;
        }


        /// <summary>
        /// Thực hiện thêm mới danh sách student vào lớp
        /// </summary>
        /// <param name="classStudents"></param>
        /// <returns></returns>
        /// created by: kbnguyet 17.10.2023
        public bool InsertMultiple(List<ClassStudent> classStudents)
        {
            // thực hiện validate dữ liệu trước khi thêm mới
            foreach (var classStudent in classStudents)
            {
                base.ValidateCommon(classStudent, (int)Common.Enumeration.Action.Insert);
                base.HandleEntityBeforeInsert(classStudent);
            }
            // kiểm tra danh sách lỗi
            if (errors.Count > 0)
            {
                throw new IMSException(IMSResource.Msg_Exception, null, null);
            }
            // xử lí dữ liệu trước khi thêm mới
            // thực hiện thêm mới
            int row = _classStudentRepository.InsertMultiple(classStudents);
            return row == classStudents.Count();
        }

        /// <summary>
        /// Xử lí lấy dữ liệu export và trả về file cho người dùng
        /// </summary>
        /// <param name="filterConditions"></param>
        /// <returns></returns>
        public Stream ExportExcel(List<FilterCondition>? filterConditions)
        {
            // trường hợp export tất cả hoặc export theo điều kiện nào đó
            bool isExportAll = true;
            if (filterConditions.Count > 0)
            {
                isExportAll = false;
            }
            var exportData = isExportAll ? _classStudentRepository.GetFilterData(new List<FilterCondition>(), "") : base.GetByFilter(filterConditions, "");

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var stream = new MemoryStream();
            var package = new ExcelPackage(stream);
            var workSheet = package.Workbook.Worksheets.Add($"Danh sách sinh viên");
            package.Workbook.Properties.Author = "NGUYETKTB";
            BindingFormatForExcel(workSheet, exportData); // Binding format cho file excel
            package.Workbook.Properties.Title = $"Danh sách sinh viên";
            workSheet.Cells[workSheet.Dimension.Address].AutoFitColumns();
            package.Save();
            stream.Position = 0; // Đặt con trỏ về đầu file để đọc
            return package.Stream;
        }


        /// <summary>
        /// Xử lí format dữ liệu cho excel
        /// </summary>
        /// <param name="worksheet"></param>
        /// <param name="classStudents"></param>
        private void BindingFormatForExcel(ExcelWorksheet worksheet, IEnumerable<ClassStudent> classStudents)
        {
            // lấy ra các column excel của student
            var excelColumns = _classStudentRepository.GetStudentExcelModel();
            // lấy cột cuối cùng
            var lastColumn = excelColumns.Count() + 1;

            // tạo phần header cho file excel
            using (var range = worksheet.Cells[1, 1, 1, lastColumn])
            {
                // Tạo range từ ô A1 đến ô cuối cùng của tiêu đề
                range.Merge = true;
                range.Style.Font.Bold = true;
                range.Style.Font.Size = 16;
                range.Style.Font.Name = "Arial";
                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                range.Value = $"Danh sách sinh viên";
            }

            // Gộp các ô từ A2 đến ô cuối cùng của dòng 2
            worksheet.Cells[2, 1, 2, lastColumn].Merge = true;


            // Style chung cho tất cả bảng
            using (var range = worksheet.Cells[3, 1, classStudents.Count() + 3, lastColumn])
            {
                // Tạo range từ ô A3 đến ô cuối cùng của dữ liệu
                range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                range.Style.Font.Name = "Arial";
                range.Style.Font.Size = 11;
            }
            // style cho header table
            using (var range = worksheet.Cells[3, 1, 3, lastColumn])
            {
                range.Style.Font.Size = 11;
                range.Style.Font.Bold = true;
            }

            // đổ dữ liệu vào header table
            int columnIndex = 1;
            //worksheet.Cells[3, columnIndex].Value = 1;
            columnIndex++;
            foreach (var item in excelColumns)
            {
                worksheet.Cells[3, columnIndex].Value = item.field_name;
                columnIndex++;
            }

            // đổ dữ liệu vào các cột tương ứng
            int rowIndex = 4;
            int index = 1;
            // duyệt qua danh sách dữ liệu
            foreach (var entity in classStudents)
            {
                columnIndex = 1;
                worksheet.Cells[rowIndex, columnIndex].Value = index;
                worksheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                columnIndex++;
                // duyệt qua các cột để map
                foreach (var item in excelColumns)
                {
                    var propertyInfo = entity.GetType().GetProperty(item.field_key);
                    var propertyValue = "";
                    if (propertyInfo != null)
                    {
                        propertyValue = (string)propertyInfo.GetValue(entity, null);
                    }
                    var indexValue = "";
                    switch (item.field_type)
                    {
                        case (int)FieldType.Int:
                            if (int.TryParse(propertyValue, out int result))
                            {
                                indexValue = (result as int?)?.ToString();
                            }
                            break;
                        case (int)FieldType.Decimal:
                            break;
                        case (int)FieldType.DateTime:
                            DateTime date = DateTime.ParseExact(propertyValue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            DateTime parsedDate;
                            if (DateTime.TryParseExact(propertyValue, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedDate))
                            {
                                indexValue = (parsedDate as DateTime?)?.ToString("dd/MM/yyyy");
                            }
                            break;
                        default:
                            indexValue = propertyValue?.ToString();
                            break;
                    }
                    worksheet.Cells[rowIndex, columnIndex].Value = indexValue;
                    columnIndex++;
                }
                rowIndex++;
                index++;
            }
        }

        public Object ImportExcel(IFormFile formFile, Guid class_id)
        {
            IEnumerable<StudentExcelModel> excelModels = _classStudentRepository.GetStudentExcelModel();
            List<ClassStudent> classStudents = new List<ClassStudent>();
            using (var stream = new MemoryStream())
            {
                formFile.CopyTo(stream);
                stream.Position = 2;
                using (var package = new ExcelPackage(stream))
                {
                    // lấy ra worksheet đầu tiên trong file
                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                    // lấy ra số hàng trong worksheet
                    int rowCount = worksheet.Dimension.Rows;
                    // lấy ra số cột trong worlsheet
                    int columnCount = worksheet.Dimension.Columns;
                    // lấy ra hàng đầu tiền
                    var firstRow = new List<object>();
                    for (int col = 2; col <= columnCount; col++)
                    {
                        var cellValue = worksheet.Cells[3, col].Value;
                        firstRow.Add(cellValue);
                    }
                    // duyệt các hàng và các cột
                    for (var row = 4; row <= rowCount; row++)
                    {
                        ClassStudentDto classStudent = new ClassStudentDto();
                        List<string> errorMessage = new List<string>();
                        for (var col = 2; col < columnCount; col++)
                        {
                            var fieldValue = worksheet.Cells[row, col].Value; // giá trị tại hàng row - cột col
                            // lấy ra cột thứ col của hàng đầu tiên 
                            var firstRowValue = firstRow[col - 2];
                            // duyệt excel columns
                            foreach (var excelColumn in excelModels)
                            {
                                if (excelColumn.field_name.Equals(firstRowValue))
                                {
                                    base.HandleDataImport(classStudent, excelColumn.field_key,
                                        excelColumn.field_type, fieldValue, errorMessage);
                                }
                            }
                        }
                        // lấy ra project code , project name -> suy ra project id
                        Dictionary<string, object> projectProps = new Dictionary<string, object>();
                        projectProps.Add("project_code", classStudent.project_code);
                        // nếu project tồn tại thì gán bình thường
                        IEnumerable<Project> projectValue = _projectService.GetListByFields(projectProps);
                        if (projectValue != null && projectValue.Count() == 1)
                        {
                            classStudent.project_id = projectValue.FirstOrDefault().project_id;
                        }
                        // lấy ra student id thông qua email
                        Dictionary<string, object> studentProps = new Dictionary<string, object>();
                        studentProps.Add("email", classStudent.student_email);
                        IEnumerable<User> studentValue = _userService.GetListByFields(studentProps);
                        // nếu student tồn tại thì gán bình thường
                        if (studentValue != null && studentValue.Count() == 1)
                        {
                            classStudent.student_id = studentValue.FirstOrDefault().user_id;
                        }
                        else
                        {
                            // lấy ra project code , project name -> suy ra project id
                            Dictionary<string, object> roles = new Dictionary<string, object>();
                            roles.Add("setting_value", "Student");
                            // nếu project tồn tại thì gán bình thường
                            IEnumerable<Setting> roleValue = _settingService.GetListByFields(roles);
                            // nếu student không tồn tại thì thêm mới student
                            User user = new User
                            {
                                user_id = Guid.NewGuid(),
                                fullname = classStudent.student_name,
                                email = classStudent.student_email,
                                password = _commonUtility.GeneratePassword(),
                                phone_number = classStudent.student_phone,
                                setting_id = roleValue.FirstOrDefault().setting_id
                            };
                            // sau đó gửi mail
                            if (_userService.Insert(user))
                            {
                                // đây là đoạn gửi mail
                                // lấy lại student theo mail
                                studentProps.Remove("email");
                                studentProps.Add("email", classStudent.student_email);
                                studentValue = _userService.GetListByFields(studentProps);
                                classStudent.student_id = studentValue.FirstOrDefault().user_id;
                            }
                        }
                        ClassStudent student = new ClassStudent
                        {
                            class_student_id = Guid.NewGuid(),
                            student_id = classStudent.student_id,
                            project_id = classStudent.project_id,
                            class_id = class_id,
                        };
                        classStudents.Add(student);
                    }
                }
            }
            if (classStudents.Count() > 0)
            {
                bool rs = InsertMultiple(classStudents);
            }
            return null;
        }



        protected override void ValidateCustom(ClassStudent entity, int action = -1)
        {
            base.ValidateCustom(entity, action);
            bool isExist = false;
            if (entity != null)
            {
                Dictionary<string, object> properties = new Dictionary<string, object>();
                properties.Add("student_id", entity.student_id);
                IEnumerable<ClassStudent> students = _classStudentRepository.GetListByFields(properties);
                foreach (var student in students)
                {
                    isExist = student.class_id.Equals(entity.class_id);
                }
            }
            if (isExist)
            {
                _commonUtility.AddValueToDictionary((Dictionary<string, List<string>>)errors, "student", "Student is already exist in this class.");
            }
        }

    }
}
