﻿using IssueManagement.BL.BaseService;
using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using IssueManagement.Common.Model;
using IssueManagement.Common.Pagination;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.BL.ClassStudentService
{
    public interface IClassStudentService : IBaseService<ClassStudent, ClassStudentDto>
    {
         bool InsertMultiple(List<ClassStudent> classStudents);

        Stream ExportExcel(List<FilterCondition>? filterConditions);

        Object ImportExcel(IFormFile formFile, Guid class_id);
        PagingModel GetStudents(PagingParam pagingParam);


    }
}
