﻿using IssueManagement.BL.BaseService;
using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.BL.MilestoneService
{
    public interface IMilestoneService : IBaseService<Milestone, MilestoneDto>
    {
    }
}
