﻿using IssueManagement.BL.BaseService;
using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using IssueManagement.Common.UnitOfWork;
using IssueManagement.Common.Utilities;
using IssueManagement.DL.BaseRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.BL.MilestoneService
{
    public class MilestoneService : BaseService<Milestone, MilestoneDto>, IMilestoneService
    {
        private readonly CommonUtility _commonUtility;
        public MilestoneService(IBaseRepository<Milestone, MilestoneDto> baseRepository, IUnitOfWork unitOfWork, CommonUtility commonUtility) : base(baseRepository, unitOfWork, commonUtility)
        {
            _commonUtility = commonUtility;
        }
    }
}
