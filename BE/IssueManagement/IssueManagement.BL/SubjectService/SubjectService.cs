﻿using IssueManagement.BL.BaseService;
using IssueManagement.BL.SettingService;
using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using IssueManagement.Common.Resources;
using IssueManagement.Common.UnitOfWork;
using IssueManagement.Common.Utilities;
using IssueManagement.Common.Validation;
using IssueManagement.DL.BaseRepository;
using IssueManagement.DL.ProjectRepository;
using IssueManagement.DL.SettingRepository;
using IssueManagement.DL.SubjectRepository;
using IssueManagement.DL.UserRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.BL.SubjectService
{
    public class SubjectService : BaseService<Subject, SubjectDto>, ISubjectService
    {
        private readonly CommonUtility _commonUtility;
        private readonly IUserRepository _userRepository;
        private readonly ISettingRepository _settingRepository;
        public SubjectService(IBaseRepository<Subject, SubjectDto> baseRepository, IUnitOfWork unitOfWork, CommonUtility commonUtility, IUserRepository userRepository, ISettingRepository settingRepository) : base(baseRepository, unitOfWork, commonUtility)
        {
            _commonUtility = commonUtility;
            _userRepository = userRepository;
            _settingRepository = settingRepository;
        }
        protected override void ValidateCustom(Subject entity, int action = -1)
        {
            //Kiểm tra user có tồn tại hay không và setting_value phải là Manager
            Dictionary<string, object> userProperties = new Dictionary<string, object>();
            userProperties.Add("user_id", entity.assignee_id);
            User user = _userRepository.GetListByFields(userProperties).First();
            Dictionary<string, object> roleProperties = new Dictionary<string, object>();
            roleProperties.Add("setting_id", user.setting_id);
            Setting role = _settingRepository.GetListByFields(roleProperties).Count() > 0 ? _settingRepository.GetListByFields(roleProperties).First() : new Setting();
            if (role.setting_value!="Manager" && role.data_group==1)
            {
                _commonUtility.AddValueToDictionary((Dictionary<string, List<string>>)errors, "manager_id", string.Format(IMSResource.Msg_NotExist, "Manager"));
            }
        }
    }
}
