﻿using IssueManagement.BL.BaseService;
using IssueManagement.Common.Entities;
using IssueManagement.Common.EntityDto;
using IssueManagement.Common.Model;
using IssueManagement.Common.Resources;
using IssueManagement.Common.UnitOfWork;
using IssueManagement.Common.Utilities;
using IssueManagement.DL.BaseRepository;
using IssueManagement.DL.ClassRepository;
using IssueManagement.DL.ProjectRepository;
using IssueManagement.DL.SubjectRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace IssueManagement.BL.IssueSettingService
{
    public class IssueSettingService : BaseService<IssueSetting, IssueSettingDto>, IIssueSettingService
    {
        private readonly CommonUtility _commonUtility;
        private readonly ISubjectRepository _subjectRepository;
        private readonly IClassRepository _classRepository;
        private readonly IProjectRepository _projectRepository;

        public IssueSettingService(IBaseRepository<IssueSetting, IssueSettingDto> baseRepository, IUnitOfWork unitOfWork, CommonUtility commonUtility, ISubjectRepository subjectRepository, IClassRepository classRepository, IProjectRepository projectRepository) : base(baseRepository, unitOfWork, commonUtility)
        {
            _commonUtility = commonUtility;
            _subjectRepository = subjectRepository;
            _classRepository = classRepository;
            _projectRepository = projectRepository;
        }
        //protected override void ValidateCustom(IssueSetting entity, int action = -1)
        //{
        //    if(entity.class_id == null && entity.project_id == null &&  entity.subject_id == null) {
        //        _commonUtility.AddValueToDictionary((Dictionary<string, List<string>>)errors, "Input", string.Format(IMSResource.Msg_NotExist, "Need atleast 1 field!"));
        //    }
        //    if(entity.class_id != null)
        //    {
        //        Dictionary<string, object> classProperties = new Dictionary<string, object>();
        //        classProperties.Add("class_id", entity.class_id);
        //        IEnumerable<Class> classes = _classRepository.GetListByFields(classProperties);
        //        if(classes.Count() == 0)
        //        {
        //            _commonUtility.AddValueToDictionary((Dictionary<string, List<string>>)errors, "class_id", string.Format(IMSResource.Msg_NotExist, "Class"));
        //        }
        //    }
        //    if (entity.subject_id !=null)
        //    {
        //        Dictionary<string, object> subjectProperties = new Dictionary<string, object>();
        //        subjectProperties.Add("subject_id",entity.subject_id);
        //        IEnumerable<Subject> subjects = _subjectRepository.GetListByFields(subjectProperties);
        //        if (subjects.Count() == 0)
        //        {
        //            _commonUtility.AddValueToDictionary((Dictionary<string, List<string>>)errors, "subject_id", string.Format(IMSResource.Msg_NotExist, "Subject"));
        //        }
        //    }
        //    if (entity.project_id != null)
        //    {
        //        Dictionary<string, object> projectProperties = new Dictionary<string, object>();
        //        projectProperties.Add("project_id", entity.project_id);
        //        IEnumerable<Project> projects = _projectRepository.GetListByFields(projectProperties);
        //        if (projects.Count() == 0)
        //        {
        //            _commonUtility.AddValueToDictionary((Dictionary<string, List<string>>)errors, "project_id", string.Format(IMSResource.Msg_NotExist, "Project"));
        //        }
        //    }
        //}


    }
}
