﻿CREATE TABLE issue (
  issue_id CHAR(36) NOT NULL DEFAULT '',
  issue_type CHAR(36) DEFAULT NULL,
  issue_type_value VARCHAR(255) DEFAULT NULL,
  issue_status CHAR(36) DEFAULT NULL,
  issue_status_value VARCHAR(255) DEFAULT NULL,
  work_process CHAR(36) DEFAULT NULL,
  work_process_value VARCHAR(255) DEFAULT NULL,
  due_date DATETIME DEFAULT NULL,
  description VARCHAR(500) DEFAULT NULL,
  project_id CHAR(36) DEFAULT NULL,
  project_code VARCHAR(100) DEFAULT NULL,
  project_name VARCHAR(255) DEFAULT NULL,
  milestone_id CHAR(36) DEFAULT NULL,
  milestone_name VARCHAR(255) DEFAULT NULL,
  assignee CHAR(36) DEFAULT NULL,
  assignee_name VARCHAR(255) DEFAULT NULL,
  created_date DATETIME DEFAULT NULL,
  created_by VARCHAR(255) DEFAULT NULL,
  modified_date DATETIME DEFAULT NULL,
  modified_by VARCHAR(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  PRIMARY KEY (issue_id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_0900_ai_ci,
ROW_FORMAT = DYNAMIC;

ALTER TABLE issue 
  ADD CONSTRAINT FK_issue_assignee FOREIGN KEY (assignee)
    REFERENCES user(user_id);

ALTER TABLE issue 
  ADD CONSTRAINT FK_issue_issue_status FOREIGN KEY (issue_status)
    REFERENCES issue_setting(issue_setting_id);

ALTER TABLE issue 
  ADD CONSTRAINT FK_issue_issue_type FOREIGN KEY (issue_type)
    REFERENCES issue_setting(issue_setting_id);

ALTER TABLE issue 
  ADD CONSTRAINT FK_issue_milestone_id FOREIGN KEY (milestone_id)
    REFERENCES milestone(milestone_id);

ALTER TABLE issue 
  ADD CONSTRAINT FK_issue_project_id FOREIGN KEY (project_id)
    REFERENCES project(project_id);

ALTER TABLE issue 
  ADD CONSTRAINT FK_issue_work_process FOREIGN KEY (work_process)
    REFERENCES issue_setting(issue_setting_id);