﻿CREATE TABLE subject (
  subject_id CHAR(36) NOT NULL DEFAULT '' COMMENT 'id môn học',
  subject_code VARCHAR(100) NOT NULL DEFAULT '' COMMENT 'mã môn học',
  subject_name VARCHAR(255) DEFAULT NULL COMMENT 'tên môn học',
  description CHAR(20) DEFAULT NULL COMMENT 'mô tả',
  status SMALLINT DEFAULT NULL COMMENT 'trạng thái',
  created_date DATETIME DEFAULT NULL COMMENT 'ngày tạo',
  created_by VARCHAR(255) DEFAULT NULL COMMENT 'người tạo',
  modified_date DATETIME DEFAULT NULL COMMENT 'ngày sửa',
  modified_by VARCHAR(255) DEFAULT NULL COMMENT 'người sửa',
  user_id CHAR(36) NOT NULL DEFAULT '' COMMENT 'id người dùng',
  PRIMARY KEY (subject_id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_0900_ai_ci,
COMMENT = 'Bảng chứa thông tin môn học',
ROW_FORMAT = DYNAMIC;

ALTER TABLE subject 
  ADD CONSTRAINT FK_subject_user_id FOREIGN KEY (user_id)
    REFERENCES user(user_id);