﻿CREATE TABLE milestone (
  milestone_id CHAR(36) NOT NULL DEFAULT '',
  milestone_name VARCHAR(255) DEFAULT NULL,
  description VARCHAR(500) DEFAULT NULL,
  from_date DATE DEFAULT NULL,
  to_date DATE DEFAULT NULL,
  status SMALLINT DEFAULT NULL,
  class_id CHAR(36) DEFAULT NULL,
  class_code VARCHAR(100) DEFAULT NULL,
  class_name VARCHAR(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  project_id CHAR(36) DEFAULT NULL,
  project_code VARCHAR(100) DEFAULT NULL,
  project_name VARCHAR(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  created_date DATETIME DEFAULT NULL,
  created_by VARCHAR(255) DEFAULT NULL,
  modified_date DATETIME DEFAULT NULL,
  modified_by VARCHAR(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  PRIMARY KEY (milestone_id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_0900_ai_ci,
COMMENT = 'Bảng chứa thông tin milestone',
ROW_FORMAT = DYNAMIC;

ALTER TABLE milestone 
  ADD CONSTRAINT FK_milestone_class_id FOREIGN KEY (class_id)
    REFERENCES class(class_id);

ALTER TABLE milestone 
  ADD CONSTRAINT FK_milestone_project_id FOREIGN KEY (project_id)
    REFERENCES project(project_id);