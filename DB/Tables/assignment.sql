﻿CREATE TABLE assignment (
  assignment_id CHAR(36) NOT NULL DEFAULT '' COMMENT 'id assignment',
  assignment_name VARCHAR(255) DEFAULT NULL,
  description VARCHAR(500) DEFAULT NULL,
  status SMALLINT DEFAULT NULL,
  created_date DATETIME DEFAULT NULL,
  created_by VARCHAR(255) DEFAULT NULL,
  modified_date DATETIME DEFAULT NULL,
  modified_by VARCHAR(255) DEFAULT NULL,
  subject_id CHAR(36) NOT NULL DEFAULT '',
  subject_code VARCHAR(100) DEFAULT NULL,
  subject_name VARCHAR(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  PRIMARY KEY (assignment_id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_0900_ai_ci,
COMMENT = 'Bảng mô tả thông tin assignment của subject',
ROW_FORMAT = DYNAMIC;

ALTER TABLE assignment 
  ADD CONSTRAINT FK_assignment_subject_id FOREIGN KEY (subject_id)
    REFERENCES subject(subject_id);