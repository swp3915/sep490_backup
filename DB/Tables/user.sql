﻿CREATE TABLE user (
  user_id CHAR(36) NOT NULL DEFAULT '' COMMENT 'id người dùng',
  user_name VARCHAR(100) DEFAULT NULL COMMENT 'tên người dùng',
  fullname VARCHAR(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT 'họ và tên người dùng',
  password CHAR(255) NOT NULL DEFAULT '' COMMENT 'mật khẩu',
  email VARCHAR(100) NOT NULL DEFAULT '' COMMENT 'email của người dùng',
  phone_number CHAR(50) DEFAULT NULL COMMENT 'số điện thoại người dùng',
  setting_id CHAR(36) NOT NULL DEFAULT '',
  setting_name VARCHAR(255) DEFAULT NULL,
  setting_value VARCHAR(255) DEFAULT NULL,
  status SMALLINT DEFAULT NULL COMMENT 'trạng thái ',
  created_date DATETIME DEFAULT NULL COMMENT 'ngày tạo',
  created_by VARCHAR(255) DEFAULT NULL COMMENT 'người tạo',
  modified_date DATETIME DEFAULT NULL COMMENT 'ngày sửa',
  modified_by VARCHAR(255) DEFAULT NULL COMMENT 'người sửa',
  PRIMARY KEY (user_id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_0900_ai_ci,
COMMENT = 'Bảng chứa thông tin người dùng',
ROW_FORMAT = DYNAMIC;

ALTER TABLE user 
  ADD CONSTRAINT FK_user_setting_id FOREIGN KEY (setting_id)
    REFERENCES setting(setting_id);