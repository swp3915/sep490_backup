﻿CREATE TABLE project (
  project_id CHAR(36) NOT NULL DEFAULT '',
  project_code VARCHAR(100) DEFAULT NULL,
  project_name VARCHAR(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  description VARCHAR(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  status SMALLINT DEFAULT NULL,
  leader_id CHAR(36) DEFAULT NULL,
  leader_name VARCHAR(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  class_id CHAR(36) NOT NULL DEFAULT '',
  class_code VARCHAR(100) DEFAULT NULL,
  class_name VARCHAR(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  created_date DATETIME DEFAULT NULL,
  created_by VARCHAR(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  modified_date DATETIME DEFAULT NULL,
  modified_by VARCHAR(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  PRIMARY KEY (project_id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_0900_ai_ci,
COMMENT = 'Bảng chứa thông tin dự án của lớp',
ROW_FORMAT = DYNAMIC;

ALTER TABLE project 
  ADD CONSTRAINT FK_project_class_id FOREIGN KEY (class_id)
    REFERENCES class(class_id);

ALTER TABLE project 
  ADD CONSTRAINT FK_project_leader_id FOREIGN KEY (leader_id)
    REFERENCES user(user_id);