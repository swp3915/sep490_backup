﻿CREATE TABLE setting (
  setting_id CHAR(36) NOT NULL DEFAULT '',
  setting_name VARCHAR(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  setting_value VARCHAR(255) DEFAULT NULL COMMENT 'tên thiết lập',
  data_group INT DEFAULT NULL COMMENT 'field được thiết lập',
  display_order INT DEFAULT NULL COMMENT 'thứ tự hiển thị',
  status SMALLINT DEFAULT NULL,
  modified_date DATETIME DEFAULT NULL COMMENT 'ngày sửa',
  created_date DATETIME DEFAULT NULL COMMENT 'ngày tạo',
  created_by VARCHAR(255) DEFAULT NULL COMMENT 'người tạo',
  modified_by VARCHAR(255) DEFAULT NULL COMMENT 'người sửa',
  PRIMARY KEY (setting_id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_0900_ai_ci,
COMMENT = 'Bảng chứa thông tin được quản lý (bao gồm role, semester) - có thể gọi là bảng thiết lập',
ROW_FORMAT = DYNAMIC;