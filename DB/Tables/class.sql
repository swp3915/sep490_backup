﻿CREATE TABLE class (
  class_id CHAR(36) NOT NULL DEFAULT '',
  class_code VARCHAR(100) DEFAULT NULL,
  class_name VARCHAR(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  description VARCHAR(500) DEFAULT NULL,
  status SMALLINT DEFAULT NULL,
  subject_id CHAR(36) NOT NULL DEFAULT '',
  subject_code VARCHAR(100) DEFAULT NULL,
  subject_name VARCHAR(255) DEFAULT NULL,
  setting_id CHAR(36) NOT NULL DEFAULT '',
  setting_name VARCHAR(255) DEFAULT NULL,
  setting_value VARCHAR(255) DEFAULT NULL,
  created_date DATETIME DEFAULT NULL,
  created_by VARCHAR(255) DEFAULT NULL,
  modified_date DATETIME DEFAULT NULL,
  modified_by VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (class_id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_0900_ai_ci,
COMMENT = 'Bảng chứa thông tin lớp học',
ROW_FORMAT = DYNAMIC;

ALTER TABLE class 
  ADD CONSTRAINT FK_class_setting_id FOREIGN KEY (setting_id)
    REFERENCES setting(setting_id);

ALTER TABLE class 
  ADD CONSTRAINT FK_class_subject_id FOREIGN KEY (subject_id)
    REFERENCES subject(subject_id);