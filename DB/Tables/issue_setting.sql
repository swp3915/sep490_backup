﻿CREATE TABLE issue_setting (
  issue_setting_id CHAR(36) NOT NULL DEFAULT '',
  issue_value VARCHAR(255) DEFAULT NULL,
  issue_group VARCHAR(255) DEFAULT NULL,
  description VARCHAR(255) DEFAULT NULL,
  status SMALLINT DEFAULT NULL,
  style JSON DEFAULT NULL,
  subject_id CHAR(36) DEFAULT NULL,
  subject_code VARCHAR(255) DEFAULT NULL,
  subject_name VARCHAR(255) DEFAULT NULL,
  class_id CHAR(36) DEFAULT NULL,
  class_code VARCHAR(100) DEFAULT NULL,
  class_name VARCHAR(255) DEFAULT NULL,
  project_id CHAR(36) DEFAULT NULL,
  project_code VARCHAR(100) DEFAULT NULL,
  project_name VARCHAR(255) DEFAULT NULL,
  created_date DATETIME DEFAULT NULL,
  created_by VARCHAR(255) DEFAULT NULL,
  modified_date DATETIME DEFAULT NULL,
  modified_by VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (issue_setting_id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_0900_ai_ci,
ROW_FORMAT = DYNAMIC;

ALTER TABLE issue_setting 
  ADD CONSTRAINT FK_issue_setting_class_id FOREIGN KEY (class_id)
    REFERENCES class(class_id);

ALTER TABLE issue_setting 
  ADD CONSTRAINT FK_issue_setting_project_id FOREIGN KEY (project_id)
    REFERENCES project(project_id);

ALTER TABLE issue_setting 
  ADD CONSTRAINT FK_issue_setting_subject_id FOREIGN KEY (subject_id)
    REFERENCES subject(subject_id);